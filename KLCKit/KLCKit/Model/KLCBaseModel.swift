//
//  KLCBaseModel.swift
//  InstallManage
//
//  Created by knight on 2017/9/18.
//  Copyright © 2017年 Goswift iOS team All rights reserved.
//

import UIKit
import HandyJSON

open class KLCBaseModel: HandyJSON {
    required public init() {}
    open func mapping(mapper: HelpingMapper) {}
}
