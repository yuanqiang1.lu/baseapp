//
//  KLCKitUmbrella.h
//  KLCKit
//
//  Created by Kun Huang on 2019/4/2.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import <Pingpp/Pingpp.h>
