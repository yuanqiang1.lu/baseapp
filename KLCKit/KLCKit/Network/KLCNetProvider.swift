//
//  KLCNetProvider.swift
//  KLC
//
//  Created by wang zhenqi on 2018/1/17.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
import Moya
import HandyJSON
import RxSwift
import enum Result.Result
//import Kingfisher
import Alamofire

public extension NSNotification.Name {
    /// 登出发送的通知
    static let klcDidLogoutNotificationName = NSNotification.Name("klc_logout_noitification")
    
    /// 需要弹出登录界面的通知
    static let klcNeedLoginNotificationName = NSNotification.Name("klc_need_login_noitification")
    
    static let klcLoginSuccessNotificationName = NSNotification.Name("klc_login_success_noitification")
}
/// 接收所有网络返回数据 model
func processNetStatusCode(_ code: Int) {
    if code == .loginCode || code == 601 { // 约定401为未登录状态的状态码
        NotificationCenter.default.post(name: .klcNeedLoginNotificationName, object: nil)
    }
}

public class KLCResponseModel<T: HandyJSON>: KLCBaseModel{
    
    var code = 0 {
        didSet {
            processNetStatusCode(code)
        }
    }
    var data: Any?
    var msg: String?
    
    /// 本次返回是否成功(逻辑成功)
    var isSuccess:Bool {
        let scode = 200..<300
        let scode1 = 0...1
        return scode.contains(code) || scode1.contains(code) 
    }
    
    /// 解析数组对象
    func parseByType() -> [T?]? {
        return JSONDeserializer<T>.deserializeModelArrayFrom(array: data as? NSArray)
    }

    /// 解析单个对象
    func parseByType() -> T? {
        return JSONDeserializer<T>.deserializeFrom(dict: data as? NSDictionary)
    }
}

class CustomServerTrustPoliceManager : ServerTrustPolicyManager {
    override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        return .disableEvaluation
    }
    public init() {
        super.init(policies: [:])
    }
}

/// 所有网络请求的发起工具
public let KLCNetwork: RxMoyaProvider<MultiTarget> =
{
    //debug Log config
    let pluginLog = NetworkLoggerPlugin(verbose: true,
                                        cURL: true,
                                        output: nil,
                                        requestDataFormatter: {
                                            (data) -> (String) in
                                            return String(data: data, encoding: String.Encoding.utf8) ?? ""
    },
                                        responseDataFormatter: { (data) -> (Data) in
                                            return Data()
    })
    
    let manager = Manager(configuration: URLSessionConfiguration.default,serverTrustPolicyManager:CustomServerTrustPoliceManager())
    let provider = RxMoyaProvider<MultiTarget>(manager:manager, plugins:[AuthPlugin(tokenClosure: getTicket, userAgentClosure: klicenUserAgent),pluginLog])
    
    return provider
}()

public func cancelAllRequest() {
    KLCNetwork.manager.session.getTasksWithCompletionHandler { (dataTask, uploadTask, downloadTask) in
        dataTask.forEach{ $0.cancel() }
        uploadTask.forEach{ $0.cancel() }
        downloadTask.forEach{ $0.cancel() }
    }
}


//MARK: - 自定义插件, 设置请求头
struct AuthPlugin: PluginType {
    let tokenClosure: () -> String?
    let userAgentClosure: () -> [String : String]
    
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        var request = request
        // 设置token
        if let token = tokenClosure() {
            request.addValue("ticket=\(token)", forHTTPHeaderField: "Cookie")
        }
        // 设置userAgent
        let dict = userAgentClosure()
        dict.forEach { (key, value) in
            request.addValue(value, forHTTPHeaderField: key)
        }
        
//        request.addValue("https", forHTTPHeaderField: "X-Forwarded-Proto")
        request.timeoutInterval = 20
//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)// 手机震动
        return request
    }
    
    func didReceive(_ result: Result<Moya.Response, MoyaError>, target: TargetType) {
        if case .success(let response) = result {
            let responseData = String(data: response.data, encoding: String.Encoding.utf8) ?? ""
            print("Moya.Response=====\n \(responseData)")
        } else {
            print("Moya.result=======\n \(result)")
        }
    }
}

let authEndpoint = NetworkActivityPlugin { (type, targetType) in
    
}

public func networkStatus() {
    let manager = NetworkReachabilityManager(host: "http://www.apple.com")
    manager?.listener = { status in
        switch status {
        case .notReachable:
            print("notReachable")
        case .unknown:
            print("unknown")
        case .reachable(.ethernetOrWiFi):
            print("ethernetOrWiFi")
        case .reachable(.wwan):
            print("wwan")
        }
    }
    manager?.startListening()
}


extension ObservableType where E == Response {
    public func mapModel<T: HandyJSON>(_ type:T.Type) -> Observable<T?> {
        return flatMap({ (response) -> Observable<T?> in
            do{
                let model = try response.mapModel(T.self)
                return Observable.just(model)
            } catch {
                return Observable.error(error)
            }
        })
    }
    
    public func mapArrayModel<T: HandyJSON>(_ type:T.Type) -> Observable<[T?]?> {
        return flatMap({ (response) -> Observable<[T?]?> in
            do{
                let array = try response.mapArrayModel(T.self)
                return Observable.just(array)
            } catch {
                return Observable.error(error)
            }
        })
    }
    
    public func mapDictionary() -> Observable<Dictionary<String, Any>?> {
        return flatMap({ (response) -> Observable<Dictionary<String, Any>?> in
            do{
                return Observable.just( try response.mapDict())
            } catch {
                return Observable.error(error)
            }
            
        })
    }
    
//    public func mapImage() -> Observable<UIImage?> {
//        return flatMap({ (response) -> Observable<UIImage?> in
//            do{
//                let image = try response.mapImage()
//                return Observable.just(image)
//            } catch {
//                return Observable.error(error)
//            }
//        })
//    }
}

/// 测试类
//struct Cat: HandyJSON {
//    var name:String?
//    var age:Int?
//}

//MARK:- -Response解析规则
extension Response {

    /// 解析model, 从网络接收到数据解析KLCResponseModel, 再从data字段中解析出想要的数据
    func mapModel<T: HandyJSON>(_ type:T.Type) throws -> T? {
        // 测试数据
        // let dict = ["code" : 0, "msg" : "成功了哈", "data" : ["name" : "小黑", "age" : 1]] as [String : Any]
        do {
            guard let dict = try self.mapJSON() as? NSDictionary else {
                throw KLCNetError(msg: "未知错误", code: 0)
            }
            
            // 检查数据完整性
            if let error = dict.value(forKey: "error") as? String {
                throw KLCNetError(msg: error, code: self.statusCode)
            }
            
            if (dict["code"] != nil) {
                let base = JSONDeserializer<KLCResponseModel<T>>.deserializeFrom(dict: dict)
                // 当业务逻辑成功后才进行具体model解析,否则将错误信息抛出
                if let success = base?.isSuccess, success, dict.count > 1 {
                    return base?.parseByType()
                } else {
//                    KLCLog(message: "不明错误")
                    throw KLCNetError(msg: base?.msg ?? "未知错误", code: base?.code ??  base?.code ?? 0)
                }
            } else {
                let base = JSONDeserializer<T>.deserializeFrom(dict: dict)
                return base
            }
            
        } catch _ as MoyaError {
            processNetStatusCode(self.statusCode)
            throw KLCNetError(msg: self.description, code: self.statusCode)
        } catch let error as KLCNetError{
            processNetStatusCode(error.code)
            throw error
        }
    }
    
    /// 解析model 数组, 从网络接收到数据解析KLCResponseModel, 再从data字段中解析出想要的数据
    func mapArrayModel<T: HandyJSON>(_ type: T.Type) throws -> [T?]? {
        // 测试数据
//        let dict = ["code" : 0, "msg" : "成功了哈", "data" : [["name" : "小黑", "age" : 1],["name" : "小白", "age" : 1.5]]] as [String : Any]
        do {
            if let dict = try self.mapJSON() as? NSDictionary {
                let base = JSONDeserializer<KLCResponseModel<T>>.deserializeFrom(dict: dict)
                if let success = base?.isSuccess, success, dict.count > 1  {
                    return base?.parseByType()
                } else {
                    throw KLCNetError(msg: base?.msg ?? "未知错误", code: base?.code ?? 0)
                }
            } else if let array = try self.mapJSON() as? NSArray {
                return JSONDeserializer<T>.deserializeModelArrayFrom(array: array)
            } else {
                return nil
            }
        } catch _ as MoyaError {
            processNetStatusCode(self.statusCode)
            throw KLCNetError(msg: self.description, code: self.statusCode)
        } catch let error as KLCNetError {
            processNetStatusCode(error.code)
            throw error
        }
    }
    
    /// 解析model, 从网络接收到数据解析不使用KLCResponseModel, 再从data字段中解析出想要的数据
    func mapObjectModel<T: HandyJSON>(_ type:T.Type) throws -> T? {
        do {
            let dict = try self.mapJSON() as? NSDictionary
            
            let base = JSONDeserializer<T>.deserializeFrom(dict: dict)
            return base
        } catch _ as MoyaError {
            processNetStatusCode(self.statusCode)
            throw KLCNetError(msg: self.description, code: self.statusCode)
        } catch {
            processNetStatusCode(self.statusCode)
            throw error
        }
    }
    
    /// 解析成字典
    func mapDict() throws -> Dictionary<String, Any>? {
        do {
            return try self.mapJSON(failsOnEmptyData:false) as? Dictionary<String, Any>
             
        } catch _ as MoyaError {
            processNetStatusCode(self.statusCode)
            throw KLCNetError(msg: self.description, code: self.statusCode)
        } catch {
            processNetStatusCode(self.statusCode)
            throw error
        }
    }
}

/// 本code 代表订单重复提交或者提交过程中单子被指派给了别人, 此时要删除缓存内容
public let KLCNetNeedDeleteErrorCode = 721

public class KLCNetError: LocalizedError {
    var msg = ""
    var code = 200
    convenience init(msg:String, code:Int) {
        self.init()
        self.msg = msg
        self.code = code
    }
}

extension KLCNetError {
    var errorDescription: String {
        return msg
    }
    var localizedDescription: String {
        return msg
    }
}

public extension Int {
    static let loginCode = 401
    static let noNetwork = -1009
    static let unConnectHost = -1003
}

public func KLCErrorDescription(_ error: Error) -> String {
    if let error = error as? KLCNetError {
        if  error.code == .loginCode {
            return error.msg
        }
        return error.errorDescription
    }
    
    if let error = error as? MoyaError {
        switch error {
        case .underlying(let error, _):
            if (error as NSError).code == .noNetwork || (error as NSError).code == .unConnectHost {
                return "网络开小差了，稍后再试试吧~"
            }
        default:
            return error.localizedDescription
        }
    }
    return error.localizedDescription
}



public func KLCErrorCode(_ error:Error) -> Int {
    if let error = error as? KLCNetError {
        return error.code
    }
    
    if let error = error as? MoyaError {
        switch error {
        case .underlying(let error, _):
            return (error as NSError).code
        default:
            return error.response?.statusCode ?? -1
        }
    }
    return -1
}
