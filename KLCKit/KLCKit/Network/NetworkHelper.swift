//
//  NetworkHelper.swift
//  InstallManage
//
//  Created by knight on 2018/1/17.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
import UIKit

/// app 的后段标识 用于Http Header
public var KLCAppType = "type"
public var KLCRuntimeEnvType = "Appstore"

public var KLCHost = "http://apac-ar-test.herokuapp.com"
public var KLCBaseUrl = KLCHost + "/api"
public var KLCNewBaseUrl = "http://apac-ar-test.herokuapp.com"
public var KLCForceAdUrl = "http://kli.com/awesome"
public var KLCFileBaseUrl = "http://file.kli.com"

public enum KLCRequstResultStatus {
/// 请求成功
    case success
    /// 请求失败
    case failed
    /// 没网
    case noNetwork
    /// 正在请求
    case requesting
    
    public var desc:String {
        switch self {
        case .failed:
            return "请求失败,请重试"
        case .noNetwork:
            return "目前没有网络"
        default:
            return ""
        }
    }
}

let kTicket = "kTicket"

public func getTicket() -> String? {
    return UserDefaults.standard.string(forKey: kTicket)
}

//public func getUserAgent() -> String {
//    let phoneVersion = UIDevice.current.systemVersion
//    let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
//    let platform = getPlatform()
//    let screenSize = getScreen()
//    let deviceUUID = (UIDevice.current.identifierForVendor?.uuidString)!// "readUDID()"
//    let userAgent = "MOBILE|iOS|\(phoneVersion)|\(KLCAppType)|\(currentVersion)|\(platform)|\(screenSize)|\(deviceUUID)"
//    return userAgent
//}

/// 客户端类型|操作系统|操作系统版本号|产品|客户端版本号|手机品牌|手机型号|屏幕分辨率|IMEI|渠道代码
public func klicenUserAgent() -> [String : String] {
    let phoneVersion = UIDevice.current.systemVersion
    let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    let platform = getPlatform()
    let screenSize = getScreen()
    let deviceUUID = (UIDevice.current.identifierForVendor?.uuidString)!// "readUDID()"
    let userAgent = "MOBILE|iOS|\(phoneVersion)|\(KLCAppType)|\(currentVersion)|iPhone|\(platform)|\(screenSize)|\(deviceUUID)|1"
    let traceId = RandomString.shared.getRandomStringOfLength(length: 32)
    return ["Klicen-Agent" : userAgent, "X-TRACE-ID" : traceId]
}

private func getPlatform() -> String {
    var size = 0
    sysctlbyname("hw.machine", nil, &size, nil, 0)
    var machine = [CChar](repeating: 0,  count: Int(size))
    sysctlbyname("hw.machine", &machine, &size, nil, 0)
    return String(cString: machine)
}

private func getScreen() -> String {
    let dotHeight = UIScreen.main.bounds.size.height
    let dotWidth = UIScreen.main.bounds.size.width
    let scale = UIScreen.main.scale
    let width = dotWidth * scale
    let height = dotHeight * scale
    return "\(width)*\(height)"
}
