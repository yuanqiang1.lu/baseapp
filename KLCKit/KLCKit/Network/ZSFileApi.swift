//
//  ZSFileApi.swift
//  ZEUS
//
//  Created by wang zhenqi on 2018/10/12.
//  Copyright © 2018 Goswift iOS team All rights reserved.
//

import UIKit
import Moya

enum ZSFileApi: TargetType {
    case upload(file:Data, name:String)
}

extension ZSFileApi {
    var baseURL: URL {
        return URL(string: KLCBaseUrl)!
    }
    
    var path: String {
        return "/common/file/upload/"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var task: Task {
        switch self {
        case .upload(let base64, let name):
            return .uploadCompositeMultipart([MultipartFormData(provider: .data(base64), name:"file", fileName:name, mimeType:"image/*" )],
                                             urlParameters: [:])
        }
    }
    
    public var parameterEncoding:ParameterEncoding {
        return URLEncoding.default
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
}
