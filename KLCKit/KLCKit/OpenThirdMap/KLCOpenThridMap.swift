//
//  KLCOpenThridMap.swift
//  KLCKit
//
//  Created by Kun Huang on 2019/4/15.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

import UIKit
import CoreLocation

public class KLCOpenThirdMap: NSObject {
    
    public enum NavType {
        case walk // 步行
        case car // 驾车
    }
    
    public struct ThridMap {
        public var name:String
        public var type:OpenMapType
        public var schemeUrl:String
    }
    
    public enum OpenMapType:String {
        case amap = "iosamap://" // 高德地图
        case tencent = "qqmap://" // 腾讯地图
        case baidu = "baidumap://" // 百度地图
        case google = "comgooglemaps://" // 谷歌地图
        case apple = "https://maps.apple.com/" // 苹果地图
    }
    
    // 打开三方导航
    public class func openMap(mapType:OpenMapType,
                              addr:String,
                              location:CLLocationCoordinate2D,
                              appName:String,
                              navType:NavType) {
    
        var url = "\(mapType.rawValue)"
        switch mapType {
        case .amap:
            let type = navType == .walk ? 2 : 0
            url = url + "path?sourceApplication=\(appName)&did=BGVIS2&dlat=\(location.latitude)&dlon=\(location.longitude)&dev=0&t=\(type)"
        case .tencent:
            let type = navType == .walk ? "walk" : "drive"
            let coord = "\(location.latitude),\(location.longitude)"
            url = url + "qqmap://map/routeplan?from=我的位置&type=\(type)&tocoord=\(coord)&coord_type=1&policy=0"
        case .baidu:
            let type = navType == .walk ? "walking" : "driving"
            let coord = "\(location.latitude),\(location.longitude)"
            url = url + "map/direction?origin={{我的位置}}&destination=latlng:\(coord)|name=\(addr)&mode=\(type)&coord_type=gcj02"
        case .google:
            let type = navType == .walk ? "walking" : "driving"
            url = url + "?saddr=\(addr)daddr=%@&directionsmode=\(type)"
        case .apple:
            let coord = "\(location.latitude),\(location.longitude)"
            url = url + "?daddr=\(coord)&saddr=Current+Location"
        }
    
        url = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? mapType.rawValue
        
        if let navUrl = URL(string: url) {
            UIApplication.shared.openURL(navUrl)
        }
    }
    
    // 获取app上的所有地图
    public class func getInAppAllMap() -> [ThridMap] {

        var array = [ThridMap]()
        if isAppInstalled(type: .amap) {
            let model = ThridMap(name: "高德地图", type: .amap, schemeUrl: OpenMapType.amap.rawValue)
            array.append(model)
        }
        
        if isAppInstalled(type: .baidu) {
            let model = ThridMap(name: "百度地图", type: .baidu, schemeUrl: OpenMapType.baidu.rawValue)
            array.append(model)
        }
        
        if isAppInstalled(type: .tencent) {
            let model = ThridMap(name: "腾讯地图", type: .tencent, schemeUrl: OpenMapType.tencent.rawValue)
            array.append(model)
        }
        
        if isAppInstalled(type: .google) {
            let model = ThridMap(name: "谷歌地图", type: .google, schemeUrl: OpenMapType.google.rawValue)
            array.append(model)
        }
        
        let model = ThridMap(name: "苹果地图", type: .apple, schemeUrl: OpenMapType.apple.rawValue)
        array.append(model)
        
        return array
    }
    
    // 判断对应app是否安装
    private class func isAppInstalled(type:OpenMapType) -> Bool {
        if let url = URL(string: type.rawValue) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
    
    public static func showNavMapsSheet(address:String, coor:CLLocationCoordinate2D, type:NavType = .car) {
        let maps = getInAppAllMap()
        if maps.isEmpty {
            CurrentViewController?.showToast("请安装第三方导航地图后使用", duration: 1.0)
            return
        }
    
        KLCPopupView.showSheet(title: "请选择导航地图",
                               actionTitles: maps.map({$0.name}),
                               cancelTitle: "取消",
                               actonBlock: { (_, index, mapName) in
            openMap(mapType: maps[index].type, addr: address, location: coor, appName: mapName ?? "", navType: type)
        }, cancelBlock: nil)
    }
}
