//
//  KLCPopupView.swift
//  KLCKit
//
//  Created by Kun Huang on 2019/5/14.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

import UIKit
import FWPopupView

public class KLCPopupView {
    
    // action 样式
    public enum ActionType {
        case normal
        case destructive
    }
    
    private var items = [FWPopupItem]()
    private var alertView:FWAlertView?
    
    // 添加action
    public func addAction(title:String,type:ActionType,complete:FWPopupItemClickedBlock?) {
        let action = FWPopupItem(title: title, itemType: .normal, isCancel: true, canAutoHide: true, itemClickedBlock: complete)
        switch type {
        case .normal:
            action.itemTitleFont = FONT(16)
            action.itemTitleColor = UIColor.hexStringToColor(hexString: "#9197A3")
        case .destructive:
            action.itemTitleFont = FONT(16,weight: .medium)
            action.itemTitleColor = UIColor.hexStringToColor(hexString: "#437AEB")
        }
        items.append(action)
    }
    
    // 展示alert
    public func show(title:String?,detail:String?) {
        alertView = FWAlertView.alert(title: title, detail: detail, inputPlaceholder: nil, keyboardType: .default, isSecureTextEntry: false, customView: nil, items: items, vProperty: KLCPopupView.vProperty())
        alertView?.show()
    }
    
    // 隐藏alert
    public func hide(didDisappear:(() -> Void)?) {
        alertView?.hide(popupDidDisappearBlock: { (_) in
            didDisappear?()
        })
    }
    
    // 只有一个action
    public class func showAlert(title:String?,detail:String?,confirm:String?,confirmBlock: (() -> Void)?) {
        showAlert(title: title, detail: detail, cancel: nil, confirm: confirm, cancelBlock: nil, confirmBlock: confirmBlock)
    }
    
    // 两个action
    public class func showAlert(title:String?,
                                detail:String?,
                                cancel:String?,
                                confirm:String?,
                                cancelBlock: (() -> Void)?,
                                confirmBlock: (() -> Void)?) {
        
        var items = [FWPopupItem]()
        if let cancelTitle = cancel, !cancelTitle.isEmpty {
            let action = FWPopupItem(title: cancelTitle,
                                     itemType: .normal,
                                     isCancel: true,
                                     canAutoHide: true,
                                     itemClickedBlock: {(_,_,_) in
                
                                        cancelBlock?()
            })
            action.itemTitleFont = FONT(16)
            action.itemTitleColor = UIColor.hexStringToColor(hexString: "#9197A3")
            items.append(action)
        }
        
        if let confirmTitle = confirm, !confirmTitle.isEmpty {
            let action = FWPopupItem(title: confirmTitle,
                                     itemType: .normal,
                                     isCancel: true,
                                     canAutoHide: true,
                                     itemClickedBlock: {(_,_,_) in
                
                                        confirmBlock?()
            })
            action.itemTitleFont = FONT(16,weight: .medium)
            action.itemTitleColor = UIColor.hexStringToColor(hexString: "#437AEB")
            items.append(action)
        }
        
        let alertView = FWAlertView.alert(title: title,
                                          detail: detail,
                                          inputPlaceholder: nil,
                                          keyboardType: .default,
                                          isSecureTextEntry: false,
                                          customView: nil, items: items,
                                          vProperty: vProperty())
        alertView.show()
    }
    
    // Sheet
    public class func showSheet(title:String?,
                                actionTitles:[String],
                                cancelTitle:String?,
                                actonBlock:FWPopupItemClickedBlock?,
                                cancelBlock:FWPopupVoidBlock?) {
        let sheetView = FWSheetView.sheet(title: title,
                                          itemTitles: actionTitles,
                                          itemBlock: actonBlock,
                                          cancenlBlock: cancelBlock,
                                          property: sheetProperty())
        sheetView.show()
    }
    
    // vProperty
    private class func vProperty() -> FWAlertViewProperty {
        let vProperty = FWAlertViewProperty()
        vProperty.titleFont = FONT(18, weight: .semibold)
        vProperty.titleColor = UIColor.hexStringToColor(hexString: "#3A3E45")
        vProperty.detailFont = FONT(16)
        vProperty.cornerRadius = 4
        vProperty.topBottomMargin = 0
        vProperty.letfRigthMargin = 24
        vProperty.commponentMargin = 12
        vProperty.buttonHeight = 47
        vProperty.buttonFont = FONT(16)
        vProperty.itemNormalColor = UIColor.hexStringToColor(hexString: "#3A3E45")
        vProperty.detailColor = UIColor.hexStringToColor(hexString: "#616770")
        vProperty.maskViewColor = UIColor(white: 0, alpha: 0.5)
        return vProperty
    }
    
    private class func sheetProperty() -> FWSheetViewProperty {
        let vProperty = FWSheetViewProperty()
        vProperty.titleFont = FONT(14)
        vProperty.titleColor = UIColor.hexStringToColor(hexString: "#9197A3")
        vProperty.backgroundColor = UIColor.hexStringToColor(hexString: "#F1F2F4")
        vProperty.buttonHeight = 52
        vProperty.buttonFont = FONT(16)
        vProperty.cancelBtnMarginTop = 16
        vProperty.itemNormalColor = UIColor.hexStringToColor(hexString: "#3A3E45")
        vProperty.maskViewColor = UIColor(white: 0, alpha: 0.5)
        return vProperty
    }
}
