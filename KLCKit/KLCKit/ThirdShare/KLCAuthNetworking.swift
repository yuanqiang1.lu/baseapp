//
//  KLCAuthNetworking.swift
//  KLCKit
//
//  Created by Kun Huang on 2019/4/3.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

// 身份验证网络请求

import UIKit

class KLCAuthNetworking {
    
    static let sharedInstance = KLCAuthNetworking()
    private let session = URLSession.shared
    
    // 网络请求返回
    typealias NetworkingResponseHandler = ([String: Any]?, URLResponse?, Error?) -> Void
    
    // 请求方式
    enum Method: String {
        case get = "GET"
        case post = "POST"
    }
    
    enum ParameterEncoding {
        case url
        case urlEncodedInURL
        case json
        
        func encode(_ urlRequest: URLRequest, parameters: [String: Any]?) -> URLRequest {
            guard let parameters = parameters else {
                return urlRequest
            }
            var mutableURLRequest = urlRequest
            switch self {
            case .url, .urlEncodedInURL:
                // 拼接字符
                func query(_ parameters: [String: Any]) -> String {
                    var components: [(String, String)] = []
                    for key in parameters.keys.sorted(by: <) {
                        if let value = parameters[key] {
                            components.append((key, "\(value)"))
                        }
                    }
                    return (components.map { "\($0)=\($1)" } as [String]).joined(separator: "&")
                }
                
                // 判断是否在URL后面拼接
                let method = Method(rawValue: mutableURLRequest.httpMethod!)
                var isEncodesParametersInURL = false
                if self == .urlEncodedInURL {
                    isEncodesParametersInURL = true
                } else {
                    isEncodesParametersInURL = method == .get
                }
                
                // 在URL后面拼接
                if isEncodesParametersInURL {
                    if var urlComponents = URLComponents(url: mutableURLRequest.url!, resolvingAgainstBaseURL: false) {
                        let encodedQuery = (urlComponents.percentEncodedQuery.map { $0 + "&" } ?? "")
                        let percentEncodedQuery = encodedQuery + query(parameters)
                        urlComponents.percentEncodedQuery = percentEncodedQuery
                        mutableURLRequest.url = urlComponents.url
                    }
                } else {
                    if mutableURLRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                        mutableURLRequest.setValue(
                            "application/x-www-form-urlencoded; charset=utf-8",
                            forHTTPHeaderField: "Content-Type"
                        )
                    }
                    mutableURLRequest.httpBody = query(parameters).data(using: .utf8, allowLossyConversion: false)
                }
            case .json:
                do {
                    let options = JSONSerialization.WritingOptions()
                    let data = try JSONSerialization.data(withJSONObject: parameters, options: options)
                    
                    mutableURLRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                    mutableURLRequest.setValue("application/json", forHTTPHeaderField: "X-Accept")
                    mutableURLRequest.httpBody = data
                } catch {
                    print(error)
                }
            }
            return mutableURLRequest
        }
    }
    
    // 请求
    func request(_ urlString: String,
                 method: Method,
                 parameters: [String: Any]? = nil,
                 encoding: ParameterEncoding = .url,
                 headers: [String: String]? = nil,
                 completionHandler: @escaping NetworkingResponseHandler) {
        
        guard let url = URL(string: urlString) else {
            return
        }
        var mutableURLRequest = URLRequest(url: url)
        mutableURLRequest.httpMethod = method.rawValue
        if let headers = headers {
            for (headerField, headerValue) in headers {
                mutableURLRequest.setValue(headerValue, forHTTPHeaderField: headerField)
            }
        }
        
        let request = encoding.encode(mutableURLRequest, parameters: parameters)
        let task = session.dataTask(with: request) { data, response, error in
            var json: [String: Any]?
            
            defer {
                completionHandler(json, response, error)
            }
            
            guard
                let validData = data,
                let jsonData = try? JSONSerialization.jsonObject(with: validData,
                                                                 options: .allowFragments) as? [String: Any] else {
                    print("requet failt: JSON could not be serialized because input data was nil.")
                    return
            }
            json = jsonData
        }
        task.resume()
    }
}
