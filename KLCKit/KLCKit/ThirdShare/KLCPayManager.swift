//
//  KLCPayManager.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/4/2.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import KLCModule

public class KLCPayManager:NSObject {
    
    private var scheme = ""
    private var appId = ""
    
    public static let manager = KLCPayManager()
    private override init() {}
    
    // 支付状态
    public enum KLCPayStatus {
        case success // 支付成功
        case failed // 支付失败
        case cancel // 取消支付
    }

    // 注册
    public class func register(appId:String, appURLScheme:String, isDebug:Bool) {
        manager.scheme = appURLScheme
        manager.appId = appId
        // 如果有appId 配置，一般是后台配置，app 端不用配置
        if !appId.isEmpty {
            Pingpp.setAppId(appId)
        }
        // 打开日志
        Pingpp.setDebugMode(isDebug)
        // 返回商户按钮
        Pingpp.ignoreResultUrl(true)
    }
    
    /** 创建支付
     charge JSON 格式字符串 或 NSDictionary
     viewCotroller 银联渠道需要
     */
    public class func create(charge:NSObject,
                             viewController:UIViewController,
                             completion:@escaping (_ status:KLCPayStatus,_ msg:String?)->Void) {
        Pingpp.createPayment(charge,
                             viewController: viewController,
                             appURLScheme: manager.scheme) { (result, error) in
            // 支付成功
            if result == "success" {
                completion(.success,"支付成功")
            } else {
                // 支付失败或者取消
                if let code = error?.code, code == .errCancelled { // 支付取消
                    completion(.cancel,result)
                } else { // 支付失败
                    completion(.failed,result)
                }
            }
        }
    }
    
    // 回调结果接口处理
    public class func hanleOpen(url:URL,
                                completion:@escaping (_ status:KLCPayStatus,_ msg:String?)->Void) -> Bool {
        return Pingpp.handleOpen(url) { (result, error) in
            // 支付成功
            if result == "success" {
                completion(.success,"支付成功")
            } else {
                // 支付失败或者取消
                if let code = error?.code, code == .errCancelled { // 支付取消
                    completion(.cancel,result)
                } else { // 支付失败
                    completion(.failed,result)
                }
            }
        }
    }
    
}
