//
//  KLCThirdAuth.swift
//  KLCKit
//
//  Created by Kun Huang on 2019/4/3.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

// 三方登录验证

import UIKit
import MonkeyKing

// 微信请求用户信息接口
// http://mp.weixin.qq.com/wiki/home/index.html
private let weixinUserInfoAPI = "https://api.weixin.qq.com/sns/userinfo"

// QQ请求用户信息接口
// http://wiki.open.qq.com/wiki/website/API%E5%88%97%E8%A1%A8
private let qqUserInfoAPI = "https://graph.qq.com/user/get_user_info"

// 微博获取用户信息接口
// http://open.weibo.com/wiki/%E5%BE%AE%E5%8D%9AAPI
private let weiboUserInfoAPI = "https://api.weibo.com/2/users/show.json"

public class KLCThirdAuth {
    
    public enum WeixinResult {
        /// 获取用户信息成功
        case userInfoSuccess(user:KLCWeixinUserModel)
        /// 获取的是code
        case codeSuccess(code:String)
        /// 失败
        case failure(msg:String)
    }

    // 微信验证， 没有AppSecret时候获取的是code
    public class func authWeixin(completion:@escaping (_ result:WeixinResult)->Void) {
        
        // 登录验证获取 openid token
        MonkeyKing.oauth(for: .weChat) { (info, respone, error) in
            if let _ = error { // 报错
                completion(.failure(msg:(error as NSError?)?.domain ?? "授权失败！"))
                return
            }
            
            guard
                let token = info?["access_token"] as? String,
                let openID = info?["openid"] as? String,
                let refreshToken = info?["refresh_token"] as? String,
                let expiresIn = info?["expires_in"] as? Int else {
                    
                    // 判断是否获取到了code
                    if let code = info?["code"] as? String {
                        completion(.codeSuccess(code: code))
                    } else {
                        // 失败
                        completion(.failure(msg: (error as NSError?)?.domain ?? "授权失败！"))
                    }
                    return
            }
     
            let parameters = ["openid": openID,
                              "access_token": token]
            
            // 请求用户信息
            KLCAuthNetworking.sharedInstance.request(weixinUserInfoAPI, method: .get,
                                                     parameters: parameters,
                                                     completionHandler: { (userInfo, response, error) in
                                                        
                print("userInfo \(String(describing: userInfo))")
                guard var userInfo = userInfo else {
                    completion(.failure(msg: (error as NSError?)?.domain ?? "获取用户信息失败！"))
                    return
                }
                
                userInfo["access_token"] = token
                userInfo["openid"] = openID
                userInfo["refresh_token"] = refreshToken
                userInfo["expires_in"] = expiresIn
                
                if let model = KLCWeixinUserModel.deserialize(from: userInfo) {
                    completion(.userInfoSuccess(user: model))
                } else {
                    // 失败
                    completion(.failure(msg: "获取用户信息失败！"))
                }
            })
            
        }
    }
    
    // QQ验证
    public class func authQQ(completion:@escaping (_ userModel:KLCQQUserModel?)->Void) {
        
        // 登录验证获取 openid access_token
        MonkeyKing.oauth(for: .qq, scope: "get_user_info") { (info, response, error) in
            guard
                let unwrappedInfo = info,
                let token = unwrappedInfo["access_token"] as? String,
                let openID = unwrappedInfo["openid"] as? String else {
                    completion(nil)
                    return
            }
            let parameters = [
                "openid": openID,
                "access_token": token,
                "oauth_consumer_key": KLCThridAccountConfig.config.qqConfig.appId
            ]
            
            // 请求用户信息
            KLCAuthNetworking.sharedInstance.request(qqUserInfoAPI,
                                                     method: .get,
                                                     parameters: parameters) { (userInfo, _, _) in
                                                        
                print("userInfo \(String(describing: userInfo))")
                guard var userInfo = userInfo else {
                    completion(nil)
                    return
                }
                
                userInfo["access_token"] = token
                userInfo["openid"] = openID
                let model = KLCQQUserModel.deserialize(from: userInfo)
                completion(model)
            }
        }
    }
    
    // 微博验证
    public class func authWeibo(completion:@escaping (_ userModel:KLCWeiboUserModel?)->Void) {
        
        // 登录验证获取 uid access_token
        MonkeyKing.oauth(for: .weibo) { (info, response, error) in
            guard
                let unwrappedInfo = info,
                let token = (unwrappedInfo["access_token"] as? String) ??
                    (unwrappedInfo["accessToken"] as? String),
                let userID = (unwrappedInfo["uid"] as? String) ??
                    (unwrappedInfo["userID"] as? String) else {
                        
                    return
            }
   
            let parameters = [
                "uid": userID,
                "access_token": token
            ]
            
            // 请求用户信息
            KLCAuthNetworking.sharedInstance.request(weiboUserInfoAPI,
                                                     method: .get,
                                                     parameters: parameters) { (userInfo, _, _) in
                                                        
                print("userInfo \(String(describing: userInfo))")
                guard var userInfo = userInfo else {
                    completion(nil)
                    return
                }
                
                userInfo["access_token"] = token
                userInfo["uid"] = userID
                let model = KLCWeiboUserModel.deserialize(from: userInfo)
                completion(model)
            }
        }
    }
}
