//
//  KLCThirdAuthModel.swift
//  KLCKit
//
//  Created by Kun Huang on 2019/4/3.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

import UIKit

public class KLCWeixinUserModel: KLCBaseModel {
    public var refresh_token = ""
    public var access_token = ""
    public var expires_in = ""
    public var unionid = ""
    public var openid = ""
    public var country = "" // 国家
    public var province = "" // 省份
    public var city = "" // 城市
    public var nickname = "" // 昵称
    public var head_img_url = "" // 头像
    public var sex = 0 // 1为男性，2为女性
    
    /// code，没有配置secret时只会返回code
    public var code = ""
}

public class KLCQQUserModel: KLCBaseModel {
    public var access_token = ""
    public var openid = ""
    public var nickname = ""
    public var figureurl_2 = "" // 大小为100×100像素的QQ空间头像URL
    public var gender = "" // 性别。 如果获取不到则默认返回"男"
    public var is_yellow_vip = 0 // 标识用户是否为黄钻用户（0：不是；1：是）
    public var yellow_vip_level = 0 // 黄钻等级
    public var province = "" // 省份
    public var city = "" // 城市
}

public class KLCWeiboUserModel: KLCBaseModel {
    public var id = 0 // 用户UID
    public var screen_name = "" // 用户昵称
    public var location = "" // 用户所在地
    public var description = "" // 用户个人描述
    public var profile_url = "" // 用户的微博统一URL地址
    public var gender = "" // 性别，m：男、f：女、n：未知
}
