//
//  KLCThirdShare.swift
//  KLCKit
//
//  Created by Kun Huang on 2019/4/3.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

// 三方分享

import UIKit
import MonkeyKing

public class KLCThirdShare {
    // 分享类型
     public enum ShareType {
        // 微信
        public enum WeixinType {
            case session // 微信会话
            case timeline // 微信朋友圈
            case favorite // 微信收藏
        }
        // qq
        public enum QQType {
            case friends
            case zone
            case favorite
            case dataline
        }
        
        case weixin(WeixinType) // 微信
        case qq(QQType) // QQ
        case weibo(accessToken:String) // 微博
        case system(viewController:UIViewController) // 系统
    }
    
    // 分享内容
    public enum Message {
        case url(URL)
        case image(UIImage)
        case gif(Data)
        case video(URL)
    }
    
    // 分享
    public enum ShareResult {
        case success // 成功
        case failure // 失败
    }
    
    // 三方分享调用接口
    public class func share(type:ShareType,
                     title:String? = nil,
                     desc:String? = nil,
                     thumbnail:UIImage? = nil,
                     message:Message,
                     completion:@escaping (_ result:ShareResult) -> Void) {
        
        // 配置 Media
        var media:MonkeyKing.Media?
        switch message {
        case .url(let url):
            media = MonkeyKing.Media.url(url)
        case .image(let image):
            media = MonkeyKing.Media.image(image)
        case .gif(let gifData):
            media = MonkeyKing.Media.gif(gifData)
        case .video(let url):
            media = MonkeyKing.Media.video(url)
        }
        
        // 配置info
        let info = MonkeyKing.Info(
            title:title,
            description:desc,
            thumbnail:thumbnail,
            media:media)
        
        // 分享
        switch type {
        case .weixin(let subType):
            shareWeixin(subType: subType, info: info, completion: completion)
        case .qq(let subType):
            shareQQ(subType: subType, info: info, completion: completion)
        case .weibo(let accessToken):
            shareWeibo(info: info,accessToken:accessToken,completion: completion)
        case .system(let viewController):
            self.shareSystem(info: info, viewController: viewController, completion: completion)
        }
        
    }
    
    // 分享到微信
    private class func shareWeixin(subType:ShareType.WeixinType,
                                   info:MonkeyKing.Info,
                                   completion:@escaping (_ result:ShareResult) -> Void) {
        
        var message: MonkeyKing.Message?
        switch subType {
        case .session:
            message = MonkeyKing.Message.weChat(.session(info: info))
        case .timeline:
            message = MonkeyKing.Message.weChat(.timeline(info: info))
        case .favorite:
            message = MonkeyKing.Message.weChat(.favorite(info: info))
        }
        if let message = message {
            MonkeyKing.deliver(message) { (result) in
                switch result {
                case .success(_):
                    completion(.success)
                case .failure(_):
                    completion(.failure)
                }
            }
        }
    }
    
    // 分享到QQ
    private class func shareQQ(subType:ShareType.QQType,
                               info:MonkeyKing.Info,
                               completion:@escaping (_ result:ShareResult) -> Void) {
        var message: MonkeyKing.Message?
        switch subType {
        case .friends:
            message = MonkeyKing.Message.qq(.friends(info: info))
        case .zone:
            message = MonkeyKing.Message.qq(.zone(info: info))
        case .favorite:
            message = MonkeyKing.Message.qq(.favorites(info: info))
        case .dataline:
            message = MonkeyKing.Message.qq(.dataline(info: info))
        }
        if let message = message {
            MonkeyKing.deliver(message) { (result) in
                switch result {
                case .success(_):
                    completion(.success)
                case .failure(_):
                    completion(.failure)
                }
            }
        }
    }
    
    // 分享到微博
    private class func shareWeibo(info:MonkeyKing.Info,
                                 accessToken:String,
                                 completion:@escaping (_ result:ShareResult) -> Void) {
        let message = MonkeyKing.Message.weibo(.default(info: info, accessToken: accessToken))
        MonkeyKing.deliver(message) { (result) in
            switch result {
            case .success(_):
                completion(.success)
            case .failure(_):
                completion(.failure)
            }
        }
    }
    
    // 调用系统的
    private class func shareSystem(info:MonkeyKing.Info,
                                   viewController:UIViewController ,
                                   completion:@escaping (_ result:ShareResult) -> Void) {
        
        var activityItems = Array<Any>()

        // 标题
        if let title = info.title,!title.isEmpty {
            activityItems.append(title)
        }
        
        // 内容
        if let media = info.media {
            switch media {
            case .url(let link):
                activityItems.append(link)
            case .image(let image):
                if let data = image.jpegData(compressionQuality: 0.5) {
                    activityItems.append(data)
                }
            case .gif(let gitData):
                activityItems.append(gitData)
            case .video(let url):
                activityItems.append(url)
            default:
                break
            }
        }
        
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityVC.completionWithItemsHandler = {(activityType,completed,_,error) in
            
            if completed {
                if error == nil {
                    print("分享成功")
                    completion(.success)
                } else {
                    print("分享失败")
                    completion(.failure)
                }
            }
        }
        viewController.present(activityVC, animated: true, completion: nil)
    }
    
}
