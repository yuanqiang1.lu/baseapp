//
//  KLCThridAccountConfig.swift
//  KLCKit
//
//  Created by Kun Huang on 2019/4/3.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

// 三方登录、分享配置

import UIKit
import MonkeyKing

public class KLCThridAccountConfig:NSObject {
    
    public struct Weixin {
        public var appId = ""
        public var appKey:String?
        public var miniAppID:String?
        
        @inlinable public init(_ appId:String, _ appKey:String? = nil, _ miniAppID:String? = nil) {
            self.appId = appId
            self.appKey = appKey
            self.miniAppID = miniAppID
        }
    }
    
    public struct QQ {
        public var appId = ""
        
        @inlinable public init(_ appId:String) {
            self.appId = appId
        }
    }
    
    public struct Weibo {
        public var appId = ""
        public var appKey = ""
        public var redirectURL = ""
        
        @inlinable public init(_ appId:String, _ appKey:String, _ redirectURL:String) {
            self.appId = appId
            self.appKey = appKey
            self.redirectURL = redirectURL
        }
    }
    
    // 注册前先配置
    public var weixinConfig = Weixin("","")
    public var qqConfig = QQ("")
    public var weiboConfig = Weibo("","","")
    
    public static let config = KLCThridAccountConfig()
    private override init() {}
    
    // 微信是否安装
    public class func isWeixinInstalled() -> Bool {
        return MonkeyKing.SupportedPlatform.weChat.isAppInstalled
    }
    
    // qq是否安装
    public class func isQQInstalled() -> Bool {
        return MonkeyKing.SupportedPlatform.qq.isAppInstalled
    }
    
    // 微博是否安装
    public class func isWeiboInstalled() -> Bool {
        return MonkeyKing.SupportedPlatform.weibo.isAppInstalled
    }
    
    // 传入json进行配置
    public class func register(json:[String:Any]) {
        /*
        {
            "weixin": {
                "appId": "134434",
                "appKey": "111111"
            },
            "qq": {
                "appId": ""
            },
            "weibo": {
                "appId": "134434",
                "appKey": "111111",
                "redirectURL": "url"
            }
        }
         */
        let weixin = json["weixin"] as? [String:String]
        let qq = json["qq"] as? [String:String]
        let weibo = json["weibo"] as? [String:String]
        
        // 微信
        if let weixin = weixin {
            let appId = weixin["appId"] ?? ""
            let appKey = weixin["appKey"]
            config.weixinConfig = Weixin(appId, appKey, nil)
            registerWeixin()
        }
        
        // qq
        if let qq = qq {
            let appId = qq["appId"] ?? ""
            config.qqConfig = QQ(appId)
            registerQQ()
        }
        
        // 微博
        if let weibo = weibo {
            let appId = weibo["appId"] ?? ""
            let appKey = weibo["appKey"] ?? ""
            let redirectURL = weibo["redirectURL"] ?? ""
            config.weiboConfig = Weibo(appId, appKey, redirectURL)
            registerWeibo()
        }
    }
    
    // 注册微信
    public class func registerWeixin() {
        let account = MonkeyKing.Account.weChat(appID: config.weixinConfig.appId,
                                                appKey: config.weixinConfig.appKey,
                                                miniAppID: config.weixinConfig.miniAppID)
        MonkeyKing.registerAccount(account)
    }
    
    // 注册qq
    public class func registerQQ() {
        let account = MonkeyKing.Account.qq(appID: config.qqConfig.appId)
        MonkeyKing.registerAccount(account)
    }
    
    // 注册微博
    public class func registerWeibo() {
        let account = MonkeyKing.Account.weibo(appID: config.weiboConfig.appId,
                                               appKey: config.weiboConfig.appKey,
                                               redirectURL: config.weiboConfig.redirectURL)
        MonkeyKing.registerAccount(account)
    }
    
    // 回调处理
    public class func handleOpenUrl(_ url:URL) -> Bool {
        return MonkeyKing.handleOpenURL(url)
    }
    
    // 打开微信
    public class func openWeixin() {
        if !isWeixinInstalled() {
            return
        }
        if let url = URL(string: "weixin://") {
            UIApplication.shared.openURL(url)
        }
    }
    
    // 打开QQ
    public class func openQQ() {
        if !isQQInstalled() {
            return
        }
        if let url = URL(string: "mqqapi://") {
            UIApplication.shared.openURL(url)
        }
    }
    
    // 打开微博
    public class func openWeibo() {
        if !isWeiboInstalled() {
            return
        }
        if let url = URL(string: "weibosdk://") {
            UIApplication.shared.openURL(url)
        }
    }

}
