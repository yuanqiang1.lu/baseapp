//
//  Const.swift
//  LXTKit
//
//  Created by wang zhenqi on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import Foundation

public let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
public let SCREEN_WIDTH = UIScreen.main.bounds.size.width
public let StatusBarHeight = UIApplication.shared.statusBarFrame.size.height
public let NavBarHeight = CGFloat(44)

public func DINFONT(_ fontSize: Float) -> UIFont {return UIFont.init(name: "DIN Alternate", size: CGFloat(fontSize)) ?? FONT_BOLD(fontSize)}
public func FONT(_ fontSize: Float,weight:UIFont.Weight = .regular) -> UIFont {return UIFont.systemFont(ofSize: CGFloat(fontSize), weight: weight)}
public func ScaleFONT(_ fontSize: CGFloat,weight:UIFont.Weight = .regular) -> UIFont {return UIFont.systemFont(ofSize: KScaleH(fontSize), weight: weight)}
public func FONT_BOLD(_ fontSize: Float) -> UIFont {return UIFont.boldSystemFont(ofSize: CGFloat(fontSize))}

public func KScaleH(_ c: CGFloat) -> CGFloat {return SCREEN_WIDTH / 375.0 * c}

public func KScaleV(_ c: CGFloat) -> CGFloat {return SCREEN_WIDTH / 667.0 * c}

public var docPath: String = NSHomeDirectory() + "/Documents"

public struct Platform {
    public static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
}
