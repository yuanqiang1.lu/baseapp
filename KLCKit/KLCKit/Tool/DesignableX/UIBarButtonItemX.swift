//
//  UIBarButtonItemX.swift
//  ZEUS
//
//  Created by Kun Huang on 2018/9/5.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import UIKit

class UIBarButtonItemX: UIBarButtonItem {
    
    struct UIBarButtonItemConfiguration {
        var numberBackColor = UIColor.white
        var numberTextColor = UIColor.red
        var numberFont = UIFont.systemFont(ofSize: 11)
        var numberRadius:CGFloat = 8.5
        var image:UIImage?
        var offset = CGPoint.zero
    }
    
    private var numberLabel:UILabel?
    
    public var number = 0 {
        didSet {
            numberLabel?.isHidden = number == 0
            numberLabel?.text = "\(number)"
        }
    }
    
    public convenience init(configuration:UIBarButtonItemConfiguration, target:Any, action:Selector) {
        let btn = UIButton(type: .custom)
        btn.setImage(configuration.image, for: .normal)
        btn.addTarget(target, action: action, for: .touchUpInside)
        btn.sizeToFit()
        self.init(customView: btn)
        numberLabel = UILabel(frame: CGRect(x: btn.bounds.width-configuration.numberRadius+configuration.offset.x, y: -configuration.numberRadius+configuration.offset.y, width: configuration.numberRadius*2, height: configuration.numberRadius*2))
        numberLabel?.textAlignment = .center
        numberLabel?.font = configuration.numberFont
        numberLabel?.textColor = configuration.numberTextColor
        numberLabel?.backgroundColor = configuration.numberBackColor
        numberLabel?.layer.cornerRadius = configuration.numberRadius
        numberLabel?.clipsToBounds = true
        numberLabel?.isHidden = true
        btn.addSubview(numberLabel!)
    }

}
