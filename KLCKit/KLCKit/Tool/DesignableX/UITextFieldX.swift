//
//  DesignableUITextField.swift
//  SkyApp
//
//  Created by Mark Moeykens on 12/16/16.
//  Copyright © 2016 Mark Moeykens. All rights reserved.
//

import UIKit

@IBDesignable
public class UITextFieldX: UITextField {
    
    @IBInspectable public var leftImage: UIImage? {
        didSet {
            setLeftImage()
        }
    }
    
    @IBInspectable public var leftPadding: CGFloat = 0 {
        didSet {
            setLeftImage()
        }
    }
    
    @IBInspectable public var rightImage: UIImage? {
        didSet {
            setRightImage()
        }
    }
    
    @IBInspectable public var rightPadding: CGFloat = 0 {
        didSet {
            setRightImage()
        }
    }
    
    @IBInspectable public var editRightPadding: CGFloat = 0  {
        didSet {
            
        }
    }
    
    @IBInspectable public var editLeftPadding: CGFloat = 0 {
        didSet {
            setRightImage()
        }
    }
    
    @IBInspectable public var showBottomBorder: Bool = false {
        didSet {
            setBorder()
        }
    }
    
    @IBInspectable public var bottomBorderHeight: CGFloat = 0.5 {
        didSet {
            setBorder()
        }
    }
    
    @IBInspectable public var bottomBorderColor: UIColor = UIColor.hexStringToColor(hexString: "E0E0E0") {
        didSet {
            setBorder()
        }
    }
    
    @IBInspectable public var isUpdateOther: Bool = true
    
    private var _isRightViewVisible: Bool = true
    var isRightViewVisible: Bool {
        get {
            return _isRightViewVisible
        }
        set {
            _isRightViewVisible = newValue
            updateView()
        }
    }
    
    func updateView() {
        setLeftImage()
        setRightImage()
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[kCTForegroundColorAttributeName as NSAttributedString.Key: tintColor!])
    }
    
    func setLeftImage() {
        leftViewMode = UITextField.ViewMode.always
        var view: UIView
        
        if let image = leftImage {
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: image.size.width, height: image.size.height))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = tintColor
            
            var width = imageView.frame.width + leftPadding
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: imageView.bounds.size.height))
            view.addSubview(imageView)
        } else {
            view = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: 20))
        }
        
        leftView = view
        // Placeholder text color
//        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[kCTForegroundColorAttributeName as NSAttributedString.Key: tintColor!])
    }
    
    func setRightImage() {
        rightViewMode = UITextField.ViewMode.always
        
        var view: UIView
        
        if let image = rightImage, isRightViewVisible {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.width))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = tintColor
            
            var width = imageView.frame.width + rightPadding
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: image.size.width))
            view.addSubview(imageView)
            
        } else {
            view = UIView(frame: CGRect(x: 0, y: 0, width: rightPadding, height: 20))
        }
        
        rightView = view
        // Placeholder text color
//        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[kCTForegroundColorAttributeName as NSAttributedString.Key: tintColor!])
    }
    
    override public func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.size.width - (self.rightView?.bounds.size.width ?? 0) - 24,
                      y: bounds.size.height/2 - 24/2,
                      width: 24,
                      height: 24)
    }
    
    func setBorder() {
        guard showBottomBorder else {
            return
        }
        let width = self.bounds.size.width
        let selfHeight = self.bounds.size.height
        self.viewWithTag(101)?.removeFromSuperview()
        let line = UIView(frame: CGRect(x: 0, y: selfHeight - bottomBorderHeight, width: width, height: bottomBorderHeight))
        line.backgroundColor = bottomBorderColor
        self.addSubview(line)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        if let image = leftImage, isRightViewVisible {
            let left = image.size.width + leftPadding
            return CGRect(x: bounds.origin.x + editLeftPadding + left , y: 0, width: bounds.size.width - editRightPadding, height: bounds.size.height)
        }
        return CGRect(x: bounds.origin.x + editLeftPadding , y: 0, width: bounds.size.width - editRightPadding, height: bounds.size.height)
    }
    
    
//    // MARK: - Corner Radius
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            self.layer.cornerRadius = cornerRadius
//        }
//    }
}
