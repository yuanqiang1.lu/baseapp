//
//  UIViewX.swift
//  DesignableX
//
//  Created by Mark Moeykens on 12/31/16.
//  Copyright © 2016 Mark Moeykens. All rights reserved.
//

import UIKit

@IBDesignable
open class UIViewX: UIView {
    
    // MARK: - Gradient
    
    @IBInspectable public var firstColor: UIColor = UIColor.white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable public var secondColor: UIColor = UIColor.white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable public var horizontalGradient: Bool = false {
        didSet {
            updateView()
        }
    }
    
    override open class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [ firstColor.cgColor, secondColor.cgColor ]
        
        if (horizontalGradient) {
            layer.startPoint = CGPoint(x: 0.0, y: 0.5)
            layer.endPoint = CGPoint(x: 1.0, y: 0.5)
        } else {
            layer.startPoint = CGPoint(x: 0, y: 0)
            layer.endPoint = CGPoint(x: 0, y: 1)
        }
    }
    
//    // MARK: - Border
//    
//    @IBInspectable public var borderColor: UIColor = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//    
//    @IBInspectable public var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    
//    @IBInspectable public var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//        }
//    }
    
    // MARK: - Shadow
    
    @IBInspectable public var shadowOpacity: CGFloat = 0 {
        didSet {
            layer.shadowOpacity = Float(shadowOpacity)
        }
    }
    
    @IBInspectable public var shadowColor: UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable public var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable public var shadowOffsetY: CGFloat = 0 {
        didSet {
            layer.shadowOffset.height = shadowOffsetY
        }
    }
}

@IBDesignable
extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue > 0 ? newValue : 0
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
}


public extension UIView {
    func addDottedBorder(borderColor:UIColor, fillColor:UIColor? = .clear, dashPattern:[NSNumber], borderWidth:CGFloat) {
        if layer.sublayers?.map({$0.name}).contains("dottedBorder") ?? false {
            return
        }
        let dotPath = UIBezierPath.init(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius)
        let borderLayer = CAShapeLayer()
        borderLayer.name = "dottedBorder"
        
        borderLayer.path = dotPath.cgPath
        borderLayer.lineDashPattern = dashPattern
        borderLayer.fillColor = fillColor?.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        self.layer.addSublayer(borderLayer)
        
    }
    
    func removeDottedBorder() {
        layer.sublayers?.filter({$0.name == "dottedBorder"}).forEach({ (layer) in
            layer.removeFromSuperlayer()
        })
    }
    
    func addCorner(rect:CGRect,radius:CGFloat,corner:UIRectCorner) {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corner, cornerRadii: CGSize(width: radius, height: radius))
        let mskLayer = CAShapeLayer()
        mskLayer.frame = rect
        mskLayer.path = path.cgPath
        self.layer.mask = mskLayer
    }
}

public func drawDashLine(lineView : UIView,lineLength : Int ,lineSpacing : Int,lineColor : UIColor){
    let shapeLayer = CAShapeLayer()
    shapeLayer.bounds = lineView.bounds
    //        只要是CALayer这种类型,他的anchorPoint默认都是(0.5,0.5)
    shapeLayer.anchorPoint = CGPoint(x: 0, y: 0)
    //        shapeLayer.fillColor = UIColor.blue.cgColor
    shapeLayer.strokeColor = lineColor.cgColor
    
    shapeLayer.lineWidth = lineView.frame.size.height
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    
    shapeLayer.lineDashPattern = [NSNumber(value: lineLength),NSNumber(value: lineSpacing)]
    
    let path = CGMutablePath()
    path.move(to: CGPoint(x: 0, y: 0))
    path.addLine(to: CGPoint(x: lineView.frame.size.width, y: 0))
    
    shapeLayer.path = path
    lineView.layer.addSublayer(shapeLayer)
}
