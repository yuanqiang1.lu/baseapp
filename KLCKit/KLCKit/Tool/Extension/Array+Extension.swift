//
//  Array+extension.swift
//  InstallManage
//
//  Created by wang zhenqi on 2018/4/20.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation

public extension Array {
    
    /// 根据属性或者可对象去重 exp:[String]().filterDuplicates {$0}
    func filterDuplicates<E: Equatable>(_ filter: (Element) -> E) -> [Element] {
        var result = [Element]()
        for value in self {
            let key = filter(value)
            if !result.map({filter($0)}).contains(key) {
                result.append(value)
            }
        }
        return result
    }
    
    /// 安全索引
    subscript (safe index: Int) -> Element? {
        return (0..<count).contains(index) ? self[index] : nil
    }
}

