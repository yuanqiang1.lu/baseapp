//
//  CLLocationCoordinate2D+equal.swift
//  InstallManage
//
//  Created by wang zhenqi on 2018/4/20.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D: Equatable {
    public static func ==(lhs:CLLocationCoordinate2D, rhs:CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude && rhs.longitude == lhs.longitude
    }
}

