//
//  Float+Fmt.swift
//  ZEUS
//
//  Created by wang zhenqi on 2018/11/29.
//  Copyright © 2018 Goswift iOS team All rights reserved.
//

import Foundation
public extension Double {
    /// 默认小数点后两位
    func preciseDecimal(length: Int = 2) -> String {
        if self.isNaN || self.isInfinite {
            return "-"
        }
        //四舍五入
        let decimalNumberHandle : NSDecimalNumberHandler = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode(rawValue: 0)!, scale: Int16(length), raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false)
        let decimaleNumber : NSDecimalNumber = NSDecimalNumber(value: self)
        let resultNumber : NSDecimalNumber = decimaleNumber.rounding(accordingToBehavior: decimalNumberHandle)
        var formatterString : String = length == 0 ? "0" : "0."
        let count : Int = (length < 0 ? 0 : length)
        for _ in 0 ..< count {
            formatterString.append("0")
        }
        let formatter : NumberFormatter = NumberFormatter()
        formatter.positiveFormat = formatterString
        return formatter.string(from: resultNumber)!
    }
    
    func dividBy1W() -> Double {
        return self / 10000
    }
    
    func toKmOrMeter() -> String {
        if self > 1000 {
            return "\((self / 1000.0).preciseDecimal(length: 1))km"
        }
        return "\(self.preciseDecimal(length: 0))m"
    }
    
}

public extension Float {
    /// 默认小数点后两位
    func preciseDecimal(length: Int = 2) -> String {
        //四舍五入
        if self.isNaN || self.isInfinite {
        return "-"
        }
        let decimalNumberHandle : NSDecimalNumberHandler = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode(rawValue: 0)!, scale: Int16(length), raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false)
        let decimaleNumber : NSDecimalNumber = NSDecimalNumber(value: self)
        let resultNumber : NSDecimalNumber = decimaleNumber.rounding(accordingToBehavior: decimalNumberHandle)
        var formatterString : String = length == 0 ? "0" : "0."
        let count : Int = (length < 0 ? 0 : length)
        for _ in 0 ..< count {
            formatterString.append("0")
        }
        let formatter : NumberFormatter = NumberFormatter()
        formatter.positiveFormat = formatterString
        return formatter.string(from: resultNumber)!
    }
    
    func dividBy1W() -> Float {
        return self / 10000
    }
    
    func toKmOrMeter() -> String {
        if self > 1000 {
            return "\((self / 1000.0).preciseDecimal(length: 1))km"
        }
        return "\(self.preciseDecimal(length: 0))m"
    }
}
