//
//  KLCNavigationController.swift
//  KLCKit
//
//  Created by wang zhenqi on 2019/4/12.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

import UIKit

public class KLCNavigationController: UINavigationController {

    override public func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.tintColor = UIColor.black
        /**
        let color = UIColor.black
        let shadow = NSShadow(); shadow.shadowOffset = CGSize.zero
        let titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font: ScaleFONT(18.0, weight: .bold),
            NSAttributedString.Key.shadow: shadow
        ]
        navigationBar.titleTextAttributes = titleTextAttributes
         **/
    }
    
    @objc func navigationBackClick() {
        if ((self.navigationController != nil) || (self.presentedViewController != nil ) && children.count == 1) {
            _ = dismiss(animated: true, completion: nil)
            
        }else{
            _ = popViewController(animated: true)
        }
    }
    
    override public func pushViewController(_ viewController: UIViewController, animated: Bool) {
        // 不显示下一页的返回按钮title
        if !children.isEmpty {
            viewController.hidesBottomBarWhenPushed = true
        }
        CurrentViewController?.navigationItem.backBarButtonItem =
            UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(navigationBackClick))
        super.pushViewController(viewController, animated: animated)
    }

}
