//
//  File.swift
//  InstallManage
//
//  Created by Kun Huang on 2018/7/10.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation

public extension String {
    func nsRange(string:String) -> NSRange {
        if let range = self.range(of: string) {
            return NSRange(range, in: self)
        }
        return NSMakeRange(0, 0)
    }

    func substingInRange(_ r: Range<Int>) -> String? {
        if r.lowerBound < 0 || r.upperBound > self.count {
            return nil
        }
        let startIndex = self.index(self.startIndex, offsetBy:r.lowerBound)
        let endIndex   = self.index(self.startIndex, offsetBy:r.upperBound)
        return String(self[startIndex..<endIndex])
    }
    
    func cutString(rule cutStr:String, originStr sourceStr:String) -> String {
        let cutStrRang = sourceStr.nsRange(string: cutStr)
        return sourceStr.substingInRange(Range(uncheckedBounds: (cutStrRang.length, sourceStr.count))) ?? ""
    }
}

//通过对String扩展，字符串增加下表索引功能
public extension String {
    subscript(index:Int) -> String
    {
        get{
            return String(self[self.index(self.startIndex, offsetBy: index)])
        }
        set{
            let tmp = self
            self = ""
            for (idx, item) in tmp.enumerated() {
                if idx == index {
                    self += "\(newValue)"
                }else{
                    self += "\(item)"
                }
            }
        }
    }
}

public extension String {
    var localString: String {
        return NSLocalizedString(self, comment: "")
    }
}

public extension String {
    /*
     *去掉首尾空格
     */
    var removeHeadAndTailSpace:String {
        let whitespace = NSCharacterSet.whitespaces
        return self.trimmingCharacters(in: whitespace)
    }
    /*
     *去掉首尾空格 包括后面的换行 \n
     */
    var removeHeadAndTailSpacePro:String {
        let whitespace = NSCharacterSet.whitespacesAndNewlines
        return self.trimmingCharacters(in: whitespace)
    }
    /*
     *去掉所有空格
     */
    var removeAllSapce: String {
        return self.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
    }
}

public extension String {
    func findFirstLetter() -> String {
        //转变成可变字符串
        let mutableString = NSMutableString(string: self)
        
        //将中文转换成带声调的拼音
        CFStringTransform(mutableString as CFMutableString, nil, kCFStringTransformToLatin, false)
        
        //去掉声调
        let pinyinString = mutableString.folding(options: String.CompareOptions.diacriticInsensitive, locale: NSLocale.current)
        
        //将拼音首字母换成大写
//        let strPinYin = polyphoneStringHandle(nameString: self, pinyinString: pinyinString).uppercased()
        
        //截取大写首字母
        let firstString = String(pinyinString.prefix(1)).uppercased()
        
        //判断首字母是否为大写
        let regexA = "^[A-Z]$"
        let predA = NSPredicate.init(format: "SELF MATCHES %@", regexA)
        return predA.evaluate(with: firstString) ? firstString : "#"
    }
}

public class RandomString {
    let characters = "0123456789abcdefghijklmnopqrstuvwxyz"
    
    /**
     生成随机字符串,
     
     - parameter length: 生成的字符串的长度
     
     - returns: 随机生成的字符串
     */
    public func getRandomStringOfLength(length: Int) -> String {
        var ranStr = ""
        for _ in 0..<length {
            let index = Int(arc4random_uniform(UInt32(characters.count)))
            ranStr.append(characters[index])
        }
        print("-------traceId:\(ranStr)")
        return ranStr
    }
    public static let shared = RandomString()
}
