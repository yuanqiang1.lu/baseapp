//
//  UIImage+Extension.swift
//  LXTKit
//
//  Created by wang zhenqi on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import Foundation

extension UIImage {
    public static func image(_ imageBuffer:CVImageBuffer) -> UIImage {
        let ciImage = CIImage.init(cvImageBuffer: imageBuffer)
        let context = CIContext.init(options: nil)
        let cgImage = context.createCGImage(ciImage, from: CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(imageBuffer), height: CVPixelBufferGetHeight(imageBuffer)))
        let image = UIImage.init(cgImage: cgImage!)
        
        return image
    }
    
    // 初始化一个有颜色的
    public static func navBarShadowImage(color: UIColor) -> UIImage? {
        let rect: CGRect = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.5 * UIScreen.main.scale)
        UIGraphicsBeginImageContext(rect.size)
        
        color.setFill()
        UIRectFill(rect)
        
        guard let image: UIImage = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        UIGraphicsEndImageContext()
        let img = UIImage(cgImage: image.cgImage!, scale: UIScreen.main.scale, orientation: .up)
        return img
    }
    
    public static func imageFromView(view:UIView) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    // OpenGL 或 CAGradientLayer渐变用这个方法
    public static func imageFromOpenGLView(view:UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    public static func image(with color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        
        let context = UIGraphicsGetCurrentContext()!
        color.setFill()
        context.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    public static func compressImage(_ originalImage: UIImage, _ toMaxDataSizeKBytes: CGFloat) -> Data {
        var data = originalImage.jpegData(compressionQuality: 1.0)!
        var dataKBytes = CGFloat(data.count) / 1024.0
        var maxQuality = 1.0
        var lastData = dataKBytes
        var times = 0
        while dataKBytes > toMaxDataSizeKBytes && maxQuality > 0.01 {
            maxQuality = maxQuality - 0.3
            data = originalImage.jpegData(compressionQuality: CGFloat(maxQuality))!
            dataKBytes = CGFloat(data.count) / 1024.0
            times = times + 1
            if lastData == dataKBytes {
                break
            } else {
                lastData = dataKBytes
            }
            if times == 5 {
                break
            }
        }
        return data
    }
    
    public func image(tintColor:UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
        tintColor.setFill()
        let bounds = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        UIRectFill(bounds)
        self.draw(in: bounds, blendMode: .destinationIn, alpha: 1.0)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    /// 生成带文字的图片
    public static func image(text:String,size:CGSize,
                             font:UIFont=UIFont.systemFont(ofSize: 12),
                             _ backColor:UIColor=UIColor.orange,
                             _ textColor:UIColor=UIColor.white,
                             _ isCircle:Bool=true) -> UIImage? {
        let label = UILabel(frame: CGRect(origin: CGPoint.zero, size: size))
        label.font = font
        label.text = text
        label.textColor = textColor
        label.backgroundColor = backColor
        label.textAlignment = .center
        if isCircle {
            label.clipsToBounds = true
            label.layer.cornerRadius = size.width/2.0
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        label.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    /**
     *  重设图片大小
     */
    public func resizeImage( targetSize: CGSize) -> UIImage? {
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    public var hasAlphaChannel: Bool {
        if let cgImage = cgImage {
            let alphaInfo = cgImage.alphaInfo
            return alphaInfo == .first
                || alphaInfo == .last
                || alphaInfo == .premultipliedFirst
                || alphaInfo == .premultipliedLast
        } else {
            return false
        }
    }
    
    public static func createQRCodeImage(content: String,size: CGSize, color:UIColor = .black) -> UIImage {
        let stringData = content.data(using: String.Encoding.utf8)
        let qrFilter = CIFilter(name: "CIQRCodeGenerator")
        
        qrFilter?.setValue(stringData, forKey: "inputMessage")
        qrFilter?.setValue("L", forKey: "inputCorrectionLevel")
        
        let colorFilter = CIFilter(name: "CIFalseColor")
        colorFilter?.setDefaults()
        
        colorFilter?.setValuesForKeys(["inputImage" : (qrFilter?.outputImage)!,"inputColor0":CIColor.init(cgColor: color.cgColor),"inputColor1":CIColor.init(cgColor: UIColor.white.cgColor)])
        
        let qrImage = colorFilter?.outputImage
        let cgImage = CIContext(options: nil).createCGImage(qrImage!, from: (qrImage?.extent)!)
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        context!.interpolationQuality = .none
        context!.scaleBy(x: 1.0, y: -1.0)
        context?.draw(cgImage!, in: (context?.boundingBoxOfClipPath)!)
        let codeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return codeImage!
    }
    
}
