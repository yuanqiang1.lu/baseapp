//
//  UINavigationBar+Extension.swift
//  KLCKit
//
//  Created by wang zhenqi on 2019/5/8.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

import Foundation

public extension UINavigationBar {
    func hideUnderline() {
        for subview in subviews {
            if subview.isKind(of: UIImageView.self) {
                for hairline in subview.subviews {
                    if hairline.isKind(of: UIImageView.self) && hairline.bounds.height <= 1.0 {
                        hairline.isHidden = true
                        let line = hairline as? UIImageView
                        line?.image = UIImage.image(with: .clear)
                    }
                }
            }
        }
    }
}
