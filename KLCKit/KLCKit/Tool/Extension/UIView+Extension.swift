//
//  UIView+Extension.swift
//  LXTKit
//
//  Created by wang zhenqi on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import Foundation

public extension UIView {
    /// 虚线边框
    func dashBorder(width: CGFloat?, color: UIColor?, fillColor: UIColor?, duration: [NSNumber]?) {
        if layer.sublayers == nil {
            return
        }
        
        for layer in layer.sublayers! {
            if let borderLayer = layer as? CAShapeLayer{
                borderLayer.frame = bounds
                borderLayer.path = UIBezierPath(rect: borderLayer.bounds).cgPath
                borderLayer.lineDashPattern = duration ?? [1,1]
                borderLayer.fillColor = fillColor?.cgColor ?? UIColor.clear.cgColor
                borderLayer.strokeColor = color?.cgColor ?? UIColor.white.cgColor
                borderLayer.lineWidth = width ?? 0
                
                return
            }
        }
        
        let borderLayer = CAShapeLayer()
        borderLayer.frame = bounds
        borderLayer.path = UIBezierPath(rect: borderLayer.bounds).cgPath
        borderLayer.lineDashPattern = duration ?? [1,1]
        borderLayer.fillColor = fillColor?.cgColor ?? UIColor.clear.cgColor
        borderLayer.strokeColor = color?.cgColor ?? UIColor.white.cgColor
        borderLayer.lineWidth = width ?? 0
        layer.insertSublayer(borderLayer, below: layer)
    }
    
    var currentViewController: UIViewController? {
        get {
            var n = self.next
            while n != nil {
                if let controller = n as? UIViewController {
                    return controller
                }
                n = n?.next
            }
            return nil
        }
    }
}

public extension UIView {
    func startRotation(_ clockwise:Bool = false) {
        let anim = CABasicAnimation(keyPath: "transform.rotation.z")
        anim.toValue = Double.pi * 2 * (clockwise ? 1 : -1)
        anim.duration = 1
        anim.isCumulative = true
        
        anim.repeatCount = HUGE
        self.layer.add(anim, forKey: "rotationAnimation")
    }
    
    func stopAnimate() {
        self.layer.removeAnimation(forKey: "rotationAnimation")
    }
}

public extension UIView {
    func removeAllSubviews() {
        subviews.forEach { $0.removeFromSuperview()}
    }
}
