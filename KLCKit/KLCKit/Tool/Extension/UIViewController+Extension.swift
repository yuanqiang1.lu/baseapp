//
//  UIViewController+Extension.swift
//  LXTKit
//
//  Created by wang zhenqi on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import Foundation
import Toast_Swift

public var CurrentViewController: UIViewController? {
    
    return findBestViewController(UIApplication.shared.keyWindow?.rootViewController)
}

func findBestViewController(_ vc:UIViewController?) -> UIViewController? {
    guard let vc = vc else {
        return nil
    }
    if let present = vc.presentedViewController {
        return findBestViewController(present)
    } else if vc.isKind(of: UISplitViewController.classForCoder()) {
        let svc = vc as! UISplitViewController
        if svc.viewControllers.count > 0 {
            return findBestViewController(svc.viewControllers.last!)
        } else {
            return vc
        }
    } else if vc.isKind(of: UINavigationController.classForCoder()) {
        let svc = vc as! UINavigationController
        if svc.viewControllers.count > 0 {
            return findBestViewController(svc.viewControllers.last!)
        } else {
            return vc
        }
    } else if vc.isKind(of: UITabBarController.classForCoder()) {
        let svc = vc as! UITabBarController
        if (svc.viewControllers?.count)! > 0 {
            return findBestViewController(svc.selectedViewController!)
        } else {
            return vc
        }
    } else {
        return vc
    }
}


/**
 注：Alert必须写在独立的方法，因为不只是UIViewController要用
 */
public func showAlert(_ content: String) {
    showAlert(nil, content: content, cancel: nil, confirm: nil, cancelBlock: nil, confirmBlock: nil)
}

public func showAlert(_ title:String, content: String) {
    showAlert(title, content: content, cancel: nil, confirm: nil, cancelBlock: nil, confirmBlock: nil)
}

public func showAlert(_ title:String? = nil, content: String? = nil, confirmBlock: (() -> Void)?) {
    showAlert(title, content: content, cancel: nil, confirm: nil, cancelBlock: nil) {
        confirmBlock?()
    }
}

public func showAlert(_ title:String?, content: String? = nil, cancelBlock: (() -> Void)?, confirmBlock: (() -> Void)?) {
    showAlert(title, content: content, cancel: nil, confirm: nil, cancelBlock: {
        cancelBlock?()
    }) {
        confirmBlock?()
    }
}

public func showAlert(_ title:String? = nil, content: String? = nil, cancel: String?, confirm: String?,
               cancelBlock: (() -> Void)?, confirmBlock: (() -> Void)?) {
    showAlert(title, content: content, cancel: cancel, confirm: confirm, cancelBlock: cancelBlock,
              confirmBlock: confirmBlock, controller: CurrentViewController)
}

public func showAlert(_ title:String?,
                      content: String? = nil,
                      cancel: String?,
                      confirm: String?,
                      cancelBlock: (() -> Void)?,
                      confirmBlock: (() -> Void)?,
                      controller: UIViewController? = CurrentViewController,
                      titleAlgin: NSTextAlignment = .center,
                      messageAlign: NSTextAlignment = .center) {
    if let isAlert = controller?.isKind(of: UIAlertController.self), isAlert {
        return
    }
    let alert = UIAlertController.init(title: title,
                                       message: content,
                                       preferredStyle: .alert)
    
    let confirmAction = UIAlertAction(title: confirm ?? "确定",
                                      style: .default) { (action) in
                                        confirmBlock?()
    }
    alert.addAction(confirmAction)
    
    if cancelBlock != nil || cancel != nil {
        let cancelAction = UIAlertAction(title: cancel ?? "取消",
                                         style: .cancel) { (action) in
                                            cancelBlock?()
        }
        alert.addAction(cancelAction)
    }
    
    // 自定义对齐方式
    if let subview1 = alert.view.subviews.first,
        let subview2 = subview1.subviews.first,
        let subview3 = subview2.subviews.first,
        let subview4 = subview3.subviews.first,
        let subview5 = subview4.subviews.first {
        if let titleLabel = subview5.subviews.first as? UILabel {
            titleLabel.textAlignment = titleAlgin
        }
        if subview5.subviews.count > 1, let messageLabel = subview5.subviews[1] as? UILabel {
            messageLabel.textAlignment = messageAlign
        }
    }
    
    // 弹出提示框
    controller?.present(alert, animated: true)
}

//MAKR: - 提示框-ActionSheet
/**
 注：ActionSheet必须写在独立的方法，因为不只是UIViewController要用
 */
public func showActionSheet(_ title:String?, content: String?, first: String?, second: String?,
                     firstBlock: (() -> Void)?, secondBlock: (() -> Void)?, cancelBlock: (() -> Void)?) {
    showActionSheet(title, content: content, first: first, second: second, firstBlock: firstBlock,
                    secondBlock: secondBlock, cancelBlock: cancelBlock, controller: nil)
}

public func showActionSheet(_ title:String?, content: String?, first: String?, second: String?, firstBlock: (() -> Void)?,
                     secondBlock: (() -> Void)?, cancelBlock: (() -> Void)?, controller: UIViewController?) {
    let alert = UIAlertController.init(title: title,
                                       message: content,
                                       preferredStyle: .actionSheet)
    
    if first != nil && !first!.isEmpty {
        let firstAction = UIAlertAction(title: first ?? "",
                                        style: .default) { (action) in
                                            firstBlock?()
        }
        alert.addAction(firstAction)
    }
    
    if second != nil && !second!.isEmpty {
        let secondAction = UIAlertAction(title: second ?? "",
                                         style: .default) { (action) in
                                            secondBlock?()
        }
        alert.addAction(secondAction)
    }
    
    if cancelBlock != nil {
        let cancelAction = UIAlertAction(title: "取消",
                                         style: .cancel) { (action) in
                                            cancelBlock?()
        }
        alert.addAction(cancelAction)
    }
    
    if controller != nil {
        controller?.present(alert, animated: true)
    } else {
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true)
    }
}

public extension UIViewController {
    func showToast() {
        hideToast()
        view.isUserInteractionEnabled = false
        view.makeToastActivity(ToastPosition.center)
    }
    
    func showToastAlways(_ message: String) {
        hideToast()
        view.isUserInteractionEnabled = false
        view.makeToast(message, duration: 100000, position: ToastPosition.center)
    }
    
    func showToast(_ message: String, duration: TimeInterval = 3) {
        hideToast()
        view.isUserInteractionEnabled = false
        view.makeToast(message,
                       duration: duration,
                       position: ToastPosition.center,
                       title: nil,
                       image: nil) {[weak self] (isDismiss) in
                        self?.view.isUserInteractionEnabled = true
        }
    }
    
    func showToast(_ message: String, duration: TimeInterval, complete:@escaping (() -> Void)) {
        hideToast()
        view.isUserInteractionEnabled = false
        view.makeToast(message,
                       duration: duration,
                       position: ToastPosition.center,
                       title: nil,
                       image: nil) {[weak self] (isDismiss) in
                        self?.view.isUserInteractionEnabled = true
                        complete()
        }
    }
    
    func hideToast() {
        ToastManager.shared.style.fadeDuration = 0.2
        view.isUserInteractionEnabled = true
        view.hideAllToasts(includeActivity: true)
    }
}
