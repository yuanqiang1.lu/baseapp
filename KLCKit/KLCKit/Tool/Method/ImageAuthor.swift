//
//  ImageAuthor.swift
//  LXTKit
//
//  Created by wang zhenqi on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import Foundation
import Photos

public func isCameraAvailable() -> Bool {
    if !UIImagePickerController.isSourceTypeAvailable(.camera) {
        showAlert("您的设备无可用摄像头")
        return false
    }
    
    let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    if status == .denied || status == .restricted {
        KLCPopupView.showAlert(title: nil, detail: "无权访问摄像头，不能拍照", cancel: "忽略", confirm: "去设置", cancelBlock: nil) {
            ///todo:调试兼容性问题
            _ = URL(string: UIApplication.openSettingsURLString)!
            //UIApplication.shared.open(settingUrl, options: [:], completionHandler: nil)
        }
        return false
    }
    return true
}

public func phAuthorizationCheck(image:UIImage) {
    let status = PHPhotoLibrary.authorizationStatus()
    switch status {
    case .restricted, .denied:
        print("无法访问相册, 请在设置中开启相册访问功能")
//        CurrentViewController?.showToast("无法访问相册, 请在设置中开启相册访问功能", duration: 2)
    case .authorized:
        saveImageTo(image)
    case .notDetermined:
        PHPhotoLibrary.requestAuthorization({ (status) in
            if status == .authorized {
                saveImageTo(image)
            }
        })
    @unknown default:
        print("其他状态")
    }
}

public func createAssetCollection(_ name:String) -> PHAssetCollection? {
    let asset = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
    for i in 0..<asset.count {
        let assetCollection = asset.object(at: i)
        if assetCollection.localizedTitle == name {
            return assetCollection
        }
    }
    var assetId:String?
    do {
        try PHPhotoLibrary.shared().performChangesAndWait {
            assetId = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: name).placeholderForCreatedAssetCollection.localIdentifier
        }
    } catch {
        return nil
    }
    return PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [assetId!], options: nil).lastObject
}

public func saveImageTo(_ image:UIImage, assetName name:String = ""){
    
    var assetLocalId:String?
    PHPhotoLibrary.shared().performChanges({
        assetLocalId = PHAssetCreationRequest.creationRequestForAsset(from: image).placeholderForCreatedAsset?.localIdentifier
    }) { (rst, error) in
        if !rst {
            DispatchQueue.main.async(execute: {
//                CurrentViewController?.showToast("保存图片失败", duration: 1)
            })
            return
        }
        let destAsset = createAssetCollection(name)
        if destAsset == nil {
            DispatchQueue.main.async(execute: {
//                CurrentViewController?.showToast("保存图片成功,创建相册失败", duration: 1)
            })
            return
        }
        
        PHPhotoLibrary.shared().performChanges({
            let photo = PHAsset.fetchAssets(withLocalIdentifiers: [assetLocalId!], options: nil)
            let request = PHAssetCollectionChangeRequest(for: destAsset!)
            request?.addAssets(photo)
        }, completionHandler: { (rst, error) in
            DispatchQueue.main.sync {
                if rst {
                    //                        UIViewController.currentViewController?.showToast("已保存", duration: 1)
                } else {
//                    CurrentViewController?.showToast("保存失败", duration: 1)
                }
            }
            
        })
    }
}
