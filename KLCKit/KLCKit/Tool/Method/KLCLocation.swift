//
//  KLCLocation.swift
//  InstallManage
//
//  Created by knight on 2017/8/28.
//  Copyright © 2017年 Goswift iOS team All rights reserved.
//

import UIKit
import CoreLocation

public typealias KLCLocationComplete = (CLPlacemark?) -> Void

public class KLCLocation: NSObject, CLLocationManagerDelegate {
    let locationManager = CLLocationManager()
    var completeBlock: KLCLocationComplete?  // 必须谨防循环引用
    var locationBlock: ((CLLocationCoordinate2D) -> Void)?
    static public let shared = KLCLocation()
}

//MARK: - 系统定位
public extension KLCLocation {
    func startLocation(_ locationCompleteBlock: KLCLocationComplete?,
                       _ currentLocatin: ((CLLocationCoordinate2D) -> Void)? = nil) {
        completeBlock = locationCompleteBlock
        locationBlock = currentLocatin
        //设置定位服务管理器代理
        locationManager.delegate = self
        //设置定位进度
        locationManager.desiredAccuracy = kCLLocationAccuracyBest //最佳定位
        //更新距离
        locationManager.distanceFilter = 100
        //发出授权请求
//        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if (CLLocationManager.locationServicesEnabled()) {
            //允许使用定位服务的话，开始定位服务更新
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopLocation() {
        locationManager.stopUpdatingLocation()
    }
}

//MARK: - 系统定位协议
public extension KLCLocation {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            showAlert(nil,
                      content: "没有定位权限，不能正常使用此功能",
                      cancel: "忽略",
                      confirm: "去设置",
                      cancelBlock: nil,
                      confirmBlock: { 
                        let settingUrl = URL(string: UIApplication.openSettingsURLString)!
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(settingUrl, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(settingUrl)
                        }
            })
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // 获取最新的坐标
        let currLocation : CLLocation = locations.last!  // 持续更新
        locationBlock?(currLocation.coordinate)
        // 反解析地理编码
        if completeBlock != nil { // 不传这个block说明不需要反解析
            reverseGeocode(location: currLocation)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func reverseGeocode(location: CLLocation) {
        //使用坐标，获取地址
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            if error != nil {
                print("获取地址失败: \(error!.localizedDescription)")
                self.completeBlock?(nil)
                return
            }
            
            let pm = placemarks! as [CLPlacemark]
            if (pm.count > 0){
                let p = placemarks![0] as CLPlacemark
                self.completeBlock?(p)
                self.stopLocation()
            } else {
                self.completeBlock?(nil)
            }
        })
    }
}
