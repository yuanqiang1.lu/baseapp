//
//  JchRegex.swift
//  InstallManage
//
//  Created by knight on 2017/9/7.
//  Copyright © 2017年 Goswift iOS team All rights reserved.
//

import Foundation

/// 验证邮箱
public func regexEmail(_ email: String) -> Bool {
    let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    return validate(email, regex)
}
/// 验证手机号
public func regexPhone(_ phone: String) -> Bool {
    let regex = "^1[0-9]{10}$"
    return validate(phone, regex)
}

/// 验证座机
public func regexTel(_ tel: String) -> Bool {
    let regex = "^(010|02\\d|0[3-9]\\d{2})?\\d{6,8}$"
    return validate(tel, regex)
}

/// 验证车牌号
public func regexCarPlateNormal(_ plate: String) -> Bool {
    let regex = "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9领使警学挂港澳试超]{1}$"
    return validate(plate, regex)
}

public func regexCarPlateSepcial(_ plate: String) -> Bool {
    let regex = "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{5}[A-HJ-NP-Z0-9领使警学挂港澳试超]{1}$"
    return validate(plate, regex)
}

/// 验证车架号
public func regexCarVin(_ engine: String) -> Bool {
    let regex = "^[A-HJ-NPR-Z0-9]{17,17}$"
    return validate(engine, regex)
}

/// 验证发动机号
public func regexCarEngine(_ engine: String) -> Bool {
    let regex = "^[A-HJ-NP-Z0-9]{5,12}$"
    return validate(engine, regex)
}

/// 验证用户名
public func regexeUserName(_ name: String) -> Bool {
    let regex = "^[A-Za-z0-9]{6,20}+$"
    return validate(name, regex)
}

/// 验证密码 8-20位同时包含字母数字
public func regexPassword(_ passWord: String) -> Bool {
    let regex = "^(?=.*[0-9])(?=.*[a-zA-Z])(.{8,20})$" //同时包含字母和数字
    return validate(passWord, regex)
}

/// 验证昵称
public func regexNickname(_ nickname: String) -> Bool {
    let regex = "^[\u{4e00}-\u{9fa5}]{4,8}$"
    return validate(nickname, regex)
}


//MARK: - 通用方法
/// 纯数字
public func regexPureNumber(_ number: String) -> Bool {
    let regex = "^[0-9]*$"
    return validate(number, regex)
}

/// 纯数字并带长度
public func regexPureNumber(_ number: String, lengthMin: Int, lengthMax: Int) -> Bool {
    let regex = "^^[0-9]{\(lengthMin),\(lengthMax)}$"
    return validate(number, regex)
}

/// 纯英文字母
public func regexPureLetter(_ number: String) -> Bool {
    let regex = "^[a-zA-Z]*$"
    return validate(number, regex)
}

/// 纯汉字长度
public func regexPureChinese(_ number: String, lengthMin: Int, lengthMax: Int) -> Bool {
    let regex = "^[\u{4e00}-\u{9fa5}]{\(lengthMin),\(lengthMax)}$"
    return validate(number, regex)
}

/// 纯英文带长度
public func regexPureLetter(_ number: String, lengthMin: Int, lengthMax: Int) -> Bool {
    let regex = "^[a-zA-Z]{\(lengthMin),\(lengthMax)}$"
    return validate(number, regex)
}

/// 数字英文带长度
public func regexNumberLetter(_ number: String, lengthMin: Int, lengthMax: Int) -> Bool {
    let regex = "^[A-Za-z0-9]{\(lengthMin),\(lengthMax)}$"
    return validate(number, regex)
}

public func validate(_ content: String, _ regex: String) -> Bool {
    let predicate = NSPredicate(format: "SELF MATCHES%@", regex)
    return predicate.evaluate(with: content)
}

/// 身份证判断
public func regexIdentityCard(_ identityCard: String) -> Bool {
    //判断是否为空
    if identityCard.count <= 0 {
        return false
    }
    //判断是否是18位，末尾是否是x
    let regex2: String = "^(\\d{14}|\\d{17})(\\d|[xX])$"
    let identityCardPredicate = NSPredicate(format: "SELF MATCHES %@", regex2)
    if !identityCardPredicate.evaluate(with: identityCard) {
        return false
    }
    //判断生日是否合法
    let range = NSRange(location: 6, length: 8)
    let datestr: String = (identityCard as NSString).substring(with: range)
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyyMMdd"
    if formatter.date(from: datestr) == nil {
        return false
    }
    //判断校验位
    if  identityCard.count == 18 {
        let idCardWi: [String] = ["7", "9", "10", "5", "8", "4", "2", "1", "6", "3", "7", "9", "10", "5", "8", "4", "2"]
        //将前17位加权因子保存在数组里
        let idCardY: [String] = ["1", "0", "10", "9", "8", "7", "6", "5", "4", "3", "2"]
        //这是除以11后，可能产生的11位余数、验证码，也保存成数组
        var idCardWiSum: Int = 0
        //用来保存前17位各自乖以加权因子后的总和
        for i in 0..<17 {
            idCardWiSum += Int((identityCard as NSString).substring(with: NSRange(location: i, length: 1)))! * Int(idCardWi[i])!
        }
        let idCardMod: Int = idCardWiSum % 11
        //计算出校验码所在数组的位置
        let idCardLast: String = String(identityCard.suffix(1))// identityCard.substring(from: identityCard.index(identityCard.endIndex, offsetBy: -1))
        //得到最后一位身份证号码
        //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
        if idCardMod == 2 {
            if idCardLast == "X" || idCardLast == "x" {
                return true
            } else {
                return false
            }
        } else {
            //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
            if (idCardLast as NSString).integerValue == Int(idCardY[idCardMod]) {
                return true
            } else {
                return false
            }
        }
    }
    return false
}
