//
//  Methods.swift
//  KLCKit
//
//  Created by wang zhenqi on 2019/4/28.
//  Copyright © 2019 Chengdu Luxingtong Electronic Co., Ltd. All rights reserved.
//

import Foundation

public func callService(_ phone: String = "18583221119") {
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(URL(string: "tel://\(phone)")!)
    } else {
        _ = UIApplication.shared.openURL(URL(string: "tel://\(phone)")!)
    }
}

public func goKlicenWebSide() {
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(URL(string: "http://www.klicen.com")!)
    } else {
        _ = UIApplication.shared.openURL(URL(string: "http://www.klicen.com")!)
    }
}

// 是否开启推送通知
public func isOpenNotification() -> Bool {
    let settings = UIApplication.shared.currentUserNotificationSettings
    
    if let settings = settings, settings.types == UIUserNotificationType(rawValue: 0) {
        return false
    }
    return true
}

// 前往App设置
public func openAppSetting() {
    let url = URL(string:UIApplication.openSettingsURLString)
    UIApplication.shared.openURL(url!)
}
