//
//  IMViewModelType.swift
//  InstallManage
//
//  Created by wang zhenqi on 2018/3/30.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
public protocol KLCViewModelType {
    associatedtype KLCInput
    associatedtype KLCOutput
    
    /// 完成input和output的相互订阅
    func transform(input: KLCInput) -> KLCOutput
}
