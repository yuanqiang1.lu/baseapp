//
//  KLCProvinceCodeView.swift
//  InstallManage
//
//  Created by knight on 2017/8/31.
//  Copyright © 2017年 Goswift iOS team All rights reserved.
//

import UIKit

public class KLCProvinceCodeView: UIView, UICollectionViewDataSource, UICollectionViewDelegate {

    let provinceCodes = ["京","沪","浙","苏","粤","鲁","晋","冀","豫","川","渝","辽","吉","黑","皖","鄂",
                         "湘","赣","闽","陕","甘","宁","蒙","津","贵","云","桂","琼","青","新","藏"]
    let letterArray = ["A","B","C","D","E","F","G","H","I","J","K","L","M",
                       "N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    let collectionview: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.itemSize = CGSize.init(width: SCREEN_WIDTH/8.0, height: SCREEN_WIDTH/8.0)
        
        let colView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        colView.backgroundColor = UIColor.white
        return colView
    }()
    public var province: String?
    public var letter: String?
    public var complete: ((String) -> Void)?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(white: 0, alpha: 0.2)
        
        let tapButton = UIButton()
        tapButton.addTarget(self, action: #selector(hide), for: .touchUpInside)
        addSubview(tapButton)
        tapButton.snp.updateConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.tag = 1000
        addSubview(collectionview)
        collectionview.snp.updateConstraints { (make) in
            make.leading.trailing.equalTo(self)
            make.height.equalTo(4*SCREEN_WIDTH/8.0)
            make.bottom.equalTo(self)
        }
        collectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "ProvinceCell")
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func show() {
        let window = UIApplication.shared.keyWindow
        let selfView = window?.viewWithTag(1000)
        if selfView != nil {
            self.removeFromSuperview()
        }
        window!.addSubview(self)
        self.snp.updateConstraints({ (make) in
            make.edges.equalTo(window!)
        })
    }
    
    @objc func hide() {
        removeFromSuperview()
    }
}

extension KLCProvinceCodeView {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return provinceCodes.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "ProvinceCell", for: indexPath)
        cell.layer.borderWidth = 0.3
        cell.layer.borderColor = UIColor.lightGray.cgColor
        
        var textLabel: UILabel? = cell.contentView.viewWithTag(100) as? UILabel
        if textLabel == nil {
            textLabel = UILabel()
            textLabel?.tag = 100
            textLabel?.font = FONT(18.0)
            textLabel?.textColor = UIColor.hexStringToColor(hexString: "#ACACAC")
            textLabel?.textAlignment = .center
            cell.contentView.addSubview(textLabel!)
            textLabel?.snp.updateConstraints({ (make) in
                make.center.equalTo(cell.contentView)
            })
        }
        
        textLabel?.text = ""
        if province == nil || province!.isEmpty {
            if provinceCodes.count > indexPath.row {
                textLabel?.text = provinceCodes[indexPath.row]
            }
        } else {
            if letterArray.count > indexPath.row {
                textLabel?.text = letterArray[indexPath.row]
            }
        }
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if province == nil || province!.isEmpty {
            if provinceCodes.count > indexPath.row {
                province = provinceCodes[indexPath.row]
                collectionView.reloadData()
            }
        } else {
            if letterArray.count > indexPath.row {
                letter = letterArray[indexPath.row]
                complete?(province! + letter!)
                
                removeFromSuperview()
            }
        }
    }
}
