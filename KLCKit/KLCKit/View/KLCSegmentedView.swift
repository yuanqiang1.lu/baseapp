    //
//  IMSegmentedView.swift
//  InstallManage
//
//  Created by Kun Huang on 2018/4/26.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import UIKit
import SnapKit

public class KLCSegmentedView: UIView {
    
    /// 选中回调
    public var selectedItemClosure:((_ index:Int) -> Void)?
    
    public var titles = [String]() {
        didSet {
            currentIndex = 0
            
            lineView.isHidden = titles.count <= 1
            setNeedsLayout()
        }
    }
    public var lineHeight:CGFloat = 3.0 // 指示条高
    public var lineWidth:CGFloat = 0.0 // 指示条宽
    public var lineColor = UIColor.clear {
        didSet {
            lineView.backgroundColor = lineColor
        }
    }
    
    /// 指示条颜色
    public var selectedTitleColor = UIColor.white //
    public var bottomLineColor = UIColor.clear {
        didSet {
            bottomLine.backgroundColor = bottomLineColor
        }
    }
    
    // 小红点颜色
    public var potViewColor:UIColor = .red
    fileprivate var showPotIndexs = [Int]()
    
    public var normaltitleColor = UIColor.white.withAlphaComponent(0.7) //
    public var selectedFont = UIFont.boldSystemFont(ofSize: 14)
    public var normalFont = UIFont.systemFont(ofSize: 14)
    private let bottomLine = UIView()
    // 是否允许选择
    public var isAllowSelect = true
    
    fileprivate var lineView = UIView()
    fileprivate var contentView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewLayout())
    public var currentIndex = 0 {
        didSet {
                    UIView.animate(withDuration: 0.25, animations: {
                        if !self.itemW.isNaN && !self.itemW.isInfinite{
                            let point = CGPoint(x: CGFloat(self.currentIndex+1)*self.itemW-self.itemW/2.0, y: self.lineView.center.y)
                            self.lineView.center = point
                        }
            
                    }) { (finished) in
            
                    }
        }
    }
    fileprivate var itemW:CGFloat = 0.0
    
    public init(titles:[String], frame:CGRect) {
        super.init(frame: frame)
        self.titles = titles
        setupUI()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupUI() {
        removeAllSubviews()
        bottomLine.backgroundColor = bottomLineColor
        addSubview(bottomLine)
        bottomLine.snp.makeConstraints { (maker) in
            maker.leading.bottom.trailing.equalToSuperview()
            maker.height.equalTo(0.5)
        }
        
        backgroundColor = UIColor.white
        addSubview(contentView)
        addSubview(lineView)
        lineView.cornerRadius = lineHeight / 2
        contentView.bounces = false
        contentView.register(KLCSegmentedCell.self, forCellWithReuseIdentifier: "KLCSegmentedCell")
        contentView.showsHorizontalScrollIndicator = false
        contentView.delegate = self
        contentView.dataSource = self
        contentView.backgroundColor = UIColor.clear
        lineView.backgroundColor = lineColor
        contentView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height-lineHeight)
        contentView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
//        let bottomLine = UIView(frame: CGRect(x: 0, y: frame.height-0.5, width: frame.width, height: 0.5))
//        bottomLine.backgroundColor = UIColor.hexStringToColor(hexString: "E7E7E7")
//        addSubview(bottomLine)
    }
    
    /// 滑动到index
    public func scrollTo(index:Int) {
        if index > titles.count-1 {
            return
        }
        
        if index == currentIndex {return}
        
        print("index=\(index)")
        
        selectedItem(index: Int(index))
        
    }
    
    /// 修改某个item的title
    public func updateItemTitle(index:Int,title:String) {
        if index > titles.count-1 {
            return
        }
        titles[index] = title
        contentView.reloadItems(at: [IndexPath(item: index, section: 0)])
    }
    
    /// 选中了某个cell
    public func selectedItem(index:Int) {
        if currentIndex >= titles.count { //
            currentIndex = 0
        }
        selectedItemClosure?(index)
        let currentCell = contentView.cellForItem(at: IndexPath(item: index, section: 0)) as? KLCSegmentedCell
        let lastCell = contentView.cellForItem(at: IndexPath(item: currentIndex, section: 0)) as? KLCSegmentedCell
        currentCell?.textLabel.textColor = selectedTitleColor
        lastCell?.textLabel.textColor = normaltitleColor
        currentCell?.textLabel.font = selectedFont
        lastCell?.textLabel.font = normalFont
        currentIndex = index
    }
    
    /// 显示某个item上的小红点
    public func showPotView(index:Int, isShow:Bool) {
        if index < 0 && index > titles.count {return}
        
        if isShow {
            // 如果没有包含在加入
            if !showPotIndexs.contains(index) {
                showPotIndexs.append(index)
            }
        } else {
            // 判断是否包含，和下标再移除
            if let item = showPotIndexs.firstIndex(of: index),showPotIndexs.contains(index) {
                showPotIndexs.remove(at: item)
            }
        }

        let cell = contentView.cellForItem(at: IndexPath(item: index, section: 0)) as? KLCSegmentedCell
        cell?.isShowPot = isShow
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        itemW = titles.count <= 5 ? frame.width/CGFloat(titles.count) : frame.width/5.5
        if !self.itemW.isNaN && !self.itemW.isInfinite{
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0.0
            layout.minimumInteritemSpacing = 0.0
        
            if titles.count <= 5 {
                layout.itemSize = CGSize(width: itemW, height: frame.height-lineHeight)
            } else {
                if #available(iOS 10.0, *) {
                    layout.itemSize = UICollectionViewFlowLayout.automaticSize
                } else {
                    layout.itemSize = CGSize(width: 40, height: 30)
                }
                layout.estimatedItemSize = CGSize(width: itemW, height: frame.height-lineHeight)
            }
        
            contentView.collectionViewLayout = layout
            lineView.frame = CGRect(x: 0, y: 0, width: lineWidth, height: lineHeight)
            if currentIndex == 0 {
                lineView.center = CGPoint(x: itemW/2.0, y: frame.height-lineHeight/2.0)
            } else {
                lineView.center = CGPoint(x: CGFloat(self.currentIndex+1)*self.itemW-self.itemW/2.0, y: frame.height-lineHeight/2.0)
            }
            
            contentView.reloadData()
        }
        
        contentView.selectItem(at: IndexPath(item: currentIndex, section: 0), animated: true, scrollPosition: .right)
    }
}

extension KLCSegmentedView: UICollectionViewDelegate, UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KLCSegmentedCell", for: indexPath) as! KLCSegmentedCell
        cell.textLabel.text = titles[indexPath.row]
        cell.textLabel.textColor = indexPath.row == currentIndex ? selectedTitleColor : normaltitleColor
        cell.textLabel.font = indexPath.row == currentIndex ? selectedFont : normalFont
        cell.potViewColor = potViewColor
        cell.isShowPot = showPotIndexs.contains(indexPath.row)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if currentIndex == indexPath.row || !isAllowSelect {return}
        selectedItem(index: indexPath.row)
    }
    
}
class KLCSegmentedCell: UICollectionViewCell {
        
        var text :String? {
            didSet {
                textLabel.text = text
            }
        }
    
        /// 小红点颜色
        var potViewColor:UIColor = .red {
            didSet {
                badgePotView.backgroundColor = potViewColor
            }
        }
    
        /// 是否显示小红点
        var isShowPot:Bool = false {
            didSet {
                badgePotView.isHidden = !isShowPot
            }
        }
    
        private lazy var containerView = UIView()
        
        let textLabel = UILabel()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            globalSetup()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            globalSetup()
        }
        
        private func globalSetup() {
            setupContainerView()
            setupContentLabel()
        }
        
        private func setupContainerView() {
            contentView.addSubview(containerView)
            containerView.snp.makeConstraints { (maker) in
                maker.edges.equalToSuperview()
            }
        }
        
        private func setupContentLabel() {
            textLabel.numberOfLines = 1
            textLabel.textAlignment = .center
            containerView.addSubview(textLabel)
            containerView.addSubview(badgePotView)
            textLabel.snp.makeConstraints { (maker) in
                maker.leading.equalTo(5)
                maker.trailing.equalTo(-5)
                maker.centerY.equalToSuperview()
                maker.width.greaterThanOrEqualTo(50)
            }
            
            badgePotView.snp.makeConstraints { (maker) in
                maker.centerX.equalTo(textLabel.snp_trailing)
                maker.centerY.equalTo(textLabel.snp_top)
                maker.width.height.equalTo(7)
            }
        }
    
        // 小红点
        private lazy var badgePotView:UIView = {
            let potView = UIView()
            potView.backgroundColor = .red
            potView.clipsToBounds = true
            potView.layer.cornerRadius = 3.5
            potView.isHidden = true
            return potView
        }()
    }
