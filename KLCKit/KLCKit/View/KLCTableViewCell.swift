//
//  KLCTableViewCell.swift
//  ZEUS
//
//  Created by wang zhenqi on 2018/8/14.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import UIKit
import RxSwift

open class KLCTableViewCell: UITableViewCell {
    public var disposeBag:DisposeBag! = DisposeBag()
    open override func awakeFromNib() {
        super.awakeFromNib()
//        self.disposeBag = DisposeBag()
    }

    open override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    open override func prepareForReuse() {
        self.disposeBag = nil
        super.prepareForReuse()
    }
}
