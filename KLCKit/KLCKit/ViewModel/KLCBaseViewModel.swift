//
//  IMBaseViewModel.swift
//  InstallManage
//
//  Created by wang zhenqi on 2018/4/3.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

open class KLCBaseViewModel {
    public let disposeBag = DisposeBag()
    public init() {}
    
    // 监听网络状态,方便缺省页使用
    public var requestStatus = BehaviorRelay(value: KLCRequstResultStatus.requesting)
}

/// 可调用倒计时方法
public protocol KLCCountDownType {
    func startTimer(_ timerCountSecond:Int) -> Variable<(String,Bool)>
}

public extension KLCCountDownType {
    
    /// 倒计时timer,默认60s; 返回 Variable<(String,Bool)>, String:标题, Bool:Button 的可点击状态
    func startTimer(_ timerCountSecond:Int = 60) -> Variable<(String,Bool)>{
        let titleAndEnable = Variable(("\(timerCountSecond)s",false))
        var timerCount = timerCountSecond
        let timer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
        timer.schedule(deadline: .now(), repeating: .seconds(1))
        timer.setEventHandler {
            timerCount -= 1
            
            if timerCount <= 0 {
                timer.cancel()
            }
            DispatchQueue.main.async {
                if timerCount == 0 {
                    titleAndEnable.value = ("over", true)
                } else {
                    titleAndEnable.value = (String(format:"%02d",timerCount), false)
                }
            }
        }
        timer.resume()
        return titleAndEnable
    }
}
