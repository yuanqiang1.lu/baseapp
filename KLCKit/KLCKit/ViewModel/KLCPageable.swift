//
//  IMPageable.swift
//  InstallManage
//
//  Created by wang zhenqi on 2018/4/2.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
import Moya
import RxSwift
import HandyJSON


enum KLCPagingTarget {
    case refresh(String, [String:Any])
    case more(String, [String:Any])
}

extension KLCPagingTarget: TargetType {
    var baseURL: URL {
        switch self {
        case .refresh(let url, _):
            return try! (KLCBaseUrl + url).asURL()
        case .more(let url, _):
            return try! url.asURL()
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return "".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        case .more(_, let parameter):
            return .requestParameters(parameters: parameter, encoding: URLEncoding.default)
        case .refresh(_, let parameter):
            return .requestParameters(parameters: parameter, encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}

class KLCPagingModel<T:HandyJSON>: KLCBaseModel {
    var count = 0
    var amount = 0.0
    var next:String?
    var previous: String?
    var results:[T]?
}

class KLCPagingViewModel<T:HandyJSON>: KLCBaseViewModel {
    let dataList = Variable<[T]>([])
    var pageModel = KLCPagingModel<T>()
}

extension KLCPagingViewModel: KLCViewModelType {
    
    typealias KLCInput = KLCPageInput
    
    typealias KLCOutput = KLCPageOutput
    
    struct KLCPageInput {
        var requestType = Variable(KLCPagingTarget.refresh("", [:]))
    }
    struct KLCPageOutput {
        let requestComplate = PublishSubject<Bool>()
        let dataList = Variable([T]())
    }
    
    func transform(input: KLCPagingViewModel.KLCPageInput) -> KLCPagingViewModel.KLCPageOutput {
        let output = KLCPageOutput()
        let requestType = input.requestType.asObservable().skip(1)
        requestType.subscribe(onNext: {[unowned self] (target) in
            KLCNetwork.request(MultiTarget(target))
                .mapModel(KLCPagingModel<T>.self)
                .subscribe(onNext: { [unowned self](page) in
                    self.pageModel.count = page?.count ?? 0
                    self.pageModel.next = page?.next
                    self.pageModel.previous = page?.previous
                    self.pageModel.amount = page?.amount ?? 0
                    switch input.requestType.value {
                    case .refresh(_, _):
                        self.dataList.value = page?.results ?? []
                    case .more(_, _):
                        self.dataList.value += page?.results ?? []
                    }
                    output.requestComplate.onNext(true)
                }, onError: { (error) in
                    print(error)
                    output.requestComplate.onNext(false)
                })
                .disposed(by: self.disposeBag)
            }).disposed(by: disposeBag)

        return output
    }
}
