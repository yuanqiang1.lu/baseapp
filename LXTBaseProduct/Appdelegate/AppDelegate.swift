//
//  AppDelegate.swift
//  LXTBaseProduct
//
//  Created by luyuanqiang on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import UIKit
import KLCKit
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    open var router: NavigatorType?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        appConfiguration()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: .badge) { (accepted, error) in
                log.info("accecepted : \(accepted)")
            }
            UNUserNotificationCenter.current().delegate = NotificationManager.instance
        } else {
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
            let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
            application.registerUserNotificationSettings(pushNotificationSettings)
            application.registerForRemoteNotifications()
        }
        return true
    }

    // swiftlint:disable line_length
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        //launchAd()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.cancelAllLocalNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        log.info("开始后台任务")
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let urlStr = url.absoluteString
        /// usage: klicen8://h5www.baidu.com
        let rulePrefix = "klicen8://h5"
        if urlStr.hasPrefix(rulePrefix) {
            let aUrlStr = urlStr.cutString(rule: rulePrefix, originStr: urlStr)
            log.warning("----\(aUrlStr)")
            let urlStr = "http://"+aUrlStr
            let isPushed = self.router?.present(urlStr) != nil
            if isPushed {
                log.warning("push url: \(urlStr)")
            }
        } else {
            if self.router?.present(url, wrap: UINavigationController.self) != nil {
                print("[Navigator] present: \(url)")
                return true
            }
            
            // Try opening the URL
            if self.router?.open(url) == true {
                print("[Navigator] open: \(url)")
                return true
            }
            
            return KLCThridAccountConfig.handleOpenUrl(url)
        }
        return false
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        log.info("notification: \(userInfo.description)")
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        log.info("receive the notification: \(notification.description)")
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, withResponseInfo responseInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
        log.info("get message \(responseInfo.description)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        if Platform.isSimulator {
            log.error("Simulator is not support")
        } else {
            let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02x", $1)})
            //Defaults[.pushToken] = deviceTokenString
            log.info("The device token is \(deviceTokenString)")
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        if Platform.isSimulator {
            log.error("Simulator is not support")
        } else {
            log.error("Some error happen\(error)")
        }
    }
}
