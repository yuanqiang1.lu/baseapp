//
//  Appdelegate+Extension.swift
//  LXTBaseProduct
//
//  Created by awesome on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import FLEX
import IQKeyboardManagerSwift
import KLCKit
import SwiftTheme
import SwiftyBeaver
import GDPerformanceView_Swift
import Toast_Swift
import UserNotifications
@_exported import Moya
@_exported import RxSwift
@_exported import RxCocoa
@_exported import RxDataSources
@_exported import URLNavigator
@_exported import SwiftyUserDefaults
@_exported import SwiftDate

let log = SwiftyBeaver.self

extension AppDelegate {
    
    func appConfiguration() {
        configLog()
        configFlex(show:false)
        configIQKeyboard()
        configAppType()
        configTheme()
        configActionLog()
        configShare()
        appRouter()
        configRealm()
    }
    
    func configRootViewController() {
        let token = KLCUserManager.userToken()
        if let _ = token {
            let vc = KLCBookingListViewController()
            window?.rootViewController = KLCNavigationController(rootViewController: vc)
        } else {
            let vc = R.storyboard.login().instantiateInitialViewController()!
            window?.rootViewController = vc
        }
    }
    
    private func configAppType() {
        
        let url = R.file.klcBaseConfigPlist()!
        guard let dicts = NSDictionary(contentsOf: url) as? [String : Any] , let type = dicts["AppType"] as? String else {
            fatalError("dicts is can not be nil")
        }
        KLCAppType = type
        configKeystuffs(with: dicts)
        log.warning("path:\(url) dicts:\(KLCAppType)")
    
    }
    
    func configKeystuffs(with dic:[String:Any]) {
        #if DEBUG
        KLCRuntimeEnvType = "Debug"
        #elseif Enterprise
        KLCRuntimeEnvType = "Enterprise"
        #elseif Adhoc
        KLCRuntimeEnvType = "Adhoc"
        #else
        
        #endif
        
        guard let debug = dic[KLCRuntimeEnvType] as? [String: String],
            let filePath = debug["FileBaseUrl"],
            let _ = debug["BaseUrl"],
            let forceAdUrl = debug["ForceAdUrl"],
            let amapKey = debug["AMapWebKey"],
            let wchatKey = debug["WeixinOAuthID"],
            let umkey = debug["UmengKey"],
            let pushKey = debug["JPushKey"] else {
                return
        }
        KLCFileBaseUrl = filePath
        KLCForceAdUrl = forceAdUrl
        // swiftlint:disable:next line_length
        log.warning("\(breakLineStr)runtime env:\(KLCRuntimeEnvType) \nKLCFileBaseUrl:\(KLCFileBaseUrl)KLCBaseUrl:\(KLCBaseUrl) \n amapKey: \(amapKey)\n wchatKey:\(wchatKey)\n umkey:\(umkey) \n pushKey: \(pushKey) \(breakLineStr) \n forceAdUrl: \(forceAdUrl)")
        let confg = KLCStorage?.transformCodable(ofType: ConfigSettings.self)
        let setting = ConfigSettings()
        setting.amapKey = amapKey
        setting.wchatKey = wchatKey
        setting.umkey = umkey
        setting.pushKey = pushKey
        try? confg?.setObject(setting, forKey: "AppConfiguration")
    }
    
    private func configActionLog() {
        log.info("configActionLog")
        swizzleMethod("sendAction:to:forEvent:",
                      newFunc: "lxt_sendAction:to:for:",
                      target: UIButton.self)
    }
    
    private func downloadTheme() {
        // swiftlint:disable:next empty_parentheses_with_trailing_closure
        MyThemes.downloadBlueTask() { isSuccess in
            if isSuccess {
                MyThemes.switchTo(.blue)
            }
        }
    }
    
    private func appRouter() {
        let navigators = Navigator()
        // Initialize navigation map
        NavigationMap.initialize(navigator: navigators)
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .white
        let token = KLCUserManager.userToken()
        if let _ = token {
            let vc = KLCBookingListViewController()
            window.rootViewController = KLCNavigationController(rootViewController: vc)
        } else {
            let vc = R.storyboard.login().instantiateInitialViewController()!
            window.rootViewController = vc
        }
        self.window = window
        router = navigators
        window.makeKeyAndVisible()
    }
    
    private func configLog() {
        let console = ConsoleDestination()
        let file = FileDestination()
        log.addDestination(file)
        let projectName = NSLocalizedString("appname.feature", comment: "appname.feature")
        console.format = "\(projectName) $DHH:mm:ss$d $C$L$c $N.$F:$l - $M"
        file.minLevel = .warning
        log.addDestination(console)
    }
    
    private func configFlex(show isShow: Bool) {
        if isShow {
            FLEXManager.shared().showExplorer()
            PerformanceMonitor.shared().start()
        } else {
            FLEXManager.shared()?.hideExplorer()
        }
    }
    
    private func configTheme() {
        
        if MyThemes.isBlueThemeExist() {
            MyThemes.switchTo(.blue)
        } else {
            /// default: Red.plist
            ThemeManager.setTheme(plistName: "Red", path: .mainBundle)
        }
        
        let navigationBar = UINavigationBar.appearance()
        navigationBar.isTranslucent = false
        navigationBar.setBackgroundImage(UIImage.image(with: .clear), for: .default)
        navigationBar.shadowImage = UIImage.image(with: .clear)
        
        let backImg = R.image.navigate_back()?.withRenderingMode(.alwaysOriginal)
        navigationBar.backIndicatorImage = backImg
        navigationBar.backIndicatorTransitionMaskImage = backImg
        
        let tabBar = UITabBar.appearance()
        tabBar.isTranslucent = false
        tabBar.theme_tintColor = "Global.tabbarTextColor"
        tabBar.theme_barTintColor = "Global.tabbarTingColor"
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font : FONT(15, weight: .medium),
                                                             .foregroundColor : UIStandard.FontColor.G4], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font : FONT(15, weight: .medium),
                                                             .foregroundColor : UIStandard.FontColor.G5], for: .disabled)
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes([NSAttributedString.Key.font : FONT(15, weight: .medium),
                                                                                                           .foregroundColor : UIStandard.FontColor.G5], for: .normal)
    }
    
    private func configIQKeyboard() {
        let keyboardMgr = IQKeyboardManager.shared
        keyboardMgr.enable = true
        keyboardMgr.enableAutoToolbar = false
        keyboardMgr.shouldToolbarUsesTextFieldTintColor = true
        keyboardMgr.shouldShowToolbarPlaceholder = false
        keyboardMgr.shouldResignOnTouchOutside = true
        SwiftDate.defaultRegion = Region.local
    }
    
    private func configShare() {
        let confs = KLCStorage?.transformCodable(ofType: ConfigSettings.self)
        let conf = try? confs?.object(forKey: "AppConfiguration")
        log.warning("config: \(String(describing: conf?.amapKey))")
        guard let wchatKey = conf?.wchatKey else {
            return
        }
        KLCThridAccountConfig.config.weixinConfig = KLCThridAccountConfig.Weixin(wchatKey)
        KLCThridAccountConfig.registerWeixin()
    }
}
