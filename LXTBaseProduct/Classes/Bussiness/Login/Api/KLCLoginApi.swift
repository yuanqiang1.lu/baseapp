//
//  LoginApi.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/11.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import Moya
import KLCKit

enum KLCLoginApi {
    /// login
    case userLogin([String:Any])
    /// registration
    case userRegistration([String:Any])
}

extension KLCLoginApi: TargetType {
    var baseURL: URL {
        return URL(string:KLCNewBaseUrl)!
    }
    
    var path: String {
        switch self {
            case .userLogin:
                return "/users/sign_in.json"
            case .userRegistration:
                return "/users.json"
        }
    }
        
    var method: Moya.Method {
        return .post
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .userLogin(let param):
            return .requestCompositeData(bodyData: Data(), urlParameters: param)
        case .userRegistration(let param):
            return .requestCompositeData(bodyData: Data(), urlParameters: param)
        }
    }
    
    var headers: [String : String]? {
        return [:]
    }
}
