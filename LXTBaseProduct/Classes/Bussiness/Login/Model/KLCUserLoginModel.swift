//
//  KLCUserLoginModel.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import Foundation
import HandyJSON
import RealmSwift

class KLCUserLoginModel: Object, HandyJSON {
    @objc dynamic var id = 0
    @objc dynamic var email = ""
    @objc dynamic var created_at = ""
    @objc dynamic var updated_at = ""
    @objc dynamic var authentication_token = ""
    override static func primaryKey() -> String? {
        return "id"
    }
}
