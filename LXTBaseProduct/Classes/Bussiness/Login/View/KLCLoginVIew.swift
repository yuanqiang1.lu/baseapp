//
//  KLCLoginVIew.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import Foundation

class KLCLoginView: UIView {

    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var loginLabel: UILabel!
    
    @IBOutlet weak var emailTxtField: UITextField!
    
    @IBOutlet weak var pwdTxtField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var forgetPwdButton: UIButton!
    
    var emailStr = BehaviorRelay(value: "")
    
    var pwdStr = BehaviorRelay(value: "")

    override func awakeFromNib() {
        super.awakeFromNib()
        
        registerButton.setTitle("align.awesome.registration".localString, for: .normal)
        loginLabel.text = "align.awesome.logintxt".localString
        emailTxtField.placeholder = "align.awesome.inputEmail".localString
        pwdTxtField.placeholder = "align.awesome.password".localString
        loginButton.setTitle("align.awesome.logintxt".localString, for: .normal)
        forgetPwdButton.setTitle("align.awesome.forgetPasswordTxt".localString, for: .normal)
        
        setupRx()
    }
    
    private func setupRx() {
        (emailTxtField.rx.textInput <-> emailStr).disposed(by: rx.disposeBag)
        emailStr.map({
            String($0.replacingOccurrences(of: " ", with: "").prefix(25))
        }).subscribe(onNext: { [weak self] (text) in
            self?.loginButton.isEnabled = false
            self?.loginButton.backgroundColor = UIStandard.FontColor.G6
            if !text.isEmpty && text.count < 25 {
                if regexEmail(text) {
                    self?.loginButton.isEnabled = true
                    self?.loginButton.backgroundColor = UIStandard.MainColor.C2
                }
            }
        }).disposed(by: rx.disposeBag)
        
        (pwdTxtField.rx.textInput <-> pwdStr).disposed(by: rx.disposeBag)
        
    }
}
