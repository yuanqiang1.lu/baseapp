//
//  KLCRegistrationView.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit

class KLCRegistrationView: UIView {

    @IBOutlet weak var registerLabel: UILabel!
    
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var pwdTxt: UITextField!
    
    @IBOutlet weak var pwdReInputTxt: UITextField!
    
    @IBOutlet weak var registButton: UIButton!
    
    @IBOutlet weak var loginButton: UIButton!

    var emailStr = BehaviorRelay(value: "")
    
    var pwdStr = BehaviorRelay(value: "")
    
    var pwdAgainStr = BehaviorRelay(value: "")

    override func awakeFromNib() {
        super.awakeFromNib()
        registerLabel.text = "align.awesome.registration".localString
        emailTxt.placeholder = "align.awesome.inputEmail".localString
        pwdTxt.placeholder = "align.awesome.password".localString
        pwdReInputTxt.placeholder = "align.awesome.comfirmpassword".localString
        registButton.setTitle("align.awesome.registBtnTxt".localString, for: .normal)
        loginButton.setTitle("align.awesome.logintxt".localString, for: .normal)
        setupRx()
    }
    
    private func setupRx() {
        (emailTxt.rx.textInput <-> emailStr).disposed(by: rx.disposeBag)
        emailStr.map({
            String($0.replacingOccurrences(of: " ", with: "").prefix(25))
        }).subscribe(onNext: { [weak self] (text) in
            self?.registButton.isEnabled = false
            self?.registButton.backgroundColor = UIStandard.FontColor.G6
            if !text.isEmpty && text.count < 25 {
                if regexEmail(text) {
                    self?.registButton.isEnabled = true
                    self?.registButton.backgroundColor = UIStandard.MainColor.C2
                }
            }
        }).disposed(by: rx.disposeBag)
        
        (pwdTxt.rx.textInput <-> pwdStr).disposed(by: rx.disposeBag)
        
        (pwdReInputTxt.rx.textInput <-> pwdAgainStr).disposed(by: rx.disposeBag)
    }
}
