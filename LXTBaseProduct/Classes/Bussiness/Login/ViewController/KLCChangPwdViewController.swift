//
//  KLCChangPwdViewController.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import UIKit

class KLCChangPwdViewController: UIViewController {

    @IBOutlet weak var titleDescLabel: UILabel!
    
    @IBOutlet weak var newPwdTxtField: UITextField!
    
    @IBOutlet weak var comfirmTxtField: UITextField!
    
    @IBOutlet weak var sureButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "align.awesome.comfirmTitle".localString
        titleDescLabel.text = "align.awesome.comfirmTitleDesc".localString
        newPwdTxtField.placeholder = "align.awesome.newpassword".localString
        comfirmTxtField.placeholder = "align.awesome.comfirmpassword".localString
        sureButton.setTitle("align.awesome.passwordComfirm".localString, for: .normal)
        sureButton.rx.tap.subscribe(onNext: { [weak self] in
            log.info("please comfirm")
            let isPwdEqual = self?.checkPassword() ?? false
            if !isPwdEqual {
                self?.showToast("not equal", duration: 1)
                return
            }
            self?.navigationController?.popToRootViewController(animated: true)
        }).disposed(by: rx.disposeBag)
    }
    
    private func checkPassword() -> Bool {
        let pwdStr = newPwdTxtField.text
        let confirmPwdStr = comfirmTxtField.text
        if pwdStr != nil && confirmPwdStr != nil && confirmPwdStr == pwdStr {
            return true
        }
        return false
    }
}
