//
//  KLCForgetPwdViewController.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import UIKit

class KLCForgetPwdViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "align.awesome.forgetVCtitle".localString
        titleLabel.text = "align.awesome.forgetVCtitleDesc".localString
        emailTextField.placeholder = "align.awesome.forgetVCPlaceholder".localString
        nextButton.setTitle("align.awesome.forgetVCNext".localString, for: .normal)
        nextButton.rx.tap.subscribe(onNext: { [weak self] in
            let emailStr = self?.emailTextField.text ?? ""
            if regexEmail(emailStr) {
                log.info("Push to")
                let vc = KLCChangPwdViewController()
                self?.navigationController?.pushViewController(vc, animated: true)
            } else {
                self?.emailTextField.becomeFirstResponder()
                self?.showToast("Input valid email".localString, duration:1)
            }
            
        }).disposed(by: rx.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

}
