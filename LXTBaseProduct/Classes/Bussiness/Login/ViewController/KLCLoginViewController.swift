//
//  KLCLoginViewController.swift
//  LXTBaseProduct
//
//  Created by luyuanqiang on 2019/4/12.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import HandyJSON
import SwiftyUserDefaults

class KLCLoginViewController: UIViewController {
    
    @IBOutlet weak var descLabel: UILabel!
    
    let registLoginView = R.nib.klcRegistrationView(owner: nil)!
    let loginView = R.nib.klcLoginView(owner: nil)!
    
    private let viewModel = KLCLoginViewModel()
    private let input = KLCLoginViewModel.KLCInput()
    private var output:KLCLoginViewModel.KLCOutput!

    override func viewDidLoad() {
        super.viewDidLoad()
        descLabel.text = "align.awesome.creatorDesc".localString
        layout()
        setupRx()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func layout() {
        
        view.addSubview(registLoginView)
        registLoginView.snp.makeConstraints { (maker) in
            maker.top.equalTo(82)
            maker.leading.trailing.equalToSuperview()
            
        }
        
        view.addSubview(loginView)
        loginView.snp.makeConstraints { (maker) in
            maker.top.equalTo(82)
            maker.leading.trailing.equalToSuperview()
            
        }
       
    }
    
    private func setupRx() {
        output = viewModel.transform(input: input)
        output.toastMsg.subscribe(onNext: { [weak self] (msg, time, block) in
            self?.showToast(msg, duration: time, complete: { block?() })
        }).disposed(by: rx.disposeBag)
        
        output.loginResult.subscribe(onNext: { [weak self] (isLogin) in
            if isLogin {
                self?.showToast("align.awesome.loginwelcome".localString, duration: 1, complete: {
                    log.info("change route view controller")
                    KLCAppdelegate.configRootViewController()
                })
            }
        }).disposed(by: rx.disposeBag)

        bind()
        actionBind()
    }
    
    private func bind() {
        registLoginView.emailStr.bind(to: input.email).disposed(by: rx.disposeBag)
        registLoginView.pwdStr.bind(to: input.pwd).disposed(by: rx.disposeBag)
        registLoginView.pwdAgainStr.bind(to: input.pwdAgain).disposed(by: rx.disposeBag)
        
        loginView.emailStr.bind(to: input.email).disposed(by: rx.disposeBag)
        loginView.pwdStr.bind(to: input.pwd).disposed(by: rx.disposeBag)
    }
    
    private func actionBind() {
        registLoginView.registButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.resignFirstResponder()
            self?.input.registerCmd.onNext(())
        }).disposed(by: rx.disposeBag)
        
        registLoginView.loginButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.resignFirstResponder()
            self?.loginView.isHidden = false
            self?.registLoginView.isHidden = true
        }).disposed(by: rx.disposeBag)
        
        loginView.registerButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.resignFirstResponder()
            self?.loginView.isHidden = true
            self?.registLoginView.isHidden = false
        }).disposed(by: rx.disposeBag)
        
        loginView.loginButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.resignFirstResponder()
            self?.input.msgLoginCmd.onNext(())
        }).disposed(by: rx.disposeBag)
        
        loginView.forgetPwdButton.rx.tap.subscribe(onNext: { [weak self] in
            let vc = KLCForgetPwdViewController()
            self?.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: rx.disposeBag)
    }
    
}
