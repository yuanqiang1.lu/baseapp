//
//  KLCLoginViewModel.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import KLCKit

typealias KLCSimpleBlock = () -> Void

class KLCLoginViewModel: KLCBaseViewModel, KLCViewModelType {
    typealias KLCInput = KLCLoginInput
    typealias KLCOutput = KLCLoginOutput
    
    struct KLCLoginInput {
        let email = BehaviorRelay(value: "")
        let pwd = BehaviorRelay(value: "")
        let pwdAgain = BehaviorRelay(value: "")
        
        let msgLoginCmd = PublishSubject<Void>()
        let registerCmd = PublishSubject<Void>()
    }
    
    struct KLCLoginOutput {
        let toastMsg = PublishSubject<(String, Double, KLCSimpleBlock?)>()
        let loginResult = PublishSubject<Bool>()
    }
    
    func transform(input: KLCLoginViewModel.KLCInput) -> KLCLoginViewModel.KLCOutput {
        let output = KLCOutput()
        register(input, output)
        userLogin(input, output)
        return output
    }
    
    private func register(_ input:KLCInput, _ output:KLCOutput) {
        input.registerCmd.subscribe(onNext: { [unowned self] _ in
            
            if input.pwd.value.isEmpty || input.pwdAgain.value.isEmpty || input.pwd.value != input.pwdAgain.value {
                output.toastMsg.onNext(("password not equal", 1, nil))
                return
            }
            
            if input.pwd.value.count < 6 {
                output.toastMsg.onNext(("password is less than 6", 1, nil))
                return
            }
            CurrentViewController?.showToast()
            let paramDic = ["user[email]":input.email.value,
                            "user[password]":input.pwd.value,
                            "user[password_confirmation]":input.pwdAgain.value] as [String : Any]
            KLCNetwork.request(MultiTarget(KLCLoginApi.userRegistration(paramDic)))
                .mapModel(KLCUserLoginModel.self)
                .subscribe(onNext: { (rs) in
                    defer {
                        CurrentViewController?.hideToast()
                    }
                    KLCUserManager.saveUserEmail(rs?.email ?? "")
                    KLCUserManager.saveUserToken(rs?.authentication_token ?? "")
                    output.loginResult.onNext(true)
                }, onError: { (error) in
                    defer {
                        CurrentViewController?.hideToast()
                    }
                    output.toastMsg.onNext((KLCErrorDescription(error), 2, nil))
                }).disposed(by:self.disposeBag)
            
        }).disposed(by: self.disposeBag)
    }
    
    // login
    private func userLogin(_ input: KLCInput, _ output: KLCOutput) {
        input.msgLoginCmd.subscribe(onNext: { [unowned self] _ in
            KLCUserManager.saveLastLoginUsername(input.email.value)
            let paramDic = ["user[email]":input.email.value,
                            "user[password]":input.pwd.value] as [String : Any]
            CurrentViewController?.showToast()
            KLCNetwork.request(MultiTarget(KLCLoginApi.userLogin(paramDic)))
                .mapModel(KLCUserLoginModel.self)
                .subscribe(onNext: { (rs) in
                    KLCUserManager.saveUserEmail(rs?.email ?? "")
                    KLCUserManager.saveUserToken(rs?.authentication_token ?? "")
                    output.loginResult.onNext(true)
                    CurrentViewController?.hideToast()
                }, onError: { (error) in
                    output.toastMsg.onNext((KLCErrorDescription(error), 2, nil))
                    CurrentViewController?.hideToast()
                }).disposed(by:self.disposeBag)
        }).disposed(by: disposeBag)
    }
    
}
