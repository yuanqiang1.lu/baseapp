//
//  KLCBookingApi.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import Foundation
import Moya
import KLCKit

enum KLCBookingApi {
    /// booking list
    case booksList
    /// get desk list
    case desksList
    /// Booking a new desk
    case newBooking([String:Any])
    /// update booking by index
    case updateBooking(index:Int,param:[String:Any])
}

extension KLCBookingApi: TargetType {
    var baseURL: URL {
        switch self {
        case .desksList,.booksList,.updateBooking,.newBooking:
            return URL(string:KLCNewBaseUrl)!
        }
    }
    
    var path: String {
        switch self {
        case .newBooking:
            return "/bookings.json"
        case .booksList:
            return "/bookings.json"
        case .desksList:
            return "/desks.json"
        case .updateBooking(let param):
            return "/bookings/\(param.index).json"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .booksList,.desksList:
            return .get
        case .newBooking:
            return .post
        case .updateBooking:
            return .put
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .newBooking(let param):
            return .requestCompositeData(bodyData: Data(), urlParameters: param)
        case let .updateBooking(obj):
            return .requestCompositeData(bodyData: Data(), urlParameters: obj.param)
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .booksList,.desksList,.updateBooking,.newBooking:
            let email = KLCUserManager.userEmail() ?? ""
            let token = KLCUserManager.userToken() ?? ""
            return ["X-User-Email":email,
                    "X-User-Token":token]
        }
    }
}
