//
//  KLCSeatsModel.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/4.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import HandyJSON
import RealmSwift

class KLCSeatsModel: Object, HandyJSON {
    @objc dynamic var id = 0
    @objc dynamic var label = ""
    @objc dynamic var location = ""
    @objc dynamic var created_at = ""
    @objc dynamic var updated_at = ""
    @objc dynamic var url = ""
    override static func primaryKey() -> String? {
        return "id"
    }
}

class KLCBookingResultModel: Object, HandyJSON {
    @objc dynamic var id = 0
    @objc dynamic var user_id = ""
    @objc dynamic var desk_id = ""
    @objc dynamic var created_at = ""
    @objc dynamic var updated_at = ""
    @objc dynamic var booked_at = ""
    @objc dynamic var booked_from = ""
    @objc dynamic var booked_to = ""
    @objc dynamic var user:KLCUserLoginModel?
    @objc dynamic var desk:KLCSeatsModel?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
