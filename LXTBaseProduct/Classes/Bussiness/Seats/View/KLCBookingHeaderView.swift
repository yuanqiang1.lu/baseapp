//
//  KLCBookingHeaderView.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import UIKit

class KLCBookingHeaderView: UIView {

    enum buttonType:Int {
        case create
        case exit
    }
    
    @IBOutlet weak var welcomeLabel: UILabel!
    
    @IBOutlet weak var newBookingButton: UIButton!
    
    @IBOutlet weak var exitButton: UIButton!
    
    let actionCmd = PublishSubject<buttonType>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let email = KLCUserManager.userEmail() ?? ""
        welcomeLabel.text = "Welcome, \(email)"
        newBookingButton.setTitle("align.awesome.createButtonTxt".localString, for: .normal)
        exitButton.setTitle("align.awesome.exitButtonTxt".localString, for: .normal)
        actions()
    }
    
    private func actions() {
        newBookingButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.actionCmd.onNext(.create)
        }).disposed(by: rx.disposeBag)
        
        exitButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.actionCmd.onNext(.exit)
        }).disposed(by: rx.disposeBag)
    }
}
