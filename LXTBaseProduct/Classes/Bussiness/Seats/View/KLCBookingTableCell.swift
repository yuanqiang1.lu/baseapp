//
//  KLCBookingTableCell.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/5.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit

class KLCBookingTableCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var seatLabel: UILabel!
    
    @IBOutlet weak var timeSlotLabel: UILabel!
    
    var model:KLCBookingResultModel? {
        didSet {
            guard let model = model else {
                return
            }
            nameLabel.text = model.user?.email
            seatLabel.text = model.desk?.label
            timeSlotLabel.text = "\(model.booked_from.toISODate()?.toFormat("yyyy-MM-dd HH:mm") ?? "") to \(model.booked_to.toISODate()?.toFormat("yyyy-MM-dd HH:mm") ?? "")"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
