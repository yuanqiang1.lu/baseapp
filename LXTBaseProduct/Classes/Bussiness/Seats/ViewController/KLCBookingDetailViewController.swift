//
//  KLCBookingDetailViewController.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import UIKit

class KLCBookingDetailViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var DeskLabel: UILabel!
    
    @IBOutlet weak var timeSlotLabel: UILabel!
    
    @IBOutlet weak var editView: UIStackView!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    var model:KLCBookingResultModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIStandard.BGColor.BG2
        title = "Detail"
        bindAction()
        updateView()
    }
    
    private func bindAction() {
        cancelButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.showToast("cancel action", duration: 2)
        }).disposed(by: rx.disposeBag)
        
        editButton.rx.tap.subscribe(onNext: { [weak self] in
            let vc = KLCBookingViewController()
            vc.bookingModel = self?.model
            self?.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: rx.disposeBag)
    }
    
    private func updateView() {
        editView.isHidden = true
        
        guard let m = model else {
            return
        }
        
        nameLabel.text = "name : \(m.user?.email ?? "")"
        DeskLabel.text = "desk is: \(m.desk?.label ?? "")"
        
        timeSlotLabel.text = "time slot:\n \(m.booked_from.toISODate()?.toFormat("yyyy-MM-dd HH:mm") ?? "" ) to \(m.booked_to.toISODate()?.toFormat("yyyy-MM-dd HH:mm") ?? "")"
        
        let usereMail = KLCUserManager.userEmail()
        
        if let mail = usereMail, mail == m.user?.email {
            editView.isHidden = false
        } else {
            editView.isHidden = true
        }
    }

}
