//
//  KLCBookingListViewController.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class KLCBookingListViewController: UIViewController {
    
    private let viewModel = KLCSeatBookingViewModel()
    private let input = KLCSeatBookingViewModel.KLCInput()
    private var output:KLCSeatBookingViewModel.KLCOutput!
    
    var modelBKArr = [KLCBookingResultModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let cachedBKItemsArr = KLCBookingDesksManager.bookingList()
        if let arr = cachedBKItemsArr {
            modelBKArr = arr
            tableView.reloadData()
        }
        NotificationManager.instance.apns()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "align.awesome.bookingListVCTitle".localString
        layout()
        setupRx()
        input.bookinglistCmd.onNext(())
    }
    
    private func setupRx() {
        output = viewModel.transform(input: input)
        output.toastMsg.subscribe(onNext: { [weak self] (msg, time, block) in
            self?.showToast(msg, duration: time, complete: { block?() })
        }).disposed(by: rx.disposeBag)
        output.bookingListResult.subscribe( onNext: { [weak self] responseArr in
            self?.modelBKArr = responseArr
            self?.tableView.reloadData()
        }).disposed(by: rx.disposeBag)
        output.requestStatus.subscribe(onNext: { [weak self] status in
            if status == .failed {
                self?.tableView.reloadData()
            }
        }).disposed(by: rx.disposeBag)
    }
    
    private func layout() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        let headV = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 90))
        headV.addSubview(headerView)
        headerView.frame = headV.frame

        tableView.tableHeaderView = headV
        
        headerView.welcomeLabel.text = userName()
        
        headerView.actionCmd.subscribe(onNext: { [weak self] type in
            if type == .create {
                self?.createBooking()
            } else {
                self?.exit()
            }
        }).disposed(by: rx.disposeBag)
    }
    
    private func userName() -> String {
        let email = KLCUserManager.userEmail() ?? ""
        let isEmail = regexEmail(email)
        
        if isEmail {
            let splitArr = email.split(separator: "@")
            return "Hi,\(String(describing: splitArr.first ?? ""))"
        } else {
            return "Hi,\(email)"
        }
    }
    
    private func exit() {
        KLCPopupView.showAlert(title: "align.awesome.logout".localString,
                               detail: nil,
                               cancel: "align.awesome.cancelbtn".localString,
                               confirm: "align.awesome.surebtn".localString,
                               cancelBlock: nil) { [weak self] in
                                KLCUserManager.saveUserEmail("")
                                KLCUserManager.saveUserToken("")
                                self?.showToast("align.awesome.logingout".localString, duration: 1, complete: {
                                    KLCAppdelegate.configRootViewController()
                                })
        }

    }
    
    private func createBooking() {
        let vc = KLCBookingViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    lazy var headerView:KLCBookingHeaderView = {
        return R.nib.klcBookingHeaderView(owner:nil)!
    }()
    
    lazy var tableView: UITableView = {
        let table: UITableView = UITableView(frame: CGRect.zero, style: .plain)
        table.separatorStyle = .none
        table.backgroundColor = .clear
        table.rowHeight = UITableView.automaticDimension
        let v = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
        table.tableFooterView = v
        table.delegate = self
        table.dataSource = self
        table.emptyDataSetSource = self
        table.emptyDataSetDelegate = self
        table.register(R.nib.klcBookingTableCell)
        
        table.es.addPullToRefresh(animator: KLCRefreshHeaderAnimator()) { [weak self] in
            self?.input.bookinglistCmd.onNext(())
            self?.tableView.es.stopPullToRefresh()
        }
        return table
    }()
}

extension KLCBookingListViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelBKArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.klcBookingTableCell.identifier, for: indexPath) as! KLCBookingTableCell
        cell.model = modelBKArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        log.info("click item:\(indexPath.row)")
        
        let vc = KLCBookingDetailViewController()
        vc.model = modelBKArr[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension KLCBookingListViewController: EmptyDataSetDelegate, EmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        let status = output.requestStatus.value
        switch status {
        case .noNetwork:
            return R.image.no_net()
        case .requesting:
            return nil
        default:
            return R.image.no_report()
        }
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        var title = ""
        let status = output.requestStatus.value
        switch status {
        case .noNetwork:
            title = status.desc
        case .failed:
            title = "align.awesome.emptyDataDesc".localString
        default:
            title = ""
        }
        return NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : FONT(15),
                                                              .foregroundColor : UIStandard.FontColor.G5])
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor? {
        return .white
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView) -> Bool {
        return true
    }
}
