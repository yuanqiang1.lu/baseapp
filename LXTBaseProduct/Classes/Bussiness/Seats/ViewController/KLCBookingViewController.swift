//
//  KLCBookingViewController.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import UIKit

class KLCBookingViewController: UIViewController {

    @IBOutlet weak var selectDeskButton: UIButton!
    
    @IBOutlet weak var startTimeButton: UIButton!
    
    @IBOutlet weak var endTimeButton: UIButton!
    
    @IBOutlet weak var createButton: UIButton!
    
    private let viewModel = KLCSeatBookingViewModel()
    private let input = KLCSeatBookingViewModel.KLCInput()
    private var output:KLCSeatBookingViewModel.KLCOutput!
    
    var bookingModel:KLCBookingResultModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "align.awesome.newBookingVCTitle".localString
        
        selectDeskButton.setTitle("align.awesome.seatButtonTxt".localString, for: .normal)
        startTimeButton.setTitle("align.awesome.startTimeSlotTxt".localString, for: .normal)
        endTimeButton.setTitle("align.awesome.endTimeSlotTxt".localString, for: .normal)
        createButton.setTitle("align.awesome.createButtonTxt".localString, for: .normal)
        
        setupRx()
        bindAction()
        updateLayout()
    }
    
    private func setupRx() {
        output = viewModel.transform(input: input)
        output.toastMsg.subscribe(onNext: { [weak self] (msg, time, block) in
            self?.showToast(msg, duration: time, complete: { block?() })
        }).disposed(by: rx.disposeBag)
        output.actionResult.subscribe(onNext: { [weak self] finished in
            if finished {
                self?.navigationController?.popViewController(animated: true)
            }
        }).disposed(by: rx.disposeBag)
    }
    
    private func bindAction() {
        selectDeskButton.rx.tap.subscribe(onNext: { [weak self] in
            let vc = KLCDeskListViewController()
            vc.block = { model in
                self?.input.deskObj.accept(model)
                self?.selectDeskButton.setTitle("Desk is\(model.label)", for: .normal)
            }
            self?.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: rx.disposeBag)
        
        startTimeButton.rx.tap.subscribe(onNext: { [weak self] in
            var setting = DatePickerSetting()
            setting.dateMode = .dateAndTime
        UsefulPickerView.showDatePicker("align.awesome.timeSlotStart".localString, datePickerSetting: setting) { (date) in
                log.info("date:\(date.toString())")
            self?.input.startTime.accept(date)
            self?.startTimeButton.setTitle("Start:\(date.toFormat("yyyy-MM-dd HH:mm") )", for: .normal)
            }
        }).disposed(by: rx.disposeBag)
        
        endTimeButton.rx.tap.subscribe(onNext: { [weak self] in
            var setting = DatePickerSetting()
            setting.dateMode = .dateAndTime
            setting.minimumDate = Date()
            UsefulPickerView.showDatePicker("align.awesome.timeSlotEnd".localString, datePickerSetting: setting) { (date) in
                log.info("date:\(date.toString())")
                self?.input.endTime.accept(date)
                self?.endTimeButton.setTitle("End:\(date.toFormat("yyyy-MM-dd HH:mm") )", for: .normal)
            }
        }).disposed(by: rx.disposeBag)
        
        createButton.rx.tap.subscribe(onNext: { [weak self] in
            if self?.bookingModel != nil {
                self?.input.updateBookingCmd.onNext(())
            } else {
                self?.input.newBookingCmd.onNext(())
            }

        }).disposed(by: rx.disposeBag)
    }
    
    private func updateLayout() {
        guard let m = bookingModel else {
            return
        }
        input.bookingId.accept(m.id)
        selectDeskButton.setTitle("The desk:\(m.desk?.label ?? "")", for: .normal)
        startTimeButton.setTitle("Start:\(m.booked_from.toISODate()?.toFormat("yyyy-MM-dd HH:mm") ?? "" )", for: .normal)
        endTimeButton.setTitle("End:\(m.booked_to.toISODate()?.toFormat("yyyy-MM-dd HH:mm") ?? "")", for: .normal)
        createButton.setTitle("align.awesome.passwordComfirm".localString, for: .normal)
    }

}
