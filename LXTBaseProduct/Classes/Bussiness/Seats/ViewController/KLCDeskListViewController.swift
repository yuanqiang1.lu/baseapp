//
//  KLCDeskListViewController.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class KLCDeskListViewController: UIViewController {

    private let viewModel = KLCSeatBookingViewModel()
    private let input = KLCSeatBookingViewModel.KLCInput()
    private var output:KLCSeatBookingViewModel.KLCOutput!
    
    var modelArr = [KLCSeatsModel]()
    var block:((KLCSeatsModel) -> Void)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let cachedDskItemsArr = KLCBookingDesksManager.desksList()
        if let arr = cachedDskItemsArr {
            modelArr = arr
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIStandard.BGColor.BG2
        title = "align.awesome.deskListVCTitle".localString
        layout()
        setupRx()
        input.desklistCmd.onNext(())
    }
    
    private func setupRx() {
        output = viewModel.transform(input: input)
        output.toastMsg.subscribe(onNext: { [weak self] (msg, time, block) in
            self?.showToast(msg, duration: time, complete: { block?() })
        }).disposed(by: rx.disposeBag)
        output.deskListResult.subscribe( onNext: { [weak self] deskArr in
            self?.modelArr = deskArr
            self?.tableView.reloadData()
            log.info("refresh ...")
        }).disposed(by: rx.disposeBag)
        output.requestStatus.subscribe(onNext: { [weak self] status in
            if status == .failed {
                self?.tableView.reloadData()
            }
        }).disposed(by: rx.disposeBag)
    }
    
    private func layout() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(0)
            maker.leading.trailing.bottom.equalToSuperview()
        }
    }

    lazy var tableView: UITableView = {
        let table: UITableView = UITableView(frame: CGRect.zero, style: .grouped)
        table.separatorStyle = .none
        table.backgroundColor = .clear
        table.rowHeight = UITableView.automaticDimension
        let v = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
        v.backgroundColor = .red
        table.tableFooterView = v
        table.delegate = self
        table.dataSource = self
        table.emptyDataSetSource = self
        table.emptyDataSetDelegate = self
        table.register(R.nib.klcDeskItemCell)

        table.es.addPullToRefresh(animator: KLCRefreshHeaderAnimator()) { [weak self] in

            self?.tableView.es.stopPullToRefresh()
        }
        return table
    }()
}

extension KLCDeskListViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.klcDeskItemCell.identifier, for: indexPath) as! KLCDeskItemCell
        cell.deskItemLabel.text = modelArr[indexPath.row].label
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        log.info("click item:\(indexPath.row)")
        block?(modelArr[indexPath.row])
        navigationController?.popViewController(animated: true)
    }
    
}

extension KLCDeskListViewController: EmptyDataSetDelegate, EmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        let status = output.requestStatus.value
        switch status {
        case .noNetwork:
            return R.image.no_net()
        case .requesting:
            return nil
        default:
            return R.image.no_report()
        }
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        var title = ""
        let status = output.requestStatus.value
        switch status {
        case .noNetwork:
            title = status.desc
        case .failed:
            title = "align.awesome.emptyDataDesc".localString
        default:
            title = ""
        }
        return NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : FONT(15),
                                                              .foregroundColor : UIStandard.FontColor.G5])
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor? {
        return .white
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView) -> Bool {
        return true
    }
}
