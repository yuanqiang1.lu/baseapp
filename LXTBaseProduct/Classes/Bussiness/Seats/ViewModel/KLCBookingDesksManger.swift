//
//  KLCBookingDesksManger.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/7.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import Foundation

class KLCBookingDesksManager {
    
    static func saveList(_ list:[KLCBookingResultModel]) {
        do {
            try klicenRealm.write {
                klicenRealm.add(list, update: true)
            }
        } catch _ {
            log.error("save List error☠️")
        }
    }
    
    static func bookingList() -> [KLCBookingResultModel]? {
        return klicenRealm.objects(KLCBookingResultModel.self).map({$0})
    }
    
    static func desksList() -> [KLCSeatsModel]? {
        return klicenRealm.objects(KLCSeatsModel.self).map({$0})
    }
    
}
