//
//  KLCSeatBookingViewModel.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import KLCKit

class KLCSeatBookingViewModel: KLCBaseViewModel, KLCViewModelType {
    typealias KLCInput = KLCSeatBookingInput
    typealias KLCOutput = KLCSeatBookingOutput
    
    struct KLCSeatBookingInput {
        
        let email = BehaviorRelay(value: "")
        let startTime = BehaviorRelay<Date?>(value: nil)
        let endTime = BehaviorRelay<Date?>(value: nil)
        let deskObj = BehaviorRelay<KLCSeatsModel?>(value: nil)
        let bookingId = BehaviorRelay<Int?>(value: nil)
        
        let desklistCmd = PublishSubject<Void>()
        
        let bookinglistCmd = PublishSubject<Void>()
        
        let newBookingCmd = PublishSubject<Void>()
        
        let updateBookingCmd = PublishSubject<Void>()
    }
    
    struct KLCSeatBookingOutput {
        let requestStatus = BehaviorRelay(value: KLCRequstResultStatus.requesting)
        let toastMsg = PublishSubject<(String, Double, KLCSimpleBlock?)>()
        let deskListResult = BehaviorRelay(value: [KLCSeatsModel]())
        let bookingListResult = BehaviorRelay(value: [KLCBookingResultModel]())
        
        let actionResult = PublishSubject<Bool>()
    }
    
    func transform(input: KLCSeatBookingViewModel.KLCInput) -> KLCSeatBookingViewModel.KLCOutput {
        let output = KLCOutput()
        desksList(input,output)
        bookingsList(input,output)
        newBooking(input,output)
        updateBooking(input,output)
        return output
    }
}

extension KLCSeatBookingViewModel {
    
    private func desksList(_ input: KLCInput, _ output: KLCOutput) {
        input.desklistCmd.subscribe(onNext: {
            KLCNetwork.request(MultiTarget(KLCBookingApi.desksList))
                .mapArrayModel(KLCSeatsModel.self)
                .subscribe(onNext: { (rs) in
                    var deskArr = [KLCSeatsModel]()
                    rs?.forEach({ model in
                        if let mo = model {
                            deskArr.append(mo)
                        }
                    })
                    if !deskArr.isEmpty {
                        output.deskListResult.accept(deskArr)
                        output.requestStatus.accept(.success)
                    }
                    
                }, onError: { (error) in
                    output.requestStatus.accept(.failed)
                    output.toastMsg.onNext((KLCErrorDescription(error), 2, nil))
                }).disposed(by:self.disposeBag)
        }).disposed(by: self.disposeBag)
    }
    
    private func bookingsList(_ input: KLCInput, _ output: KLCOutput) {
        input.bookinglistCmd.subscribe(onNext: {
            output.requestStatus.accept(.requesting)
            CurrentViewController?.showGifToast(true)
            KLCNetwork.request(MultiTarget(KLCBookingApi.booksList))
                .mapArrayModel(KLCBookingResultModel.self)
                .subscribe(onNext: { (rs) in
                    defer {
                        CurrentViewController?.hideToast()
                    }
                    var bookArr = [KLCBookingResultModel]()
                    rs?.forEach({ model in
                        if let mo = model {
                            bookArr.append(mo)
                        }
                    })
                    if !bookArr.isEmpty {
                        output.bookingListResult.accept(bookArr)
                        output.requestStatus.accept(.success)
                    }
                    NotificationManager.instance.localPushWithLoadComplete()
                    KLCBookingDesksManager.saveList(bookArr)
                }, onError: { (error) in
                    defer {
                        CurrentViewController?.hideToast()
                    }
                    output.requestStatus.accept(.failed)
                    output.toastMsg.onNext((KLCErrorDescription(error), 2, nil))
                }).disposed(by:self.disposeBag)
        }).disposed(by: self.disposeBag)
    }
    
    private func newBooking(_ input: KLCInput, _ output: KLCOutput) {
        input.newBookingCmd.subscribe(onNext: {
            
            guard let desk = input.deskObj.value else {
                output.toastMsg.onNext(("Please select desk", 1, nil))
                return
            }
            
            let start = input.startTime.value?.toISO()
            let end = input.endTime.value?.toISO()
            guard let startTime = start, let endTime = end else {
                output.toastMsg.onNext(("The time slot is empty", 1, nil))
                return
            }
            
            let paramDic = ["booking[desk_id]":desk.id,
                            "booking[booking_at]":Date().toISO(),
                            "booking[booked_from]":startTime,
                            "booking[booked_to]":endTime] as [String : Any]
            KLCNetwork.request(MultiTarget(KLCBookingApi.newBooking(paramDic)))
                .mapModel(KLCBookingResultModel.self)
                .subscribe(onNext: { (rs) in
                    NotificationManager.instance.updateComplete(isNew:true)
                    output.actionResult.onNext(true)
                }, onError: { (error) in
                    if KLCErrorCode(error) < 300 {
                        output.actionResult.onNext(true)
                    } else {
                        output.toastMsg.onNext((KLCErrorDescription(error), 2, nil))
                    }
                }).disposed(by:self.disposeBag)
        }).disposed(by: self.disposeBag)
    }
    
    private func updateBooking(_ input: KLCInput, _ output: KLCOutput) {
        input.updateBookingCmd.subscribe(onNext: {
            
            guard let desk = input.deskObj.value else {
                output.toastMsg.onNext(("Please select desk", 1, nil))
                return
            }
            
            let start = input.startTime.value?.toISO()
            let end = input.endTime.value?.toISO()
            guard let startTime = start, let endTime = end else {
                output.toastMsg.onNext(("The time slot is empty", 1, nil))
                return
            }
            
            guard let bookingId = input.bookingId.value else {
                output.toastMsg.onNext(("The booking id is empty", 1, nil))
                return
            }
            
            let paramDic = ["booking[desk_id]":desk.id,
                            "booking[booking_at]":Date().toISO(),
                            "booking[booked_from]":startTime,
                            "booking[booked_to]":endTime] as [String : Any]
            KLCNetwork.request(MultiTarget(KLCBookingApi.updateBooking(index: bookingId, param: paramDic)))
                .mapModel(KLCUserLoginModel.self)
                .subscribe(onNext: { (_) in
                    log.info("It never return code")
                }, onError: { (error) in
                    if KLCErrorCode(error) < 300 {
                        NotificationManager.instance.updateComplete(isNew:false)
                        output.actionResult.onNext(true)
                    } else {
                        output.toastMsg.onNext((KLCErrorDescription(error), 2, nil))
                    }
                }).disposed(by:self.disposeBag)
        }).disposed(by: self.disposeBag)
    }
}
