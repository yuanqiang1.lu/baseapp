//
//  KLCFileApi.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/30.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import Moya

enum KLCFileApi: TargetType {
    /// 批量上传
    case uploadMult(imgs:[(String, Data)])
    
    /// 单个上传
    case upload(Data, String)
}

extension KLCFileApi {
    var baseURL: URL {
        return URL(string: KLCBaseUrl)!
    }
    
    var path: String {
        return "/file/upload/"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var task: Task {
        switch self {
        case .uploadMult(let datas):
            var array = [MultipartFormData]()
            datas.forEach { (dt) in
                array.append(MultipartFormData(provider: .data(dt.1),
                                               name:"files",
                                               fileName:dt.0,
                                               mimeType:"image/jpeg"))
            }
            return .uploadCompositeMultipart(array, urlParameters: [:])
        case .upload(let data, let name):
            return .uploadCompositeMultipart([MultipartFormData(provider: .data(data),
                                                                name:"file",
                                                                fileName:name,
                                                                mimeType:"image/jpeg")], urlParameters: [:])
        }
    }
    
    public var parameterEncoding:ParameterEncoding {
        return URLEncoding.default
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
}
