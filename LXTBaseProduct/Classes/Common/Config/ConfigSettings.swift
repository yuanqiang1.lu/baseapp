//
//  ConfigSettings.swift
//  LXTBaseProduct
//
//  Created by awesome on 2019/5/7.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation

class ConfigSettings:Codable {
    var amapKey = ""
    var wchatKey = ""
    var umkey = ""
    var pushKey = ""
}

#if DEBUG
let klcWechatQrImageName = "klc_wechat_qr_test"
#elseif Enterprise
let klcWechatQrImageName = ""
#elseif Adhoc
let klcWechatQrImageName = ""
#else
let klcWechatQrImageName = ""
#endif
