//
//  Defaults+extension.swift
//  LXTBaseProduct
//
//  Created by awesome on 2019/4/29.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import SwiftyUserDefaults
// MARK: app settings bundle
extension DefaultsKeys {
    static let passCodePerference = DefaultsKey<String?>("passcode_preference")
    static let devSwitch = DefaultsKey<Bool>("dev_switch", defaultValue: false)
}
