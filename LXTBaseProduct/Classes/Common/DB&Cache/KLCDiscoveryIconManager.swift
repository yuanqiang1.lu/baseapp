//
//  KLCDiscoveryIconManager.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/5/12.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import RealmSwift
import KLCKit

class KLCDiscoveryIconManager {
    
    static let share = KLCDiscoveryIconManager()
    
    var currentDiscoveryIcon:KLCDiscoveryModel? {
        let userId = KLCUserManager.share.currentUser?.user_id ?? 0
        return getIcons(owner: userId)
    }
    
    func saveIcons(model:KLCDiscoveryModel) {
        try? klicenRealm.write {
            klicenRealm.add(model, update: true)
        }
    }
    
    func getAllIcons() -> [KLCDiscoveryModel]? {
        return klicenRealm.objects(KLCDiscoveryModel.self).map({$0})
    }
    
    func getIcons(owner:Int) -> KLCDiscoveryModel? {
        return klicenRealm.object(ofType: KLCDiscoveryModel.self, forPrimaryKey: owner)
    }

}
