//
//  RealmConfig.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/19.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import KLCKit

let dbPath = docPath.appending("/cachedDB.realm")

/// 数据库realm
let klicenRealm = try! Realm(fileURL: URL(string: dbPath)!)

public func configRealm() {
    /// 如果要存储的数据模型属性发生变化,需要配置当前版本号比之前大
    let dbVersion : UInt64 = 2
    let config = Realm.Configuration(fileURL: URL.init(string: dbPath),
                                     inMemoryIdentifier: nil,
                                     syncConfiguration: nil,
                                     encryptionKey: nil,
                                     readOnly: false,
                                     schemaVersion: dbVersion,
                                     migrationBlock: { (migration, oldSchemaVersion) in
                                        print(migration, oldSchemaVersion)
                                    },
                                     deleteRealmIfMigrationNeeded: false,
                                     shouldCompactOnLaunch: nil,
                                     objectTypes: nil)
    
    Realm.Configuration.defaultConfiguration = config
    Realm.asyncOpen { (realm, error) in
        if let _ = realm {
            //print("Realm 服务器配置成功!")
        } else if let error = error {
            log.error("Realm config error：\(error.localizedDescription)")
        }
    }
}
