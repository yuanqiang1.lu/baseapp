//
//  UserManager.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/11.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import KLCKit
import Cache
import KeychainAccess
import SwiftyUserDefaults

extension String {
    static let userCacheKey = "user_cache_key"
}

let diskConfig = DiskConfig(name: "com.klicen", expiry: .never, maxSize: 0, directory: URL(string: docPath), protectionType: FileProtectionType.none)
let memoryConfig = MemoryConfig()
let KLCStorage = try? Storage(
    diskConfig: diskConfig,
    memoryConfig: memoryConfig,
    transformer: TransformerFactory.forCodable(ofType: KLCUserModel.self)
)

class KLCUserManager {
    static let share = KLCUserManager()
    private var _user:KLCUserModel?
    var currentUser:KLCUserModel? {
        set { // set 触发cache更新
            cacheUser(newValue)
            _user = newValue
        }
        get {
            return getCachedUser() ?? _user
        }
    }
    
    init() {
        // 登出操作删除缓存的user
        NotificationCenter.default.addObserver(self, selector: #selector(deleteUser), name: .klcDidLogoutNotificationName, object: nil)
    }
    
    private func getCachedUser() -> KLCUserModel? {
        if let suser = try? KLCStorage?.object(forKey: .userCacheKey) {
            return suser
        }
//        NotificationCenter.default.post(name: .klcNeedLoginNotificationName, object: nil)
        //log.info("----没有获取到缓存的用户信息,发出登录通知")
        return nil
    }
    
    func cacheUser(_ user: KLCUserModel?) {
        //cache
        guard let cache = KLCStorage else {
            return
        }
        
        guard let user = user else {
            return
        }
        
        try? cache.setObject(user, forKey: .userCacheKey)
    }
    
    @objc func deleteUser() {
        try? KLCStorage?.removeObject(forKey: .userCacheKey)
    }
    
    static func requestUser() {

    }
}

extension DefaultsKeys {
    static let lastUsername = DefaultsKey<String>("last_username", defaultValue: "")
    static let lastLoginType = DefaultsKey<Int>("last_login_type", defaultValue: LoginType.byMsg.rawValue)
    static let userAuthToken = DefaultsKey<String>("userAuthToken", defaultValue: "")
    static let userLoginEmail = DefaultsKey<String>("userLoginEmail", defaultValue: "")
}

enum LoginType: Int {
    case byMsg = 0
    case byPwd = 1
    case byWechat = 2
}

/// 登录信息储存
extension KLCUserManager {
    static func saveLoginInfo(_ username:String, pwd:String?) {
        let keychain = Keychain().synchronizable(true).accessibility(.afterFirstUnlock)
        keychain[username] = pwd
    }
    
    static func pwd(byUsername username:String) -> String? {
        let keychain = Keychain().synchronizable(true).accessibility(.afterFirstUnlock)
        let pwd = keychain[username]
        return pwd
    }
    
    static var lastLoginUsername: String {
        return Defaults[.lastUsername]
    }
    
    static func saveLastLoginUsername(_ phone: String) {
        Defaults[.lastUsername] = phone
    }
    
    static func userToken() -> String? {
        let token = Defaults[.userAuthToken]
        if token != "" {
            return token
        }
        return nil
    }
    
    static func saveUserToken(_ token: String) {
        Defaults[.userAuthToken] = token
    }
    
    static func userEmail() -> String? {
        let email = Defaults[.userLoginEmail]
        if email != "" {
            return email
        }
        return nil
    }
    
    static func saveUserEmail(_ email: String) {
        Defaults[.userLoginEmail] = email
    }

    static func saveLastLoginType(_ type: LoginType = .byMsg) {
        Defaults[.lastLoginType] = type.rawValue
    }
    
    static var lastLoginType:LoginType? {
        return LoginType(rawValue: Defaults[.lastLoginType])
    }
}
