//
//  VehicleManager.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/19.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import RealmSwift
import KLCKit
import SwiftyUserDefaults

extension DefaultsKeys {
    static let currentVehicleIdKey = DefaultsKey<Int>("currentVehicleIdKey", defaultValue: 0)
}

extension Notification.Name {
    // VIP车辆加载成功
    static let vipVehicleLoadedNotification = Notification.Name("vip_vehicle_loaded")
    
    // 切换车辆通知
    static let switchCurrentVehicleNotification = Notification.Name("switch_current_vehicle")
}

class KLCVehicleManager {
    
    static let share = KLCVehicleManager()
    /// 设置currentVehicle的时候会触发数据库更新, 只要本地缓存了车辆数据,就一定有当前车
    /// 切换车辆时候还要及时加载相对应的优惠券,查询相对应的数据信息
    var currentVehicle:KLCVehicleModel? = KLCVehicleManager.currentVehicleFromDisk() {
        willSet {
            if let vId = newValue?.vehicle_id {
                KLCVehicleManager.changeToCurrent(byId: vId)
                selectedVehicle = newValue // 同时将selected vehicle设为此车辆
                KLCVehicleManager.requestVehicleMileage(vid: vId)
                // 切换车辆成功
                NotificationCenter.default.post(name: .switchCurrentVehicleNotification, object: nil)
            }
        }
    }
    
    static var hasMoreVipVehilce: Bool {
        return (vipVehicles?.count ?? 0) > 1
    }
    
    /// 切换车辆时的选中车辆,每次从数据库中获取最新的
    var selectedVehicle:KLCVehicleModel? {
        set {
            if let vId = newValue?.vehicle_id {
                KLCVehicleManager.changeToSelected(byId: vId)
            }
        }
        get {
            return KLCVehicleManager.selectedVehicleFromDisk() ?? KLCVehicleManager.share.selectedVehicle
        }
    }
    
    init() {
        // 登出操作要把当前车辆置为空,否则会出现缓存问题
        NotificationCenter.default.addObserver(self, selector: #selector(cleanData), name: .klcDidLogoutNotificationName, object: nil)
        
        // 切换用户时候及时切换当前车辆
        NotificationCenter.default.addObserver(self, selector: #selector(switchUser), name: .vipVehicleLoadedNotification, object: nil)
    }
    
    @objc private func cleanData() {
        currentVehicle = nil
    }
    
    // 切换用户要将当前车切换
    @objc private func switchUser() {
        
        // 切换用户后保存的车和当前不同再做切换
        if KLCVehicleManager.currentVehicleFromDisk()?.vehicle_id != currentVehicle?.vehicle_id {
            currentVehicle = KLCVehicleManager.currentVehicleFromDisk()
        }
    }
    
    /// 添加/更新数据
    static func saveVehicle(_ vehicle:KLCVehicleModel) {
        //swiftlint:disable:next force_try
        try! klicenRealm.write {
            klicenRealm.add(vehicle, update: true)
        }
    }
    
    /// 添加/更新数据
    static func saveVehicle(_ vehicles: [KLCVehicleModel]) {
        //swiftlint:disable:next force_try
        try! klicenRealm.write {
            klicenRealm.add(vehicles, update: true)
        }
        if currentVehicleFromDisk() == nil {
            // 切换用户后,要找用户对应的车
            if Defaults[.currentVehicleIdKey] > 0, let v = vipVehicles?.filter("vehicle_id = %d", Defaults[.currentVehicleIdKey]).first { // 将上次标记下来的ID改为当前车辆
                KLCVehicleManager.share.currentVehicle = v //vehicle(byId: Defaults[.currentVehicleIdKey])
            } else if let firstVip = vipVehicles?.first { // 如果没有存储就把VIP的第一辆车作为当前车辆
                KLCVehicleManager.share.currentVehicle = firstVip
            }
        }
    }
    
    /// 获取当前用户的车辆列表
    static var vehicles: Results<KLCVehicleModel>? {
        guard let userId = KLCUserManager.share.currentUser?.user_id else {
            return nil
        }
        return klicenRealm.objects(KLCVehicleModel.self)
            .filter("owner_id = %d", userId)
            .sorted(byKeyPath: "vip", ascending: false)
            .sorted(byKeyPath: "vehicle_id")
    }
    
    static var vipVehicles: Results<KLCVehicleModel>? {
        guard let userId = KLCUserManager.share.currentUser?.user_id else {
            return nil
        }
        return klicenRealm.objects(KLCVehicleModel.self).filter("owner_id = %d AND vip = 1", userId).sorted(byKeyPath: "vehicle_id")
    }
    
    static var normalVehicles: Results<KLCVehicleModel>? {
        guard let userId = KLCUserManager.share.currentUser?.user_id else {
            return nil
        }
        return klicenRealm.objects(KLCVehicleModel.self).filter("owner_id = %d AND vip = 0", userId).sorted(byKeyPath: "id")
    }
    
    static func vehicle(byId id: Int) -> KLCVehicleModel? {
        return klicenRealm.object(ofType: KLCVehicleModel.self, forPrimaryKey: id)
    }
    
    static func currentVehicleFromDisk() -> KLCVehicleModel? {
        guard let userId = KLCUserManager.share.currentUser?.user_id else {
            return nil
        }
        return klicenRealm.objects(KLCVehicleModel.self).filter("is_current = true AND owner_id = %d", userId).first
    }
    
    static func selectedVehicleFromDisk() -> KLCVehicleModel? {
        guard let userId = KLCUserManager.share.currentUser?.user_id else {
            return nil
        }
        return klicenRealm.objects(KLCVehicleModel.self).filter("is_selected = true AND owner_id = %d", userId).first
    }
    
    /// 要用主键,所以普通车辆也要用vehilce_id
    static func updateMileage(byId id: Int, mileage: Int) {
        guard let vehicle = vehicle(byId: id) else {
            return
        }
        //swiftlint:disable:next force_try
        try! klicenRealm.write {
            vehicle.setValue(mileage, forKey: "mileage")
        }
    }
    
    /// 将vehicle_id的车辆数据库中修改为当前车辆
    static func changeToCurrent(byId id: Int) {
        guard let vehicle = vehicle(byId: id) else {
            return
        }
        // 将选中的车辆ID存下来,否则会被新数据刷掉
        Defaults[.currentVehicleIdKey] = id
        //swiftlint:disable:next force_try
        try! klicenRealm.write {
            vehicles?.setValue(false, forKey: "is_current")
            vehicle.setValue(true, forKey: "is_current")
        }
    }
    
    /// 将vehicle_id的车辆数据库中修改为选中车辆
    static func changeToSelected(byId id: Int) {
        guard let vehicle = vehicle(byId: id) else {
            return
        }
        //swiftlint:disable:next force_try
        try! klicenRealm.write {
            vehicles?.setValue(false, forKey: "is_selected")
            vehicle.setValue(true, forKey: "is_selected")
        }
    }
    
    /// 此处ID也是vehicle_id
    static func remove(byId id:Int) {
        guard let vehicle = vehicle(byId: id) else {
            return
        }
        //swiftlint:disable:next force_try
        try! klicenRealm.write {
            klicenRealm.delete(vehicle)
        }
        // 删除普通车辆以后,将当前车辆设置为选中车辆
        KLCVehicleManager.share.selectedVehicle = KLCVehicleManager.share.currentVehicle
    }
    
    /// 删除车辆
    static func removeAllVehicle() {
        guard let vs = vehicles else {
            return
        }
        //swiftlint:disable:next force_try
        try! klicenRealm.write {
            klicenRealm.delete(vs)// 从 Realm 中删除所有数据
        }
    }
}

// 加载车辆数据
extension KLCVehicleManager {
    static func requestVehicles() {
        
    }
    
    static func requestVehicleMileage(vid: Int) {
        
    }
}
