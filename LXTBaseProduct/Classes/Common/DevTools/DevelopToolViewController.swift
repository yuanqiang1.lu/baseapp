//
//  DevelopToolViewController.swift
//  LXTBaseProduct
//
//  Created by awesome on 2019/4/12.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import SwiftTheme

class DevelopToolViewController: UIViewController {
 
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "community_yes"), style: .plain, target: self, action: #selector(navigationBackClick))
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let value = ThemeManager.string(for: "Global.navBarTingColor") ?? ""
        let sectionTitleLabel = UILabel(frame: CGRect(x: 15, y: 116, width: SCREEN_WIDTH-30, height: 20))
        sectionTitleLabel.text = "颜色:\(value)"
        sectionTitleLabel.textColor =  ThemeManager.color(for: "Global.navBarTingColor") ?? UIColor.red
        sectionTitleLabel.font = FONT(14)
        view.addSubview(sectionTitleLabel)
    }
    
    @objc func navigationBackClick() {
        if ((self.navigationController != nil) || (self.presentedViewController != nil ) && self.children.count == 1) {
            _ = dismiss(animated: true, completion: nil)
            
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let url = "http://www.google-swift.com"
        let isPushed = KLCAppdelegate.router?.present(url) != nil
        if isPushed {
        } else {
            KLCAppdelegate.router?.open(url)
        }
    }
}

protocol ExampleProtocol {
    /// veriable to description
    var simpleDescription:String { get }
    mutating func adjust()
}

struct SimpleStruct: ExampleProtocol {
    var simpleDescription: String = "A simple structure"
    mutating func adjust() {
        simpleDescription += "(adjusted)"
    }
}
