//
//  Repository.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/6/24.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import EVReflection

class Repository: EVObject {
    var identifier: NSNumber?
    var language: String?
    var url: String?
}
