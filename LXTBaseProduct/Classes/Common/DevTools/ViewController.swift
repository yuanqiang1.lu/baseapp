//
//  ViewController.swift
//  LXTBaseProduct
//
//  Created by luyuanqiang on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import UIKit
import GDPerformanceView_Swift
import KLCKit
import Cache
import Rswift
import RxCocoa
import NSObject_Rx
import SnapKit
import SwiftyUserDefaults
import SkeletonView

class ViewController: UIViewController {
    
    private let icons = ["msg_topic","msg_service","msg_klicen"]
    private let titles = ["地图测试入口","Web测试入口","自驾游测试入口"]
    private let remarks = ["地图功能","Web展示","自驾游入口显示"]
    
    // MARK: lazy loading
    lazy var tableView:UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: .grouped)
        tableView.backgroundColor = UIStandard.BGColor.BG1
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 0.01))
        tableView.estimatedRowHeight = 80
        tableView.sectionFooterHeight = 0
        tableView.separatorColor = UIStandard.BGColor.BG1
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.rowHeight = UITableView.automaticDimension
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.navigationBar.isHidden = true
        title = "首页"
        let appName = "appname.feature".localString
        log.warning("Here comes from LXTKit : \(KLCKitVersionNumber) name:\(appName)")
        view.addSubview(tableView)
        tableView.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
//        PerformanceMonitor.shared().start()
//
//        networkStatus()
//        rxTest()
//        labelTest()
//        cacheTest()
//        htmlPresentTest()
//        verboseInfo()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "··· ", style: .done, target: nil, action: nil)
    }
    
    func rxTest() {
        let button = UIButton()
        button.setTitle("DSView", for: .normal)

        //button.layer.cornerRadius = 10.0
        button.button(settings: UIStandard.BTNStyle.BTN1_1)
        button.theme_setTitleColor("ChangeThemeCell.buttonTitleColorNormal", forState: .normal)
        button.theme_setTitleColor("ChangeThemeCell.buttonTitleColorNormal", forState: .highlighted)
        button.theme_backgroundColor = "ChangeThemeCell.buttonBackgroundColor"
        button.logAction = "something awesome"
        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.height.equalTo(30.0)
            make.width.equalTo(100.0)
            make.center.equalTo(view)
        }

        button.rx.tap.subscribe(onNext: { _ in
            _ = KLCAppdelegate.router?.present(WebRoutePath, wrap: KLCNavigationController.self)
            },
                                onCompleted: {
                                    log.warning(" finished ")
        }).disposed(by: rx.disposeBag)
    }
    
    func verboseInfo() {
//        let diaryList:String = Bundle.main.path(forResource: "Info", ofType:"plist")!
//        let data:NSArray = NSArray(contentsOfFile:diaryList)!
//        let diaries:NSDictionary = data[0] as! NSDictionary
//        let nsDictionary = NSDictionary(contentsOfFile: diaryList)
    }
    
    func htmlPresentTest() {
        // swiftlint:disable:next superfluous_disable_command
        _ = KLCAppdelegate.router?.present("http://www.baidu.com")
    }
    
    func labelTest() {
        let label = UILabel(frame: CGRect(x: KScaleH(100), y: KScaleH(260), width: KScaleH(200), height: KScaleH(99)))
        label.numberOfLines = 0
        SkeletonAppearance.default.tintColor = .red
        let attr = NSMutableAttributedString(string: """
                                                Struct的默认初始化器只有两种：
                                                1 在全部属性有默认值下会有一个()的默认初始化器生成
                                                2 还会自动生成一个为每个属性赋值的memberwise初始化器，不管有没有给结构体中的属性赋值
                                                """,
                                             attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G2,
                                                          NSAttributedString.Key.font : ScaleFONT(16),
                                                          .paragraphStyle : UIStandard.paragraph1_5] )
        label.attributedText = attr
        label.lineBreakMode = .byTruncatingTail
        view.addSubview(label)
    }
    
    func cacheTest() {
       
    }
    
    @objc func savePwd() {
//        KLCUserManager.saveLoginInfo("13438157545", pwd: "wzq123")
//        KLCImgCodeViewController.show(in: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let decimal = (12.012).preciseDecimal(length: 2)
        let passcode = Defaults[.passCodePerference] ?? ""
        let develop = Defaults[.devSwitch]
        guard passcode != "", develop else {
            log.error("klcfeature.error".localString)
            log.error(["preciseDecimal":decimal, "passcode":passcode, "develop->":develop])
            return
        }
        if passcode == "klicen" {
            showToast("已开启", duration: 1.0)
            log.warning("已经开启 免登录模式")
        } else {
            log.verbose(["preciseDecimal":decimal, "passcode":passcode, "develop->":develop])
            showToast("开启失败", duration: 1.0)
            log.warning("免登录开启失败")
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        log.warning("💀内存报警了...")
        
        let file = FileDestination()
        
        if file.deleteLogFile() {
            log.warning("删除成功")
        } else {
            log.error("删除失败-----")
        }
    }
}

extension ViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? titles.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell.init(style: .default, reuseIdentifier: nil)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        }
        
        let header = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 44))
        header.backgroundColor = UIStandard.BGColor.BG1
        let sectionTitleLabel = UILabel(frame: CGRect(x: 15, y: 16, width: SCREEN_WIDTH-30, height: 20))
        sectionTitleLabel.text = "其他测试"
        sectionTitleLabel.textColor = UIStandard.FontColor.G4
        sectionTitleLabel.font = FONT(14)
        header.addSubview(sectionTitleLabel)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.01 : 44.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0: do {
    
            }
                
            case 1: do {

            }

            case 2: do {
                let dev = KLCAppdelegate.router?.present(DevelopToolViewController(),wrap: UINavigationController.self)
                dev?.title = "present dev"
            }
                
            default:
                log.warning("default print")
            }
        }
    }
}
