//
//  KLCDiscoveryIconModel.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/4/12.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import KLCKit
import HandyJSON
import RealmSwift
import Realm

class KLCDiscoveryModel: Object, HandyJSON {
    @objc dynamic var welcome:KLCDiscoveryWelcomeModel?
    /// 分类列表
    var list = List<KLCDiscoverySectionModel>()
    
    @objc dynamic var owner = KLCUserManager.share.currentUser?.user_id ?? 0
    
    override static func primaryKey() -> String? {
        return "owner"
    }
    
    func mapping(mapper: HelpingMapper) {
        // 把daily_report转换成可一对多的List
        mapper <<<
            list<--TransformOf<List<KLCDiscoverySectionModel>,[[String:Any]]>(fromJSON: {(rawArray)->List<KLCDiscoverySectionModel> in
                let list=List<KLCDiscoverySectionModel>()
                let array=JSONDeserializer<KLCDiscoverySectionModel>.deserializeModelArrayFrom(array: rawArray) as? [KLCDiscoverySectionModel]
                list.append(objectsIn: array ?? [])
                return list
            }, toJSON: { (list) -> [[String:Any]]? in
                if let array = list, !array.isEmpty {
                    
                }
                return []
            })
    }
}

// 顶部欢迎文案
class KLCDiscoveryWelcomeModel: Object, HandyJSON {
    /// 标题
    @objc dynamic var title = ""
    /// 描述
    @objc dynamic var remarks = ""
    /// 图片地址
    @objc dynamic var cover_photo = ""
    
    var owner = LinkingObjects(fromType: KLCDiscoveryModel.self, property: "welcome")
}

class KLCDiscoverySectionModel: Object, HandyJSON {
    /// 分类名
    @objc dynamic var name:String = ""
    /// 图标列表
    var icon_list = List<KLCDiscoveryIconModel>()
    
    var owner = LinkingObjects(fromType: KLCDiscoveryModel.self, property: "list")
    
    func mapping(mapper: HelpingMapper) {
        // 把daily_report转换成可一对多的List
        mapper <<<
            icon_list<--TransformOf<List<KLCDiscoveryIconModel>,[[String:Any]]>(fromJSON: {(rawArray)->List<KLCDiscoveryIconModel> in
                let list=List<KLCDiscoveryIconModel>()
                let array=JSONDeserializer<KLCDiscoveryIconModel>.deserializeModelArrayFrom(array: rawArray) as? [KLCDiscoveryIconModel]
                list.append(objectsIn: array ?? [])
                return list
            }, toJSON: { (list) -> [[String:Any]]? in
                if let array = list, !array.isEmpty {
                    
                }
                return []
            })
    }
}

// iconModel
class KLCDiscoveryIconModel: Object, HandyJSON {
    
    enum CodeType:String,HandyJSONEnum {
        // 4s
        case xyt4s = "4S"
        /// 车辆估值
        case vehicleValuation = "USED_CAR"
        /// 违章查询
        case peccancy = "PECCANCY"
        /// 折扣加油
        case discountRefuel = "DISCOUNT_REFUEL"
        /// 帮助中心
        case help = "HELP"
        /// 自驾游
        case selfDrive = "SELF_DRIVE"
        /// 行车数据
        case vehicleData = "VEHICLE_DATA"
        /// 组队开车
        case drivingTeam = "SHARING"
        /// 车辆提醒
        case vehicleAlert = "VEHICLE_NOTICE"
    }
    
    enum IconType:Int,HandyJSONEnum {
        case native = 1
        case h5
    }
    /// 图标名
    @objc dynamic var name = ""
    @objc dynamic var id = 0
    /// 唯一识别码
    @objc dynamic var code = ""
    /// 图片地址
    @objc dynamic var url = ""
    /// 图标类型（1、原生，2、H5页面
    @objc dynamic var type = 1
    /// 图标类型m， 外部使用
    var iconType:IconType {
        get {
            return IconType.init(rawValue: type) ?? .native
        } set {
            type = newValue.rawValue
        }
    }
    /// 跳转页面（原生页面路径或url地址）
    @objc dynamic var target = ""
    /// 是否可用
    @objc dynamic var active = false
    /// 不可用时提示信息
    @objc dynamic var tips = ""
    /// 用户是否能访问
    @objc dynamic var access = false
    
    /// 默认图片
    var normalImage:UIImage? {
        var imageName = "sidemenu_company"
        if let codeType = CodeType.init(rawValue: code) {
            switch codeType {
            case .xyt4s:
                imageName = "discovery_xyt"
            case .vehicleValuation:
                imageName = "discovery_used_car"
            case .peccancy:
                imageName = "discovery_illegal"
            case .discountRefuel:
                imageName = "discovery_discount"
            case .help:
                imageName = "discovery_help"
            case .selfDrive:
                imageName = "discovery_round"
            case .vehicleData:
                imageName = "discovery_driving"
            case .drivingTeam:
                imageName = "discovery_team"
            case .vehicleAlert:
                imageName = "discovery_vehicle_alert"
            }
        }
        
        return UIImage(named: imageName)
    }
    
    var owner = LinkingObjects(fromType: KLCDiscoverySectionModel.self, property: "icon_list")
    
    override class func ignoredProperties() -> [String] {
        return ["iconType","normalImage"]
    }
}
