//
//  KLCListModel.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/4/23.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import KLCKit
import HandyJSON

class KLCListModel<T:KLCBaseModel>: KLCBaseModel {
    var list:[T]?
}
