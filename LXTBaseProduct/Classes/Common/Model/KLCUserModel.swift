//
//  KLCUserModel.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/19.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit

class KeyVerboseModel: KLCBaseModel, Codable {
    var key = ""
    var verbose = ""
}

class WechatModel:KLCBaseModel, Codable {
    var nickname: String?
    var avatar_uri: String?
}

class KLCUserModel: KLCBaseModel, Codable {
    var user_id = 0
    var nickname = ""
    var avatar_uri = ""
    var vip = false
    var phone = ""
    var birthday = ""
    var has_password = false
    var gender:KeyVerboseModel?
    var user_type = 1
    var tags:[String]?
    var wechat:WechatModel?
}
