//
//  KLCVehicleModel.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/19.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import KLCKit
import HandyJSON
import RealmSwift

class KLCVehicleListModel: KLCBaseModel {
    var list:[KLCVehicleModel]?
}

class KLCVehicleModel: Object, HandyJSON {
    @objc dynamic var id = 0
    @objc dynamic var vehicle_id = 0
    @objc dynamic var plate_number = ""
    @objc dynamic var vin = ""
    @objc dynamic var mileage = 0
    @objc dynamic var engine_number = ""
    @objc dynamic var first_reg_date = ""
    @objc dynamic var install_date = ""
    @objc dynamic var nickname = ""
    @objc dynamic var installed = true
    @objc dynamic var vus: KLC4SModel?
    @objc dynamic var vehicle_type:KLCVehicleTypeModel?
    @objc dynamic var service:KLCServiceModel?
    @objc dynamic var vip = false
    //车主id
    @objc dynamic var owner_id = KLCUserManager.share.currentUser?.user_id ?? 0
    @objc dynamic var is_current = false // 只有vip才能作为current 车辆
    @objc dynamic var is_selected = false // 车库切车list view 需要选中效果,所以加个标记
    
//    // 试试好不好用
//    func mapping(mapper: HelpingMapper) {
//        mapper.specify(property: &first_reg_date) { (string) -> Date in
//            return string.toISODate()?.date ?? Date()
//        }
//    }
//
    override static func primaryKey() -> String? {
        return "vehicle_id"
    }
    
    /// 演示车的判断逻辑 根据ID判断
    func isDemoVehicle() -> Bool {
        return vehicle_id == 10018796
    }
    
    /// 服务是否过期
    func outofService() -> Bool {
        return !(service?.keep_now ?? true)
    }
    
    /// 给普通车辆生成一个vehicle_id, 因为vehicle_id 是主键
    func createVehicleIdWithNormalVehicle() {
        if let uid = KLCUserManager.share.currentUser?.user_id {
            vehicle_id = id + uid
        }
    }
}

class KLCVehicleTypeModel: Object, HandyJSON {
    @objc dynamic var id = 0
    @objc dynamic var type = ""
    @objc dynamic var model = ""
    @objc dynamic var serial = ""
    @objc dynamic var serial_id = 0
    @objc dynamic var brand_id = 0
    @objc dynamic var brand = ""
    @objc dynamic var brand_image_url = ""
    let owners = LinkingObjects(fromType: KLCVehicleModel.self, property: "vehicle_type")
    
//    override static func primaryKey() -> String? {
//        return "id"
//    }
}

class KLC4SModel: Object, HandyJSON {
    @objc dynamic var shop_id = 0
    @objc dynamic var oper_state = false
    @objc dynamic var region = ""
    @objc dynamic var address = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var is_accept_today = false
    @objc dynamic var name = ""
    @objc dynamic var after_service_phone = ""
    @objc dynamic var emergency_phone = ""
    @objc dynamic var store_phone = ""
    @objc dynamic var photo = ""
    @objc dynamic var thumb_photo = ""
    @objc dynamic var appointment_notice = ""
    @objc dynamic var characteristics = ""
    let owners = LinkingObjects(fromType: KLCVehicleModel.self, property: "vus")
    
    override static func primaryKey() -> String? {
        return "shop_id"
    }
}

class KLCServiceModel: Object, HandyJSON {
    @objc dynamic var service_start_date = ""
    @objc dynamic var service_end_date = ""
    @objc dynamic var location_start_date = ""
    @objc dynamic var location_end_date = ""
    @objc dynamic var keep_days = 0
    @objc dynamic var keep_now = false
    let owners = LinkingObjects(fromType: KLCVehicleModel.self, property: "service")
}

// 简易车品牌,车系,车型
class KLCVehicleSimpleBrandModel:Object, HandyJSON {
    @objc dynamic var brand_image_url = ""
    @objc dynamic var brand = ""
    @objc dynamic var brand_id = 0
    @objc dynamic var firstLetter: String {
        return brand.findFirstLetter()
    }
    
    override static func primaryKey() -> String? {
        return "brand_id"
    }
}

class KLCVehicleSimpleSerialModel:Object, HandyJSON {
    @objc dynamic var brand_id = 0
    @objc dynamic var serial = ""
    @objc dynamic var serial_id = 0
    override static func primaryKey() -> String? {
        return "serial_id"
    }
}

class KLCVehicleSimpleTypeModel:Object, HandyJSON {
    @objc dynamic var serial_id = 0
    @objc dynamic var model = ""
    @objc dynamic var vehicle_type_id = 0
    override static func primaryKey() -> String? {
        return "vehicle_type_id"
    }
}

class KLCVehicleBrandListModel: KLCBaseModel {
    var list:[KLCVehicleSimpleBrandModel]?
}

class KLCVehicleSerialListModel: KLCBaseModel {
    var list:[KLCVehicleSimpleSerialModel]?
}

class KLCVehicleTypeListModel: KLCBaseModel {
    var list:[KLCVehicleSimpleTypeModel]?
}
