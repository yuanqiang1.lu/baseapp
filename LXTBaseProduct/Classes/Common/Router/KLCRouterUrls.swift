//
//  KLCConstants.swift
//  LXTBaseProduct
//
//  Created by luyuanqiang on 2019/4/12.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation

let breakLineStr = "\n==============================\n==============================\n"
// swiftlint:disable:next force_cast
let KLCAppdelegate = UIApplication.shared.delegate as! AppDelegate

let schemeName = "klicen8"
// The moduel mapping
let userModel = "\(schemeName)://user/<username>"
let devRoutePath = "\(schemeName)://develop/"
let userRouterPath = "\(schemeName)://user/xxx" 
let WebRoutePath = "\(schemeName)://web/<param>/<module>"
/// 消息容器
let MessageRouterPath = "\(schemeName)://message"
/// 车辆提醒
let VehicleMessageRouterPath = "\(schemeName)://vehicleAlertMessage"
/// 车辆提醒设置
let VehicleAlertSettingRouterPath = "/vehicle_notify/setting"
/// 消息通知
let MessageNotificationRouterPath = "\(schemeName)://messageNotification"
/// 系统消息
let systemMessageRouterPath = "\(schemeName)://systemMessage"
/// 服务消息
let serviceMessageRouterPath = "\(schemeName)://serviceMessage"
/// 登录
let LoginRoutePath = "\(schemeName)://login"
/// 设置
let SettingRouterPath = "\(schemeName)://setting"
/// 地图
let MapRouterPath = "\(schemeName)://map"
/// 我的4s
let My4SRouterPath = "/xyt/detail"

/// 我的加油卡
let MyOilCardRouterPath = "\(schemeName)://oilcard"

/// 我的优惠券
let MyCouponsRouterPath = "\(schemeName)://coupons"

/// 电子围栏
let SafeLockRouterPath = "\(schemeName)://safeLock"

/// 个人信息
let PersonalInfoRouterPath = "\(schemeName)://personalInfo"

/// 修改密码
let ChangePwdRouterPath = "\(schemeName)://changePwd"

/// 添加密码
let AddPwdRouterPath = "\(schemeName)://addPwd"

/// 绑定新手机号
let BindNewPhoneRouterPath = "\(schemeName)://bindNewPhone"

/// 修改绑定手机号
let ChangeBindPhoneRouterPath = "\(schemeName)://changeBindPhone"

/// 证件管理
let CertManageRouterPath = "\(schemeName)://certmManage"

/// 隐私设置
let PrivacySettingsPath = "\(schemeName)://privacySettings"

/// 我的车库
let MyGarageRouterPath = "\(schemeName)://myGarage"

/// 行车数据
let DrivingDataRouterPath = "/vehicle/driving_data"

/// 行车数据历史记录
let DrivingHistoryRouterPath = "\(schemeName)://drivingHistory"

/// 赚汽油
let EarnGasRouterPath = "\(schemeName)://earnGas"

/// 4s店预约
let OnlineOrder4SRouterPath = "\(schemeName)://4sOnlineOrder"

/// 车辆位置共享详情
let VehicleShareDetailRouterPath = "\(schemeName)://vehicleShareDetail"

/*********************************************************************************
 web url
*********************************************************************************/
let heightParam = "navBarHeight=\(NavBarHeight)&statusBarHeight=\(StatusBarHeight)&vehicleId=\(KLCVehicleManager.share.currentVehicle?.vehicle_id ?? 0)"
/// 我的汽油
let myGasWebUrl = KLCHost + "/static/myGasoline/index.html?\(heightParam)"

/// 会员等级
let vipWebUrl = KLCHost + "/static/vipGrade/index.html?\(heightParam)"

/// 车辆当前位置
let vehicleLocWebUrl = KLCHost + "/static/vehicleCurrentPosition/index.html"

/// 位置共享
let locShareWebUrl = KLCHost + "/shareLocation.html"

/// 服务协议
let serviceAgreementWebUrl = KLCHost + "/staticHtml.html?\(heightParam)#/serviceAgreement"

/// 微信提醒人
func wxAlertPersonShare(share_param:String) -> String {
    return KLCHost + "/information.html?share_param=\(share_param)#/invite"
}

/// 隐私政策
let privacyWebUrl = KLCHost + "/staticHtml.html?\(heightParam)#/privacyPolicy"

/// 电子保函
let electronicGuaranteeUrl = KLCHost + "/electronicGuarantee.html?\(heightParam)#/"

/// 违章查询
let peccancyWebUrl = KLCHost + "/peccancy.html?\(heightParam)"

/// 帮助中心url
let helpWebUrl = KLCHost + "/help.html?\(heightParam)"

///在线续约
let renewWebUrl = KLCHost + "/contract.html?\(heightParam)"
///自驾游
let selfDriveWebUrl = KLCHost + "/selfDriving.html?\(heightParam)"
///天气限行
let weatherWebUrl = KLCHost + "/weather.html?\(heightParam)"
/// 车辆估值
let vehicleValuation = KLCHost + "/valuation.html?\(heightParam)#/"

/// 凯励程能做什么
let klicenFuncWebUrl = KLCHost + "/staticHtml.html?\(heightParam)#/appFunction"
