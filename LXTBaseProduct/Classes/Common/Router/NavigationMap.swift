//
//  NavigationMap.swift
//  LXTBaseProduct
//
//  Created by luyuanqiang on 2019/4/11.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import SafariServices
import UIKit
import KLCKit

enum NavigationMap {
    static func initialize(navigator: NavigatorType) {
        navigator.register(devRoutePath) { _, _, _ in
            return DevelopToolViewController()
        }
        
        navigator.register("http://<path:_>", self.webViewControllerFactory)
        navigator.register("https://<path:_>", self.webViewControllerFactory)
        
        navigator.handle("klicen8://alert", self.alert(navigator: navigator))
        navigator.handle("klicen8://<path:_>") { (_, _, _) -> Bool in
            // No router match, do analytics or fallback function here
            log.verbose("[Navigator] NavigationMap.\(#function):\(#line) - global fallback function is called")
            return true
        }
        
        navigator.register(LoginRoutePath) { _, _, _ in
            return R.storyboard.login().instantiateInitialViewController()!
        }
        
    }
    
    private static func webViewControllerFactory(
        url: URLConvertible,
        values: [String: Any],
        context: Any?
        ) -> UIViewController? {
        guard let url = url.urlValue else { return nil }
        
        return SFSafariViewController(url: url)
    }
    
    private static func alert(navigator: NavigatorType) -> URLOpenHandlerFactory {
        return { url, values, context in
            guard let title = url.queryParameters["title"] else { return false }
            let message = url.queryParameters["message"]
            KLCPopupView.showAlert(title: title, detail: message, confirm: "OK", confirmBlock: nil)
            return true
        }
    }
}
