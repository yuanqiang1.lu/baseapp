//
//  ActionButton+Extension.swift
//  LXTBaseProduct
//
//  Created by luyuanqiang on 2019/4/13.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//
import KLCKit

extension UIButton {
    private struct AssociatedKeys {
        static var actionKey = "lxt_button_tap"
    }
    
    @IBInspectable var logAction: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.actionKey) as? String
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.actionKey,
                                     newValue,
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    @objc func lxt_sendAction(_ action: Selector, to target: Any?, for event: UIEvent?) {
        
//        if Date().timeIntervalSince(lastClickDate) > forbidInterval { // 1秒内禁止点击
            self.lxt_sendAction(action, to: target, for: event)
            if let logAct = logAction,
                actions(forTarget: target, forControlEvent: .touchUpInside)?.first == action.description {
                Statistics.logEvent(logAct)
            }
//            lastClickDate = Date()
//        }
        
    }
}
