//
//  RXHelper.swift
//  LXTBaseProduct
//
//  Created by luyuanqiang on 2019/4/13.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation

func castOrThrow<T>(_ resultType: T.Type, _ object: Any) throws -> T {
    guard let returnValue = object as? T else {
        throw RxCocoaError.castingError(object: object, targetType: resultType)
    }
    return returnValue
}

func swizzleMethod(_ originalFunc: String, newFunc: String, target: AnyObject, isClassFunc: Bool = false) {
    let cls: AnyClass? = isClassFunc ? object_getClass(target) : target as? AnyClass
    
    let originalSelector = Selector(originalFunc)
    let swizzledSelector = Selector(newFunc)
    
    guard let originalMethod = class_getInstanceMethod(cls, originalSelector) else {
        fatalError("can't find originalMethod")
    }
    
    guard let swizzledMethod = class_getInstanceMethod(cls, swizzledSelector) else {
        fatalError("can't find swizzledMethod")
    }
    
    let didAddMethod = class_addMethod(cls,
                                       originalSelector,
                                       method_getImplementation(swizzledMethod),
                                       method_getTypeEncoding(swizzledMethod))
    
    if didAddMethod {
        class_replaceMethod(cls,
                            swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod))
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
}
