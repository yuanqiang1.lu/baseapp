//
//  KLCRefreshFooterAnimator.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/4/11.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit

var KLCRefreshFooter:KLCRefreshFooterAnimator { return KLCRefreshFooterAnimator()}

class KLCRefreshFooterAnimator: UIView,ESRefreshProtocol, ESRefreshAnimatorProtocol {
    
    var loadingMoreDescription = "松开加载更多"
    var noMoreDataDescription = "--没有更多了--"
    var loadingDescription = "KLICEN 为您玩命加载..."
    
    var view: UIView {return self}
    var insets: UIEdgeInsets = UIEdgeInsets.zero
    var trigger: CGFloat = 48.0
    var executeIncremental: CGFloat = 48.0
    var state: ESRefreshViewState = .noMoreData
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        addSubview(titleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func refreshAnimationBegin(view: ESRefreshComponent) {
        titleLabel.text = loadingDescription
    }
    
    func refreshAnimationEnd(view: ESRefreshComponent) {
        switch state {
        case .noMoreData:
            titleLabel.text = noMoreDataDescription
        default:
            titleLabel.text = ""
        }
    }
    
    func refresh(view: ESRefreshComponent, progressDidChange progress: CGFloat) {

    }
    
    func refresh(view: ESRefreshComponent, stateDidChange state: ESRefreshViewState) {
        self.state = state
        switch state {
        case .refreshing :
            titleLabel.text = loadingDescription
        case .autoRefreshing :
            titleLabel.text = loadingDescription
        case .noMoreData,.pullToRefresh:
            titleLabel.text = noMoreDataDescription
        default:
            titleLabel.text = ""
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.frame = self.bounds
    }
    
    // lazy loading
    private let titleLabel: UILabel = {
        let label = UILabel.init(frame: CGRect.zero)
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = UIStandard.FontColor.G4
        label.textAlignment = .center
        return label
    }()
    
}
