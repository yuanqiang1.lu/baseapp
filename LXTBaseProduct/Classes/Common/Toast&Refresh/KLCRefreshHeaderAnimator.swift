//
//  KLCRefreshHeader.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/4/11.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
@_exported import ESPullToRefresh

var KLCRefreshHeader:KLCRefreshHeaderAnimator {
    return  KLCRefreshHeaderAnimator()
}

class KLCRefreshHeaderAnimator: UIView,ESRefreshProtocol,ESRefreshAnimatorProtocol {
    
    // 图片count
    var count = 42
    // 图片
    var prefix = "Loader_00"
    
    var view: UIView { return self }
    var insets: UIEdgeInsets = UIEdgeInsets.zero
    var trigger: CGFloat = 56.0
    var executeIncremental: CGFloat = 56.0
    var state: ESRefreshViewState = .pullToRefresh
    var isAnimation = false
    
    // 是否是首页
    var isHome:Bool = false {
        didSet {
            prefix = isHome ? "loader1_00" : "Loader_00"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func refreshAnimationBegin(view: ESRefreshComponent) {
        imageView.center = self.center
    }
    
    func refreshAnimationEnd(view: ESRefreshComponent) {
        imageView.stopAnimating()
        imageView.image = UIImage(named: "\(self.prefix)0")
        isAnimation = false
    }
    
    func refresh(view: ESRefreshComponent, progressDidChange progress: CGFloat) {
        imageView.center = self.center
        
    }
    
    func refresh(view: ESRefreshComponent, stateDidChange state: ESRefreshViewState) {

        self.state = state
        
        if !isAnimation {
            var images = [UIImage]()
            for index in 0..<count {
                if let image = UIImage(named: "\(self.prefix)\(index)") {
                    images.append(image)
                }
            }
            
            if images.isEmpty {return}
            
            imageView.animationDuration = 1
            imageView.animationImages = images
            imageView.startAnimating()
            
            isAnimation = true
        }
    }
    
    // lazy loading
    private lazy var imageView:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "\(self.prefix)0")
        imageView.sizeToFit()
        return imageView
    }()

}
