//
//  KLCToast.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/4/11.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import Toast_Swift
import KLCKit

extension UIViewController {
    
    // 自定义的load
    func showGifToast(_ isUserInteractionEnable:Bool = false) {
        // 把fadeDuration置为0，Toast_Swift 在 fadeDuration 后会创建 Timer，网络太快会导致 hideToast 在创建 Timer 之前，造成内存不释放
        ToastManager.shared.style.fadeDuration = 0
        view.isUserInteractionEnabled = isUserInteractionEnable
        let width = UIScreen.main.bounds.size.width/375.0*46.0
        let imageView = KLCToastView(frame: CGRect(x: 0, y: 0, width: width, height: width))
        imageView.startAnimating()
        view.showToast(imageView, duration: 10,
                       point: CGPoint(x: SCREEN_WIDTH/2.0, y: SCREEN_HEIGHT/2.0),
                       completion: nil)
    }
}

class KLCToastView:UIImageView {
    
    // 图片个数
    var count = 48
    // 图片
    var prefix = "load_00"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupImages()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupImages() {
        var images = [UIImage]()
        
        for index in 0..<count {
            let name = "\(prefix)\(index)"
            if let image = UIImage(named: name) {
                images.append(image)
            }
        }
        
        if images.isEmpty {return}
        
        animationDuration = 1.1
        animationImages = images
    }
}
