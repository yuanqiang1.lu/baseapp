//
//  NotificationManager.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/10.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import Foundation
import UserNotifications

struct NotificationObj {
    var title = ""
    var body = ""
    var userInfo:[AnyHashable : Any] = [:]
    
    init() {}
    
    init(title t:String, body b:String, userInfo info:[AnyHashable : Any]) {
        title = t
        body = b
        userInfo = info
    }
}

class NotificationManager: NSObject {
    static let instance = NotificationManager()
    static func handleNotify(_ userInfo: [AnyHashable : Any]) {
        log.info("handle Notify description:\(userInfo)")
    }
    
    func updateComplete(isNew newAdd: Bool) {
        var obj = NotificationObj()
        //obj.title = "File: \(#function)"
        if newAdd {
            obj.title = "Add complete"
            obj.body = Date().toISO()
        } else {
            obj.title = "Update complete"
            obj.body = Date().toISO()
        }
        
        localPush(withObj: obj)
    }
    
    func apns() {
        let json = R.file.fileJson()
        if let json = json {
            do {
                let data = try Data.init(contentsOf:json)
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                log.info("json ---- some stuff: \(json)")
                let dic = json as! [String:Any]
                log.info("aps : \(dic["aps"] ?? "")")
                let jsonstr = String(data: data, encoding: .utf8)
                log.info("json string :\(jsonstr ?? "")")
            } catch {
                log.error(error)
            }
        }
    }
    
    func localPushWithLoadComplete() {
        var obj = NotificationObj()
        obj.title = "Data loaded succeed!"
        obj.body = "Time slot:\(Date().toISO())"
        obj.userInfo = ["File":#function]
        localPush(withObj: obj)
    }
    
    //Notification some stuffs
    func localPush(withObj obj:NotificationObj) {
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = obj.title
            content.body = obj.body
            content.userInfo = obj.userInfo
            //content.categoryIdentifier = "google.swift"
            content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "shopnew.caf"))
            content.launchImageName = "wechat_login"
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
            let request = UNNotificationRequest(identifier: "awesome\(Date().toISO())", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
                log.error("error: \(error.debugDescription)")
            })
            UNUserNotificationCenter.current().getPendingNotificationRequests { (request) in
                log.info("request : \(request.description)")
            }
        } else {
            let notification = UILocalNotification()
            notification.alertTitle = obj.title
            notification.alertBody = obj.body
            notification.userInfo = obj.userInfo
            //notification.applicationIconBadgeNumber += 1
            notification.soundName = UILocalNotificationDefaultSoundName
            
            UIApplication.shared.presentLocalNotificationNow(notification)
        }
    }
}

@available(iOS 10.0, *)
extension NotificationManager: UNUserNotificationCenterDelegate {
    //to show notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    //handle over tap notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
}
