//
//  KLCAttributeText.swift
//  LXTBaseProduct
//
//  Created by luyuanqiang on 2019/5/7.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation

class KLCAttributeText {
    //[icon]+车辆位置+车辆位置
    static func attrText(value v:String) -> NSMutableAttributedString {
        
        let markattch = NSTextAttachment()
        markattch.image = R.image.profileFooter()
        markattch.bounds = CGRect(x: 0, y: -3, width: 14, height: 14)
        let markattchStr = NSAttributedString(attachment: markattch)
        
        let attrStr = NSMutableAttributedString(string: "车辆位置：",
                                                      attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G3,
                                                                   NSAttributedString.Key.font : ScaleFONT(13)] )
        attrStr.insert(markattchStr, at: 0)
        
        let value = NSMutableAttributedString(string: v,
                                              attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G3,
                                                           NSAttributedString.Key.font : ScaleFONT(13)] )
        attrStr.append(value)
        return attrStr
    }
    
    //距您约+200+m
    static func attrDistanceText(distance v:Double) -> NSMutableAttributedString {
        var distance = ""
        var unit = "m"
        if v > 1000 {
            distance = (v/1000).preciseDecimal(length: 1)
            unit = "km"
        } else {
            distance = v.preciseDecimal(length: 0)
        }
        
        let attrStr = NSMutableAttributedString(string: "距您约：".localString,
                                                attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G3,
                                                             NSAttributedString.Key.font : ScaleFONT(14)] )
        let value = NSMutableAttributedString(string: distance,
            attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G2,
                         NSAttributedString.Key.font : ScaleFONT(30)] )
        
        let tail = NSMutableAttributedString(string: unit.localString,
                                             attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G3,
                                                          NSAttributedString.Key.font : ScaleFONT(13)] )
        
        attrStr.append(value)
        attrStr.append(tail)
        return attrStr
    }
    
    ///您: 的保障已到期
    static func renewStyleBAttrText(_ value:String) -> NSMutableAttributedString {
        let attrStr = NSMutableAttributedString(string: "您：".localString,
                                                attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G2,
                                                             NSAttributedString.Key.font : ScaleFONT(18,weight:.bold)] )
        let value = NSMutableAttributedString(string: value,
                                              attributes: [NSAttributedString.Key.foregroundColor : UIStandard.MainColor.C2,
                                                           NSAttributedString.Key.font : ScaleFONT(18,weight:.bold)] )
        
        let tail = NSMutableAttributedString(string: "的保障已到期".localString,
                                             attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G2,
                                                          NSAttributedString.Key.font : ScaleFONT(18,weight:.bold)] )
        attrStr.append(value)
        attrStr.append(tail)
        return attrStr
    }
    
    ///凯励程已守护您 天
    static func renewStyleCAttrText(_ value:String) -> NSMutableAttributedString {
        let attrStr = NSMutableAttributedString(string: "凯励程".localString,
                                                attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G2,
                                                             NSAttributedString.Key.font : ScaleFONT(18,weight:.bold)] )
        let value = NSMutableAttributedString(string: value,
                                              attributes: [NSAttributedString.Key.foregroundColor : UIStandard.MainColor.C2,
                                                           NSAttributedString.Key.font : ScaleFONT(18,weight:.bold)] )
        
        attrStr.append(value)
        return attrStr
    }
    ///凯励程 已守护[品牌] 多少天
    static func renewStyleAAttrText(_ value:String, _ days:String) -> NSMutableAttributedString {
        let attrStr = NSMutableAttributedString(string: "凯励程已守护:".localString,
                                                attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G2,
                                                             NSAttributedString.Key.font : ScaleFONT(18, weight: .bold)] )
        let value = NSMutableAttributedString(string: value,
                                              attributes: [NSAttributedString.Key.foregroundColor : UIStandard.MainColor.C2,
                                                           NSAttributedString.Key.font : ScaleFONT(18,weight: .bold)] )
        
        let tail = NSMutableAttributedString(string: "\(days)天".localString,
                                             attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G2,
                                                          NSAttributedString.Key.font : ScaleFONT(18,weight:.bold)] )
        attrStr.append(value)
        attrStr.append(tail)
        return attrStr
    }
    
    ///距服务到期还有13天
    static func renewDescStyleAAttrText(_ value:String) -> NSMutableAttributedString {
        let attrStr = NSMutableAttributedString(string: "距服务到期还有".localString,
                                                attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G4,
                                                             NSAttributedString.Key.font : ScaleFONT(12)] )
        let value = NSMutableAttributedString(string: value,
                                              attributes: [NSAttributedString.Key.foregroundColor : UIColor.hexStringToColor(hexString: "FF9500"),
                                                           NSAttributedString.Key.font : ScaleFONT(12)] )
        
        let tail = NSMutableAttributedString(string: "天".localString,
                                             attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G4,
                                                          NSAttributedString.Key.font : ScaleFONT(12)] )
        attrStr.append(value)
        attrStr.append(tail)
        return attrStr
    }
    
    static func renewDescStyleBAttrText(_ value:String) -> NSMutableAttributedString {
        let attrStr = NSMutableAttributedString(string: value,
                                                attributes: [NSAttributedString.Key.foregroundColor : UIStandard.FontColor.G4,
                                                             NSAttributedString.Key.font : ScaleFONT(12)] )
        return attrStr
    }
}
