//
//  UIStandard.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/2.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import Foundation
import KLCKit
import SwiftTheme

struct UIStandard {
    /// 规范主色
    struct MainColor {
        ///页面主色调，突出专业性与安全感，用于部分一级导航栏
        static var C1:UIColor { return ThemeManager.color(for: "Global.C1") ?? UIColor.hexStringToColor(hexString: "1751C7")} //后期可能会动态修改
        
        ///主色邻近色，用于需突出、可点击的文字、按钮
        static var C2:UIColor { return ThemeManager.color(for: "Global.C2") ?? UIColor.hexStringToColor(hexString: "437AEB")} //后期可能会动态修改
        
        static var C3:UIColor {
            get {
                return UIColor.hexStringToColor(hexString: "437AEB")
            }
            set {
                self.C3 = newValue
            }

        }
        mutating func upcolor() {
            UIStandard.MainColor.C3 = UIColor.hexStringToColor(hexString: "1752C7")
        }
    }
    
    /// 规范字体颜色
    struct FontColor {
        /// #141823
        static let G1 = UIColor.hexStringToColor(hexString: "141823")
        /// #3A3E45
        static let G2 = UIColor.hexStringToColor(hexString: "3A3E45")
        /// 616770
        static let G3 = UIColor.hexStringToColor(hexString: "616770")
        /// 9197A3
        static let G4 = UIColor.hexStringToColor(hexString: "9197A3")
        /// CCD0D5
        static let G5 = UIColor.hexStringToColor(hexString: "CCD0D5")
        /// E8EAF0
        static let G6 = UIColor.hexStringToColor(hexString: "E8EAF0")
        /// F6F7F8
        static let G7 = UIColor.hexStringToColor(hexString: "F6F7F8")
        /// white
        static let G8 = UIColor.white
    }
    
    /// 规范背景颜色
    struct BGColor {
        static let BG1 = FontColor.G7
        static let BG2 = FontColor.G8
    }
    
    /// 段落行高1.8倍
    static var paragraph1_8:NSParagraphStyle = {
        let style = NSMutableParagraphStyle()
        style.lineHeightMultiple = 1.8
        style.lineBreakMode = .byCharWrapping
        return style
    }()
    
    /// 段落行高1.5倍
    static var paragraph1_5:NSParagraphStyle = {
        let style = NSMutableParagraphStyle()
        style.lineHeightMultiple = 1.8
        style.lineBreakMode = .byCharWrapping
        return style
    }()
}

extension UIStandard {
    struct BTNSettings {
        var height:CGFloat = 0
        var corner:CGFloat = 0
        var font = FONT(18)
        var bgColor = UIColor.white
        var pressBgColor = UIColor.white
        var disabledBgColor = FontColor.G5
        var fontColor = UIColor.white
        var fontPressColor = UIColor.white
        var fontDisabledColor = UIColor.white
        var borderColor = UIColor.clear
        var borderPressColor = UIColor.clear
        var borderDisabledColor = UIColor.clear
        
        init(height:CGFloat = 26,
             corner:CGFloat = 13,
             font:UIFont = FONT(18),
             bgColor:UIColor = MainColor.C2,
             pressBgColor:UIColor = MainColor.C1,
             disabledBgColor:UIColor = FontColor.G5,
             fontColor:UIColor = FontColor.G8,
             fontPressColor:UIColor = FontColor.G8.withAlphaComponent(0.8),
             fontDisabledColor:UIColor = FontColor.G4,
             borderColor:UIColor = .clear,
             borderPressColor:UIColor = .clear,
             borderDisabledColor:UIColor = .clear) {
            self.height = height
            self.corner = corner
            self.font = font
            self.bgColor = bgColor
            self.pressBgColor = pressBgColor
            self.disabledBgColor = disabledBgColor
            self.fontColor = fontColor
            self.fontPressColor = fontPressColor
            self.fontDisabledColor = fontDisabledColor
            self.borderColor = borderColor
            self.borderPressColor = borderPressColor
            self.borderDisabledColor = borderDisabledColor
        }
        
        init(height:CGFloat, font:UIFont, settings:BTNSettings) {
            self.height = height
            self.corner = height/2
            self.font = font
            self.bgColor = settings.bgColor
            self.pressBgColor = settings.pressBgColor
            self.disabledBgColor = settings.disabledBgColor
            self.fontColor = settings.fontColor
            self.fontPressColor = settings.fontPressColor
            self.fontDisabledColor = settings.fontDisabledColor
            self.borderColor = settings.borderColor
            self.borderPressColor = settings.borderPressColor
            self.borderDisabledColor = settings.borderDisabledColor
        }
        
    }
    
    struct BTNStyle {
        
        static let BTN1_1 = BTNSettings(height: 52,
                                        corner: 26,
                                        font: FONT(18),
                                        bgColor: MainColor.C2,
                                        pressBgColor: MainColor.C1,
                                        disabledBgColor: FontColor.G6,
                                        fontColor: FontColor.G8,
                                        fontPressColor: FontColor.G8.withAlphaComponent(0.8),
                                        fontDisabledColor: FontColor.G5,
                                        borderColor: .clear,
                                        borderPressColor: .clear,
                                        borderDisabledColor: .clear)
        static let BTN1_2 = BTNSettings(height: 48, font: FONT(16), settings: BTNStyle.BTN1_1)
        static let BTN1_3 = BTNSettings(height: 40, font: FONT(14), settings: BTNStyle.BTN1_1)
        static let BTN1_4 = BTNSettings(height: 32, font: FONT(12), settings: BTNStyle.BTN1_1)
        
        static let BTN2_1 = BTNSettings(height: 52,
                                        corner: 26,
                                        font: FONT(18),
                                        bgColor: FontColor.G8,
                                        pressBgColor: FontColor.G6,
                                        disabledBgColor: FontColor.G6,
                                        fontColor: FontColor.G2,
                                        fontPressColor: FontColor.G2,
                                        fontDisabledColor: FontColor.G4,
                                        borderColor: .clear,
                                        borderPressColor: .clear,
                                        borderDisabledColor: .clear)
        static let BTN2_2 = BTNSettings(height: 48, font: FONT(16), settings: BTNStyle.BTN1_1)
        static let BTN2_3 = BTNSettings(height: 40, font: FONT(14), settings: BTNStyle.BTN1_1)
        static let BTN2_4 = BTNSettings(height: 32, font: FONT(12), settings: BTNStyle.BTN1_1)
        
        static let BTN3_1 = BTNSettings(height: 52,
                                        corner: 26,
                                        font: FONT(18),
                                        bgColor: .clear,
                                        pressBgColor: .clear,
                                        disabledBgColor: .clear,
                                        fontColor: MainColor.C2,
                                        fontPressColor: MainColor.C1.withAlphaComponent(0.7),
                                        fontDisabledColor: FontColor.G5,
                                        borderColor: MainColor.C2,
                                        borderPressColor: MainColor.C1,
                                        borderDisabledColor: FontColor.G5)
        static let BTN3_2 = BTNSettings(height: 48, font: FONT(16), settings: BTNStyle.BTN1_1)
        static let BTN3_3 = BTNSettings(height: 40, font: FONT(14), settings: BTNStyle.BTN1_1)
        static let BTN3_4 = BTNSettings(height: 32, font: FONT(12), settings: BTNStyle.BTN1_1)
    }
}

extension UIButton {
    func button(settings:UIStandard.BTNSettings) {
        layer.cornerRadius = settings.corner
        setTitleColor(settings.fontColor, for: .normal)
        setTitleColor(settings.fontPressColor, for: .highlighted)
        setTitleColor(settings.fontDisabledColor, for: .disabled)
        setBackgroundImage(UIImage.image(with: settings.bgColor), for: .normal)
        setBackgroundImage(UIImage.image(with: settings.disabledBgColor), for: .disabled)
        titleLabel?.font = settings.font
        layer.borderColor = settings.borderColor.cgColor
        layer.borderWidth = 0.5
        var frameTemp = self.frame
        frameTemp.size.height = settings.height
        self.frame = frameTemp
    }
}
