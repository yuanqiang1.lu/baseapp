//
//  KLCPayTypeView.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/18.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit

enum KLCPayType {
    case wechatPay
    case alipay
    case none
}

class KLCPayTypeView: UIView {
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var wechatPayBtn: UIButton!
    @IBOutlet weak var alipayBtn: UIButton!
    private(set) var selectedPayType = KLCPayType.none
    var block:((KLCPayType) -> Void)?
    
    @IBAction func selectWechatAction(_ sender: UITapGestureRecognizer) {
        wechatPayBtn.isSelected = true
        alipayBtn.isSelected = false
        selectedPayType = .wechatPay
        block?(.wechatPay)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
    }
    
    @IBAction func selectAlipayAction(_ sender: UITapGestureRecognizer) {
        wechatPayBtn.isSelected = false
        alipayBtn.isSelected = true
        selectedPayType = .alipay
        block?(.alipay)
    }
}
