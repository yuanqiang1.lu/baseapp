//
//  KLCVehicleListCell.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/4/18.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import Kingfisher

class KLCVehicleListCell: UITableViewCell {
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var vehicleNameLabel: UILabel!
    @IBOutlet weak var radioBtn: UIButton!
    @IBOutlet weak var normalLabel: UILabel!
    
    var isGarage = false // 如果是在车库切车,使用is_selected = true的车辆
    var model:KLCVehicleModel? {
        didSet {
            guard let model = model else {
                return
            }
            vehicleNameLabel.text = (model.vehicle_type?.brand ?? "") + (model.vehicle_type?.serial ?? "")
            normalLabel.isHidden = model.vip
            radioBtn.isSelected = isGarage ? model.is_selected : model.is_current
            vehicleNameLabel.textColor = radioBtn.isSelected ? UIStandard.FontColor.G3 : UIStandard.FontColor.G4
            if let url = URL(string: model.vehicle_type?.brand_image_url ?? "") {
                logoView.kf.setImage(with: ImageResource(downloadURL: url),
                                     placeholder: R.image.vehicle_logo(),
                                     options: [.transition(.fade(0.25))])
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            if isGarage {
                if model?.vip == true { // 如果是VIP车辆选中即为切换车辆
                    KLCVehicleManager.share.currentVehicle = model
                } else {
                    KLCVehicleManager.share.selectedVehicle = model
                }
            } else {
                KLCVehicleManager.share.currentVehicle = model
            }
        }
    }
}
