//
//  KLCInputViewController.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/5/6.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import RxSwift

class KLCInputViewController: UIViewController {

    @IBOutlet weak var inpuTextField: UITextField!
    
    var inputTitle = ""
    var placeholder = ""
    var inputValue = ""
    
    var value = PublishSubject<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = inputTitle
        inpuTextField.placeholder = placeholder
        inpuTextField.text = inputValue
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: saveBtn)
        inpuTextField.rx.text.subscribe(onNext: {[weak self] (text) in
            let text = text ?? ""
            self?.saveBtn.isUserInteractionEnabled = !text.isEmpty
            self?.saveBtn.setTitleColor(text.isEmpty ? UIStandard.FontColor.G5 : UIStandard.FontColor.G4, for: .normal)
        }).disposed(by: rx.disposeBag)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        inpuTextField.becomeFirstResponder()
    }

    @objc func saveAction() {
        log.info("保存")
        let text = inpuTextField.text ?? ""
        if text.isEmpty {
           return
        }
        
        value.onNext(text)
        navigationController?.popViewController(animated: true)
    }

    // MARK: lazy loading
    lazy var saveBtn:UIButton = {
        let saveBtn = UIButton(type: .custom)
        saveBtn.frame = CGRect(x: 0, y: 0, width: 50, height: 44)
        saveBtn.contentHorizontalAlignment = .right
        saveBtn.setTitle("保存", for: .normal)
        saveBtn.setTitleColor(UIStandard.FontColor.G4, for: .normal)
        saveBtn.setTitleColor(UIStandard.FontColor.G5, for: .disabled)
        saveBtn.titleLabel?.font = FONT(15,weight:.medium)
        saveBtn.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        return saveBtn
    }()

}
