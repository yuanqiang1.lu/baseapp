//
//  KLCQueryHelpViewController.swift
//  LXTBaseProduct
//
//  Created by awesome on 2019/4/22.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import KLCKit

class KLCQueryHelpViewController: UIViewController {
    
    enum AdPopType {
        /// 两个按钮在 广告框内
        case normal
        /// 广告框内 有一个按钮
        case oneInsideBtn
        /// 广告框外 有一个按钮
        case oneOutsideBtn
        /// 广告框外 是一个 'x'
        case closeOutside
        /// 紧急弹窗
        case emergencyPop
    }
    
    var popStyle:AdPopType = .normal
    
    var btnTitleOne:String! = "帮助中心"
    var btnTitleTwo:String! = "客服电话"
    
    ///广告中心的图片
    @IBOutlet weak var advImgView: UIImageView!
    ///广告内容 attribute txt
    @IBOutlet weak var contentLabel: UILabel!
    /// x 按钮
    @IBOutlet weak var normalCloseBtn: UIButton!
    /// 广告框外部按钮
    @IBOutlet weak var outsideBtn: UIButton!
    
    @IBOutlet weak var insideLeftBtn: UIButton!
    
    @IBOutlet weak var insideRightBtn: UIButton!
    //默认80px
    @IBOutlet weak var contentBottomConstraint: NSLayoutConstraint!
    //默认 118px
    @IBOutlet weak var btnWidthConstraint: NSLayoutConstraint!
    //默认 28px
    @IBOutlet weak var rightBtnTailConstraint: NSLayoutConstraint!
    
    ///紧急弹窗显示的图片
    @IBOutlet weak var emergencyImageView: UIImageView!
    ///广告contener
    @IBOutlet weak var popView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLayout(with: popStyle)
        updateTitle()
    }

    func showView() {
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        CurrentViewController?.present(self, animated: true, completion: nil)
    }
    
    @IBAction func makeCall(_ sender: Any) {
        callService()
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        if popStyle == .emergencyPop {
            log.info("按钮退出")
        } else {
            CurrentViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func outsideBtnAction(_ sender: Any) {
        log.info("按钮点击处理")
        if popStyle == .emergencyPop {
            log.info("按钮退出")
            exit(0)
        }
    }
    
    private func updateLayout(with style:AdPopType) {
        popView.isHidden = false
        emergencyImageView.isHidden = true
        switch style {
        case .normal://内部两个按钮
            normalCloseBtn.layer.isHidden = true
            outsideBtn.layer.isHidden = true
            insideLeftBtn.layer.isHidden = false
            insideRightBtn.layer.isHidden = false
            btnWidthConstraint.constant = 118.0
            rightBtnTailConstraint.constant = 28.0
            contentBottomConstraint.constant = 80
        case .oneInsideBtn://内部一个按钮
            normalCloseBtn.layer.isHidden = true
            outsideBtn.layer.isHidden = true
            insideLeftBtn.layer.isHidden = true
            insideRightBtn.layer.isHidden = false
            btnWidthConstraint.constant = 160.0
            rightBtnTailConstraint.constant = 70.0
        case .oneOutsideBtn://外部一个按钮
            normalCloseBtn.layer.isHidden = true
            outsideBtn.layer.isHidden = false
            insideLeftBtn.layer.isHidden = true
            insideRightBtn.layer.isHidden = true
            contentBottomConstraint.constant = 80 - 44
        case .closeOutside://外部一个x
            normalCloseBtn.layer.isHidden = false
            outsideBtn.layer.isHidden = true
            insideLeftBtn.layer.isHidden = true
            insideRightBtn.layer.isHidden = true
            contentBottomConstraint.constant = 80 - 44
        case .emergencyPop://紧急弹窗
            popView.isHidden = true
            emergencyImageView.isHidden = false
            contentBottomConstraint.constant = 80 - 44
            outsideBtn.setTitle("退出".localString, for: .normal)
        }
    }
    
    private func updateTitle() {
        let leftTitle = btnTitleOne
        let rightTitle = btnTitleTwo
        switch popStyle {
        case .normal:
            insideLeftBtn.setTitle(leftTitle, for: .normal)
            insideRightBtn.setTitle(rightTitle, for: .normal)
        case .oneInsideBtn:
            insideRightBtn.setTitle(leftTitle, for: .normal)
        case .oneOutsideBtn:
            outsideBtn.setTitle(leftTitle, for: .normal)
        case .closeOutside,.emergencyPop:
            break
        }
    }
}
