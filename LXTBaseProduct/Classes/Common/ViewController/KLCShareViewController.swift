//
//  KLCShareViewController.swift
//  LXTBaseProduct
//
//  Created by wang zhenqi on 2019/5/22.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import KLCKit

class KLCShareViewController: UIViewController {
    @IBOutlet weak var shareSystemBtn: UIButton!
    @IBOutlet weak var shareWxFriendBtn: UIButton!
    @IBOutlet weak var shareWxSessionBtn: UIButton!
    private var message: KLCThirdShare.Message?
    private var shareTitle: String?
    private var desc: String?
    private var thumbnail: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let btns = [shareSystemBtn,shareWxFriendBtn,shareWxSessionBtn]
        btns.forEach { (btn) in
            btn?.adjustImage(position: .top, spacing: 8)
        }
    }

    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    static func showShare(title:String? = nil,
                          desc:String? = nil,
                          thumbnail:UIImage? = nil,
                          msg:KLCThirdShare.Message) {
        let vc = KLCShareViewController()
        vc.shareTitle = title
        vc.desc = desc
        vc.thumbnail = thumbnail
        vc.message = msg
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        CurrentViewController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        guard let message = message else {
            return
        }
        var shareType:KLCThirdShare.ShareType!
        if sender == shareSystemBtn {
            shareType = .system(viewController: self)
        } else if sender == shareWxFriendBtn {
            shareType = .weixin(.timeline)
        } else {
            shareType = .weixin(.session)
        }
        KLCThirdShare.share(type: shareType, title: shareTitle, desc: desc, thumbnail: thumbnail, message: message) { [weak self](result) in
            switch result {
            case .success:
                self?.dismiss(animated: true, completion: nil)
            case .failure:
                self?.showToast("分享失败！", duration: 1)
            }
        }
    }
}
