//
//  KLCWeChatApi.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/4/28.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit

enum KLCWeChatApi {
    /// 用户关注的微信公众号
    case officialAccountInfo
    /// 解除公众号绑定
    case removeOfficialAccountBind
    /// 用户获取公众号二维码链接
    case getOfficialAccountQcode
}

extension KLCWeChatApi:TargetType {
    var baseURL: URL {
        return URL(string: KLCBaseUrl)!
    }
    
    var path: String {
        switch self {
        case .officialAccountInfo:
            return "/wechat/mp/"
        case .removeOfficialAccountBind:
            return "/wechat/mp/"
        case .getOfficialAccountQcode:
            return "/wechat/mp/qcode/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .officialAccountInfo,
             .getOfficialAccountQcode:
            return .get
        case .removeOfficialAccountBind:
            return .delete
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .officialAccountInfo,
             .removeOfficialAccountBind,
             .getOfficialAccountQcode:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return [:]
    }
}

extension Int {
    /// 用户没有关注微信公众号
    static let userNoBindOfficialAccount = 51800
}
