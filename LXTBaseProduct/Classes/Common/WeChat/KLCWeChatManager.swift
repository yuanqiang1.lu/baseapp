//
//  KLCWeChatManager.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/5/20.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit

class KLCWeChatManager {
    // 用户关注的微信公众号
    class func getOfficialAccountInfo(result:((_ accountModel:KLCWechatOfficialAccountModel?,_ isBind:Bool, _ errorMsg:String) -> Void)?) {
        _ = KLCNetwork.request(MultiTarget(KLCWeChatApi.officialAccountInfo))
                .mapModel(KLCWechatOfficialAccountModel.self)
                .subscribe(onNext: { (data) in
                    result?(data,true,"")
                }, onError: { (error) in
                    /// 用户没有关注公众号
                    if KLCErrorCode(error) == .userNoBindOfficialAccount {
                        result?(nil,false,KLCErrorDescription(error))
                    } else {
                        result?(nil,false,KLCErrorDescription(error))
                    }
                })
    }
    
    /// 取消公众号绑定
    class func removeOfficialAccountBind(result:((_ success:Bool, _ errorMsg:String) -> Void)?) {
        _ = KLCNetwork.request(MultiTarget(KLCWeChatApi.removeOfficialAccountBind))
                .mapModel(KLCBaseModel.self)
                .subscribe(onNext: { (_) in
                    result?(true,"")
                }, onError: { (error) in
                    result?(false,KLCErrorDescription(error))
                })
    }
    
    /// 获取关注公众号二维码
    class func getOfficialAccountQcode(result:((_ qrCode:String, _ errorMsg:String) -> Void)?) {
        _ = KLCNetwork.request(MultiTarget(KLCWeChatApi.getOfficialAccountQcode))
                .mapModel(KLCWechatOfficialAccountQcodeModel.self)
                .subscribe(onNext: { (data) in
                    let qcode = data?.qcode_url ?? ""
                    result?(qcode,"")
                }, onError: { (error) in
                    result?("",KLCErrorDescription(error))
                })
    }
}
