//
//  KLCWeChatModel.swift
//  LXTBaseProduct
//
//  Created by Kun Huang on 2019/4/28.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit

class KLCWechatOfficialAccountModel: KLCBaseModel {
    var head_image_url:String?
    var nickname:String?
}

class KLCWechatOfficialAccountQcodeModel: KLCBaseModel {
    var qcode_url:String?
}
