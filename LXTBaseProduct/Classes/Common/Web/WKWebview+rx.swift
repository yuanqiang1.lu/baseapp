//
//  WKWebview+rx.swift
//  LXTBaseProduct
//
//  Created by awesome on 2019/4/20.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import Foundation
import WebKit

class RxWebViewNavigationDelegateProxy: DelegateProxy<WKWebView, WKNavigationDelegate>, WKNavigationDelegate, DelegateProxyType {
    
    init(wkWebview: WKWebView) {
        super.init(parentObject: wkWebview, delegateProxy: RxWebViewNavigationDelegateProxy.self)
    }
    
    static func registerKnownImplementations() {
        self.register { RxWebViewNavigationDelegateProxy(wkWebview: $0) }
    }
    
    static func currentDelegate(for object: ParentObject) -> WKNavigationDelegate? {
        return object.navigationDelegate
    }
    
    static func setCurrentDelegate(_ delegate: WKNavigationDelegate?, to object: ParentObject) {
        object.navigationDelegate = delegate
    }
    
}

extension Reactive where Base: WKWebView {
    
    var title: Observable<String?> {
        return self.observeWeakly(String.self, "title")
    }
    
    var url: Observable<URL?> {
        return self.observeWeakly(URL.self, "URL")
    }
    
    var loading: Observable<Bool> {
        return self.observeWeakly(Bool.self, "isLoading")
            .map { $0 ?? false }
    }
    
    var estimatedProgress: Observable<Double> {
        return self.observeWeakly(Double.self, "estimatedProgress")
            .map { $0 ?? 0.0 }
            .share()
    }
    
    var canGoBack: Observable<Bool> {
        return self.observeWeakly(Bool.self, "canGoBack")
            .map { $0 ?? false }
    }
    
    var canGoForward: Observable<Bool> {
        return self.observeWeakly(Bool.self, "canGoForward")
            .map { $0 ?? false }
    }
    
    var navigationDelegate: DelegateProxy<WKWebView, WKNavigationDelegate> {
        return RxWebViewNavigationDelegateProxy.proxy(for: base)
    }
    
    var didFinishNavigation: ControlEvent<(WKWebView, WKNavigation)> {
        let source = navigationDelegate.methodInvoked(#selector(WKNavigationDelegate.webView(_:didFinish:))).map { a in
            return (try castOrThrow(WKWebView.self, a[0]), try castOrThrow(WKNavigation.self, a[1]))
        }
        return ControlEvent(events: source)
    }
    
    var didFailNavigation: ControlEvent<(WKWebView, WKNavigation, Error)> {
        let source = navigationDelegate.methodInvoked(#selector(WKNavigationDelegate.webView(_:didFail:withError:))).map { a  in
            return (try castOrThrow(WKWebView.self, a[0]), try castOrThrow(WKNavigation.self, a[1]), try castOrThrow(Error.self, a[2]))
        }
        return ControlEvent(events: source)
    }
    
}
