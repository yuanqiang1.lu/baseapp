//
//  LXTBaseProductTests.swift
//  LXTBaseProductTests
//
//  Created by luyuanqiang on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import XCTest
import KLCKit
@testable import LXTBaseProduct

class LXTBaseProductTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testDateTransform() {
        let mdate = "2019-01-23T07:56:59.414Z".toISODate()?.toFormat("yyyy-MM-dd HH:mm") ?? ""
        log.verbose("mdate : \(mdate)")
        //XCTFail("last item in progressValues should not be nil")
        let now = Date()
        let isoStr = now.toISO()
        log.verbose("iso str: \(isoStr)")
    }
    
    func testLocalizable() {
        let localStr = "align.awesome.name".localString
        XCTAssertNotNil(localStr)
        XCTAssertEqual(localStr,"awesome")
    }
    
    func testValidateEmail() {
        let inputStr = "abc@23.com"
        let isEmail = regexEmail(inputStr)
        XCTAssertTrue(isEmail)
        if isEmail {
            let dataArr = inputStr.split(separator: "@")
            log.info("email name:\(dataArr.first ?? "")")
        }
    }
    
    func testDateComparison() {
        defer {
            log.info("\(#function) excuting")
        }
        let aDate = "2019-01-23T07:56:59.414Z".toDate()?.date
        guard let beforeDate = aDate  else {
            XCTFail("beforedate is error format")
            return
        }
        let now = Date()
        let flag = beforeDate < now
        XCTAssertTrue(flag,"not true")
        
        let morning = ("2019-07-10 08:00".toDate("yyyy-MM-dd HH:mm")! + 10.hours)
        let endTime = morning.toFormat("yyyy-MM-dd HH:mm")
        XCTAssertEqual(endTime, "2019-07-10 12:00", "Not equalable")
    }
    
    /// Desks list showing
    func testDesksList() {
        let expectation = self.expectation(description: "the desks should complete")
        KLCNetwork.request(MultiTarget(KLCBookingApi.desksList))
            .mapArrayModel(KLCSeatsModel.self)
            .subscribe(onNext: { (rs) in
                expectation.fulfill()
            }, onError: { (error) in
                //self.errorMsg.accept(KLCErrorDescription(error))
                print("error describtion:\(KLCErrorDescription(error)) -- code: \(KLCErrorCode(error))")
            }).disposed(by:rx.disposeBag)
        waitForExpectations(timeout: 10, handler: nil)
    }
    /// Let make a registration
    func testRegister() {
        let expectation = self.expectation(description: "user register complete")
        let paramDic = ["user[email]":"awesome@k.com",
                        "user[password]":"123bceg",
                        "user[password_confirmation]":"123bceg"] as [String : Any]
        KLCNetwork.request(MultiTarget(KLCLoginApi.userRegistration(paramDic)))
            .mapModel(KLCUserLoginModel.self)
            .subscribe(onNext: { (rs) in
                expectation.fulfill()
            }, onError: { (error) in
                print("error describtion:\(KLCErrorDescription(error)) -- code: \(KLCErrorCode(error))")
            }).disposed(by:rx.disposeBag)
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testLogin() {
        let paramDic = ["user[email]":"awesome@k.com",
                        "user[password]":"123bceg"] as [String : Any]
        let expectation = self.expectation(description: "user logincomplete")
        KLCNetwork.request(MultiTarget(KLCLoginApi.userLogin(paramDic)))
            .mapModel(KLCUserLoginModel.self)
            .subscribe(onNext: { (rs) in
                log.info("Write user data to cache \(rs?.toJSONString() ?? "")")
                KLCUserManager.saveUserToken(rs?.authentication_token ?? "")
                expectation.fulfill()
            }, onError: { (error) in
                print("error describtion:\(KLCErrorDescription(error)) -- code: \(KLCErrorCode(error))")
            }).disposed(by:rx.disposeBag)
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testCachedToken() {
        let token = KLCUserManager.userToken()
        XCTAssertNotNil(token)
        if let token = token {
            log.info("token is :\(token)")
        } else {
            log.error("user token is nil")
        }
    }
    
    func testLogout() {
        let token = KLCUserManager.userToken()
        if let token = token {
            //XCTAssertNil(token, "user token is not nil")
            XCTAssertNotNil(token, "the token is:\(token)")
            log.info("User token cleaning")
            KLCUserManager.saveUserToken("")
            let userToken = KLCUserManager.userToken()
            XCTAssertNil(userToken, "user token is not nil")
        } else {
            log.info("User logout or not login")
            log.error("user token is nil")
            XCTAssertNil(token, "user token is not nil")
        }
    }
    
    func testBooking() {
        let expectation = self.expectation(description: "user booking request complete")
        let paramDic = ["booking[desk_id]":4,
                        "booking[booking_at]":12,
                        "booking[booked_from]":"2019-07-13T07:56:43.480Z",
                        "booking[booked_to]":"2019-07-13T07:56:43.480Z"] as [String : Any]
        KLCNetwork.request(MultiTarget(KLCBookingApi.newBooking(paramDic)))
            .mapModel(KLCBookingResultModel.self)
            .subscribe(onNext: { (rs) in
                expectation.fulfill()
            }, onError: { (error) in
                print("error describtion:\(KLCErrorDescription(error)) -- code: \(KLCErrorCode(error))")
            }).disposed(by:rx.disposeBag)
        waitForExpectations(timeout: 10, handler: nil)
    }
    /// Update the booking item by id
    func testUpdateBooking() {
        let expectation = self.expectation(description: "user booking request complete")
        let bookingId = 161
        let paramDic = ["booking[desk_id]":4,
                        "booking[booking_at]":12,
                        "booking[booked_from]":"2019-04-13T07:56:43.480Z",
                        "booking[booked_to]":"2019-05-13T07:56:43.480Z"] as [String : Any]
        KLCNetwork.request(MultiTarget(KLCBookingApi.updateBooking(index: bookingId, param: paramDic)))
            .mapModel(KLCUserLoginModel.self)
            .subscribe(onNext: { (rs) in
                expectation.fulfill()
            }, onError: { (error) in
                expectation.fulfill()
                print("error describtion:\(KLCErrorDescription(error)) -- code: \(KLCErrorCode(error))")
                XCTFail("\(expectation.expectationDescription) with error")
            }).disposed(by:rx.disposeBag)
        waitForExpectations(timeout: 10, handler: nil)
    }
    /// Test login with URLSession
    func testLoginWithURLSession() {
        let expectation = self.expectation(description: "user register complete")
        guard let url = URL(string: "http://apac-ar-test.herokuapp.com/users/sign_in.json?user[email]=awesome@k.com&user[password]=123bceg") else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    expectation.fulfill()
                    print(json)
                } catch {
                    print("ERROR")
                    print(error)
                }
            }
            }.resume()
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testBookingList() {
        let expectation = self.expectation(description: "user booking list complete")
        KLCNetwork.request(MultiTarget(KLCBookingApi.booksList))
            .mapArrayModel(KLCBookingResultModel.self)
            .subscribe(onNext: { (rs) in
                rs?.forEach({ model in
                    log.info("booking model:\(model?.toJSONString() ?? "")")
                })
                expectation.fulfill()
            }, onError: { (error) in
                print("error describtion:\(KLCErrorDescription(error)) -- code: \(KLCErrorCode(error))")
            }).disposed(by:rx.disposeBag)
        waitForExpectations(timeout: 10, handler: nil)
    }
    /// Test registration with URLSession
    func testRegisterApiWithURLSession() {
        let expectation = self.expectation(description: "user register complete")
        guard let url = URL(string: "http://apac-ar-test.herokuapp.com/users.json?user[email]=50202768@qq.com&user[password]=abc234560&user[password_confirmation]=abc234560") else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        //request.addValue("text", forHTTPHeaderField: "hello")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    expectation.fulfill()
                    print(json)
                } catch {
                    log.error("registry with error:\(error.localizedDescription)")
                }
            }
            
            }.resume()
        waitForExpectations(timeout: 10, handler: nil)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            log.verbose("verbose")
            testRegisterApiWithURLSession()
        }
    }

}
