//
//  LXTBaseProductUITests.swift
//  LXTBaseProductUITests
//
//  Created by luyuanqiang on 2019/3/28.
//  Copyright © 2019 Goswift iOS team. All rights reserved.
//

import XCTest
import KLCKit
@testable import LXTBaseProduct

class LXTBaseProductUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        app.tables.children(matching: .cell).element(boundBy: 2).staticTexts["2019-01-27 00:00 to 2019-01-29 00:00"].tap()
        
        let element = app.otherElements.containing(.navigationBar, identifier:"Detail").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        element.tap()
        app.navigationBars["Detail"].buttons["预订列表"].tap()
        
    }
    
    func testLogin() {
        
        let app = XCUIApplication()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 0).textFields["邮件地址"].tap()
        // FYI: https://www.codercto.com/a/46910.html
        // Some usage of uitest
        app.typeText("awe@163.com")
        
    }
}
