<img width=443 src="docs/bkground.png" alt="New Klicen Logo" />

[![Pod Version](https://img.shields.io/badge/Pod-2.0.2-6193DF.svg)](https://cocoapods.org/)
![Swift Version](https://img.shields.io/badge/xCode-10.1+-blue.svg)
![Swift Version](https://img.shields.io/badge/iOS-10.0+-blue.svg) 
![Swift Version](https://img.shields.io/badge/Swift-5.0+-orange.svg)
![Plaform](https://img.shields.io/badge/Platform-iOS-lightgrey.svg)

XCode `10.2` - swift `5.0` - iOS `9.0`

### Build Environment With Bundle

<img width=378 src="docs/environment.png" alt="Environment" />

### Profiles Managment

`~/Library/MobileDevice/Provisioning Profiles/`


### UserStory

```
1). As a user,I want to register,
2).As a user, I want to see,the usage of the seats list
3).As a user, I want to login，input name and password
4).As a user, I want to see a specific item in the seats list
5).As a user, I want to edit the booking time
6).As a user, I want to select a seat for date duration
7).As a user, I want to booking a new seat
8).As a user, I want to cancel a seat book
9).As a user, I want to edit the booking 
10).As a user, I want to reset my password
11).As a user, I want to logout 
```

### iPhone Screen Adaptation

[iPhone Screen resolution and adaptation rules](https://www.jianshu.com/p/41a8ccdf91ed)

### Login Config

`Defaults[.isLogin] = true`
